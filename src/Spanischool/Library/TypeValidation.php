<?php
namespace Spanischool\Library;

class TypeValidation {

	public static function isFloat($number) {
		return $number==(string)(float)$number;
	}
	
	public static function isInteger($number) {
		return $number==(string)(integer)$number;
	}
}
