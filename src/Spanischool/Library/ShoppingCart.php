<?php
namespace Spanischool\Library;

use Spanischool\Types\ShoppingCartTypeType;

use Doctrine\ORM\EntityManager;

use Serializable;
use Iterator;

// Carro de la compra
class ShoppingCart implements Iterator {
	
	private $items;
	private $promotionalCodes;

	public function __construct() {
		$this->clear();
		$this->currency = NULL;
	}
	
	// Eliminar todos los elementos del carro
	public function clear() {
		$this->items = array();
		$this->promotionalCodes = array();
	}
	
	// Añadir elemento al carro
	public function add($type, $id, $quantity) {
		if (!$this->contain($type, $id)) {
			$this->items[$type.'-'.$id] = new ShoppingCartItem($type, $id, $quantity);
		}
	}
	
	// Incrementar o decrementar la cantidad de algún elemento
	public function increase($type, $id, $variation) {
		$this->items[$type.'-'.$id]->setQuantity($this->items[$type.'-'.$id]->getQuantity()+$variation);
	}

	// Quitar elemento del carro
	public function remove($type, $id) {
		$newItems = array();
		foreach ($this->items as $itemKey => $item) {
			if ($item->getType()!==$type || $item->getid()!==$id) {
				$newItems[$itemKey] = $item;
			}
		}
		$this->items = $newItems;
	}
	
	// Añadir código al carro
	public function addPromotionalCode($promotionalCode) {
		// No vamos a permitir códigos repetidos
		if (array_search($promotionalCode, $this->promotionalCodes)===FALSE) {
			$this->promotionalCodes[] = $promotionalCode;
		}
	}
	
	// Quitar código del carro
	public function removePromotionalCode($promotionalCode) {
		$found = array_search($promotionalCode, $this->promotionalCodes);
		if ($found!==FALSE) {
			array_splice($this->promotionalCodes, $found, 1);
		}
	}
	
	// Determina si existe el elemento indicado
	public function contain($type, $id) {
		return array_key_exists($type.'-'.$id, $this->items);
	}
	
	// Obtener el importe subtotal del carro (precios sin códigos promocionales)
	public function getSubtotal() {
		$subtotal = 0;
		foreach ($this as $item) {
			if ($item->getPrice()!==NULL) {
				$subtotal += round($item->getQuantity()*$item->getPrice(), 2);
			}
		}
		return $subtotal;
	}

	// Obtener el importe total del carro (teniendo en cuenta los códigos promocionales)
	public function getTotal() {
		$total = 0;
		foreach ($this as $item) {
			if ($item->getPromotionalPrice()!==NULL) {
				$total += round($item->getQuantity()*$item->getPromotionalPrice(), 2);
			} elseif ($item->getPrice()!==NULL) {
				$total += round($item->getQuantity()*$item->getPrice(), 2);
			}
		}
		return $total;
	}

	public function toJSON($em, $currency) {
		$this->hydrate($em, $currency);
		
		$json = array(
			'items' => array(),
			'codigos' => $this->promotionalCodes,
			'subtotal' => $this->getSubtotal(),
			'total' => $this->getTotal()
		);
		
		foreach ($this->items as $item) {
			$json['items'][] = array(
				'tipo' => $item->getType(),
				'id' => $item->getId(),
				'descripcion' => $item->getDescription(),
				'cantidad' => $item->getQuantity(),
				'precio' => $item->getPrice(),
				'precioPromocional' => $item->getPromotionalPrice()
			);
		}
		return json_encode($json);
	}
	
	// Cargar las descripciones y precios desde el almacén de datos
	// Para minimizar la memoria usada, la lista se gestiona sin ambos datos
	public function hydrate($em, $currency) {
		// Crear lista con los id para minimizar el acceso al almacén de datos
		$productIDs = array();
		$packIDs = array();
		foreach ($this->items as $item) {
			if ($item->getType()==ShoppingCartTypeType::product) {
				$productIDs[] = $item->getId();
			} elseif ($item->getType()==ShoppingCartTypeType::pack) {
				$packIDs[] = $item->getId();
			}
		}

		// Si existen productos en la lista
		if (count($productIDs)!=0) {
			// Obtenemos los productos y sus códigos promocionales
			if (count($this->promotionalCodes)===0) {
				$sql = 'select p
					from Spanischool\Entity\Producto p
					where p.id in (:productIDs)';
				$productList = $em->createQuery($sql)
					->setParameter('productIDs', $productIDs)
					->getResult();
			} else {
				$sql = 'select p, cp
					from Spanischool\Entity\Producto p
					left join p.codigosPromocionales cp
					where p.id in (:productIDs)
					and cp.codigo in (:promotionalCodes)';
			    $productList = $em->createQuery($sql)
					->setParameter('productIDs', $productIDs)
					->setParameter('promotionalCodes', $this->promotionalCodes)
					->getResult();
			}
				
			// Por cada producto de la lista
			foreach ($productList as $product) {
				$item = $this->items[ShoppingCartTypeType::product.'-'.$product->getId()];
                                $item->setNivel($product->getNivel());
                                $item->setHoras($product->getHoras());
				// Refrescamos las descripciones y precios de los productos
				$item->setDescription($product->getNombre());
				switch ($currency) {
					case 'EUR': $item->setPrice($product->getPrecioEUR()); break;
					case 'USD': $item->setPrice($product->getPrecioUSD()); break;
					case 'RBR': $item->setPrice($product->getPrecioRBR()); break;
					case 'GBP': $item->setPrice($product->getPrecioGBP()); break;
                }

				// Inicializar el precio promocional
				$item->setPromotionalPrice(NULL);
				// Si tiene algún código promocional
				if (!$product->getCodigosPromocionales()->isEmpty()) {
					// Por cada código promocional indicado en el carrito
					foreach ($this->promotionalCodes as $promotionalCodeCart) {
						// Por cada código promocional del producto actual
						foreach ($product->getCodigosPromocionales() as $promotionalCodeProduct) {
							// Si coinciden, se aplica dicho código
							if ($promotionalCodeProduct->getCodigo()==$promotionalCodeCart) {
								switch ($currency) {
									case 'EUR': $item->setPromotionalPrice($promotionalCodeProduct->getPrecioEUR()); break;
									case 'USD': $item->setPromotionalPrice($promotionalCodeProduct->getPrecioUSD()); break;
									case 'RBR': $item->setPromotionalPrice($promotionalCodeProduct->getPrecioRBR()); break;
									case 'GBP': $item->setPromotionalPrice($promotionalCodeProduct->getPrecioGBP()); break;
								}
							}
						}
					}
				}
			}
		}

		// Si existen paquetes en la lista
		if (count($packIDs)!=0) {
			// Obtenemos los paquetes
			$packList = $em->createQuery('select p from Spanischool\Entity\Paquete p where p.id in (:packIDs)')
				->setParameter('packIDs', $packIDs)
				->getResult();
			// Refrescamos las descripciones y precios de los paquetes
			foreach ($packList as $pack) {
				$item = $this->items[ShoppingCartTypeType::pack.'-'.$pack->getId()];
				$item->setDescription($pack->getDescripcion());
				switch ($currency) {
					case 'EUR': $item->setPrice($pack->getPrecioEUR()); break;
					case 'USD': $item->setPrice($pack->getPrecioUSD()); break;
					case 'RBR': $item->setPrice($pack->getPrecioRBR()); break;
					case 'GBP': $item->setPrice($pack->getPrecioGBP()); break;
				}
			}
		}
	}
        
        public function length(){
            return count($this->items);
        }
        

	// Interfaz Iterator
	public function current() { return current($this->items); }
	public function next() { return next($this->items); }
	public function key() { return key($this->items); }
	public function valid() { return key($this->items)!==NULL; }
	public function rewind() { return reset($this->items); }
}
