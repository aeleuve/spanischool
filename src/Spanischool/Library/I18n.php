<?php
namespace Spanischool\Library;

class I18n {
	public static function dict($locale) {
		setlocale(LC_MESSAGES, $locale);
		putenv('LC_MESSAGES='.$locale);
        bindtextdomain("messages", __DIR__.'/../../../views/i18n');
        textdomain("messages");
        bind_textdomain_codeset("messages", "UTF-8");
	}
}
