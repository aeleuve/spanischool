<?php
namespace Spanischool\Library;

use Spanischool\Types\ShoppingCartTypeType;

use Serializable;

// Elemento del carro de la compra
class ShoppingCartItem {

	private $type;
	public function getType() { return $this->type; }
	public function setType($type) { $this->type = $type; }

	private $id;
	public function getId() { return $this->id; }
	public function setId($id) { $this->id = $id; }

	private $description;
	public function getDescription() { return $this->description; }
	public function setDescription($description) { $this->description = $description; }

	private $quantity;
	public function getQuantity() { return $this->quantity; }
	public function setQuantity($quantity) { $this->quantity = $quantity; }
        
        private $price;
	public function getPrice() { return $this->price; }
	public function setPrice($price) { $this->price = $price; }

	private $promotionalPrice;
	public function getPromotionalPrice() { return $this->promotionalPrice; }
	public function setPromotionalPrice($promotionalPrice) { $this->promotionalPrice = $promotionalPrice; }
        
    private $nivel;
    public function getNivel() { return $this->nivel; }
    public function setNivel($nivel) { $this->nivel = $nivel; }
       
    private $horas;
    public function getHoras() { return $this->horas; }
    public function setHoras($horas) { $this->horas = $horas; }

	public function __construct($type, $id, $quantity) {
		$this->type = $type;
		$this->id = $id;
		$this->description = NULL;
		$this->quantity = $quantity;
		$this->price = NULL;
		$this->promotionalPrice = NULL;
	}
        
        
}
