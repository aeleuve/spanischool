<?php
namespace Spanischool;
use Doctrine\Common\Cache\MemcacheCache;
use Doctrine\Common\Cache\ArrayCache;
use Doctrine\Common\Cache\ApcCache;

class Configuration
{
    private $environment;
    public function __construct($environment){
        $this->environment = $environment;
    }
    public function swiftMailer()
    {
        $config = array(
            'PRODUCTION'  => array(
                'host' => 'mail',
                'username' => 'authvar@grupogdt.com',
                'password' => 'authpass'
            ),
            'DEVELOPMENT' => array(
                'host' => 'mail.grupogdt.com',
                'username' => 'authvar@grupogdt.com',
                'password' => 'authpass'
            )
        );
       return $config[$this->environment]; 
    }
    public function payGateway()
    {
        $config = array(
            'PRODUCTION' => array(
                'tienda'       => '06d60c7b369f3dbb866d4b3fde9e7aad',
                'paypal'       => '54ee6d84b7a30194cee538db1e73846e',
                'url_pasarela' => 'http://pagos.formaciondigital.com/service/saveCart'
            ),
            'DEVELOPMENT' => array(
                'tienda'       => '222e29fc733b01b672eb0678b1c1e076',
                'paypal'       => 'b00d1af9ba7e6d1d9c79a57de0c108dd',
                'url_pasarela' => 'http://servicepay/service/saveCart'
            )
        );
        return $config[$this->environment];
    }

    public function mail(){
        return 'info@spanischool.com';
    }
    public function doctrine(){
        $connectionOptions = array(
            'PRODUCTION'  => array(
                'driver' => 'pdo_mysql',
                'dbname'        => 'fd_spanischool_portal',
                'user'          => 'plataformas',
                'password'      => 'xYzplat8876qWe',
                'host'          => 'db1',
                'charset'       => 'utf8',
                'driverOptions' => array(
                    \PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\''
                )
            ),
            'DEVELOPMENT' => array(
                'driver' => 'pdo_mysql',
                'dbname'        => 'spanischool2012',
                'user'          => 'spanischool2012',
                'password'      => 'spanischool2012',
                'host'          => 'webfr',
                'charset'       => 'utf8',
                'driverOptions' => array(
                    \PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\''
                )
            )
        );
        return $connectionOptions[$this->environment];
    }

    public function doctrineProxiesPath(){
        return __DIR__.'/Entity/Proxy';
    }

    public function doctrineEntitiesPath(){
        return array(__DIR__.'/Entity');
    }

    public function doctrineProxiesNamespace(){
        return 'Spanischool\Entity\Proxy';
    }

    public function doctrineCache(){
        if($this->environment=='PRODUCTION'){
            $cache = new MemcacheCache();
            $memcache = new \Memcache();
            $memcache->connect('10.2.9.12', 11211);
            $cache->setMemcache($memcache);
            $cache->setNamespace('spanischool');
            return $cache;
        }
        $cache = new ArrayCache();
        return $cache;
    }

    public function twig(){
        $config = array(
            'twig.path' => __DIR__.'/../../views',
            'twig.class_path' => __DIR__.'/../../vendor/twig/twig/lib',
            'twig.options' => array()
        );
        if($this->environment == 'PRODUCTION'){
            $config['twig.options']['cache'] = __DIR__.'/../../cache/twig';
        }
        return $config;
    }
}
?>
