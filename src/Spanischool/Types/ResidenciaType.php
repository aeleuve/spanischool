<?php
namespace Spanischool\Types;

class ResidenciaType extends Enumeration {
	
	const opcional = 'Opcional';
	const incluida = 'Incluida';
	
	public static function valueOf($value) {
		return parent::valueOf(new ResidenciaType(), $value);
	}
	
	public static function toArray() {
		return parent::toArray(new ResidenciaType());
	}
}
