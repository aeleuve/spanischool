<?php
namespace Spanischool\Types;

class ShoppingCartTypeType extends Enumeration {
	
	const product = 'product';
	const pack = 'pack';
	
	public static function valueOf($value) {
		return parent::valueOf(new ShoppingCartTypeType(), $value);
	}
	
	public static function toArray() {
		return parent::toArray(new ShoppingCartTypeType());
	}
}
