<?php
namespace Spanischool\Types;

use \ReflectionClass;

class Enumeration {

	public static function valueOf($class, $value) {
		$reflectionClass = new ReflectionClass($class);
		
		$constants = $reflectionClass->getConstants();
		if (array_key_exists($value, $constants)) {
			return $constants[$value];
		} else {
			return NULL;
		}
	}
	
	public static function toArray($class) {
		$reflectionClass = new ReflectionClass($class);
		return $reflectionClass->getConstants();
	}
}
