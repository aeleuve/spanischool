<?php
namespace Spanischool\Types;

class TipoProductoType extends Enumeration {
	
	const curso    = 'Curso';
	const oneToOne = 'One-To-One';
	const examen   = 'Examen';
	const pildora  = 'Píldora';
	
	public static function valueOf($value) {
		return parent::valueOf(new TipoProductoType(), $value);
	}
	
	public static function toArray() {
		return parent::toArray(new TipoProductoType());
	}
}
