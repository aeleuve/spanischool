<?php
namespace Spanischool\Exception;

use \Exception;
use \Iterator;

class ApplicationException extends Exception implements Iterator {

	private $messages;
	
	public function __construct() {
		$this->messages = array();
	}
	
	public function add($message) {
		$this->messages[] = $message;
	}
	
	public function getMessages() {
		return $this->messages;
	}
	
	public function isEmpty() {
		return count($this->messages)==0;
	}
	
	// Para hacerla iterable
	public function current() { return current($this->messages); }
	public function next() { return next($this->messages); }
	public function key() { return key($this->messages); }
	public function valid() { return key($this->messages)!==NULL; }
	public function rewind() { return reset($this->messages); }
}
