<?php
namespace Spanischool\Entity;

use Spanischool\Library\TypeValidation;
use Spanischool\Exception\ApplicationException;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * @Entity
 * @Table(name="paquetes")
 * @HasLifecycleCallbacks
 */
class Paquete {

    /**
     * @Id
     * @Column(type="integer")
     * @GeneratedValue(strategy="AUTO")
     */
    private $id;
    public function getId() { return $this->id; }
    private function setId($id) { $this->id = $id; }
   
    /**
     * @Column(type="string", length=60)
     */
    private $descripcion;
    const descripcion_LENGTH = 60;
    public function getDescripcion() { return $this->descripcion; }
    public function setDescripcion($descripcion) { $this->descripcion = $descripcion; }

    /**
     * @Column(name="precio_eur", type="decimal", precision=10, scale=2, nullable=true)
     */
    private $precioEUR;
    public function getPrecioEUR() { return $this->precioEUR; }
    public function setPrecioEUR($precioEUR) { $this->precioEUR = $precioEUR; }
    
    /**
     * @Column(name="precio_usd", type="decimal", precision=10, scale=2, nullable=true)
     */
    private $precioUSD;
    public function getPrecioUSD() { return $this->precioUSD; }
    public function setPrecioUSD($precioUSD) { $this->precioUSD = $precioUSD; }
    
    /**
     * @Column(name="precio_rbr", type="decimal", precision=10, scale=2, nullable=true)
     */
    private $precioRBR;
    public function getPrecioRBR() { return $this->precioRBR; }
    public function setPrecioRBR($precioRBR) { $this->precioRBR = $precioRBR; }
    
    /**
     * @Column(name="precio_gbp", type="decimal", precision=10, scale=2, nullable=true)
     */
    private $precioGBP;
    public function getPrecioGBP() { return $this->precioGBP; }
    public function setPrecioGBP($precioGBP) { $this->precioGBP = $precioGBP; }
    
    /**
     * @Column(type="string", length=20, nullable=true)
     */
    private $ahorro;
    const ahorro_LENGTH = 20;
    public function getAhorro() { return $this->ahorro; }
    public function setAhorro($ahorro) { $this->ahorro = $ahorro; }

    /**
     * @Column(type="boolean", nullable=true)
     */
    private $activo;
    public function getActivo() { return $this->activo; }
    public function setActivo($activo) { $this->activo = $activo; }

    /**
     * @ManyToMany(targetEntity="Producto")
     * @JoinTable(name="r_paquetes_productos",
     *      joinColumns={@JoinColumn(name="paquete_id", referencedColumnName="id")},
     *      inverseJoinColumns={@JoinColumn(name="producto_id", referencedColumnName="id")}
     *      )
     **/
    private $productos;
    public function getProductos() { return $this->productos; }
    public function addProducto($producto) { $this->productos[] = $producto; }

    public function __construct() {
		$this->productos = new ArrayCollection();
    }

    /**
	 * @PrePersist
	 * @PreUpdate
	 */
	public function oneEventListener() {
		$errors = new ApplicationException();
		
		if (!$this->descripcion) {
			$errors->add('La descripción es obligatoria');
		} else if (strlen($this->descripcion)>self::descripcion_LENGTH) {
			$errors->add('La descripción no es válida');
		}
				
		if ($this->precioEUR && !TypeValidation::isFloat($this->precioEUR)) {
			$errors->add('El precio en euros no es válido');
		}
				
		if ($this->precioUSD && !TypeValidation::isFloat($this->precioUSD)) {
			$errors->add('El precio en dólares no es válido');
		}
				
		if ($this->precioRBR && !TypeValidation::isFloat($this->precioRBR)) {
			$errors->add('El precio en reales brasileños no es válido');
		}

		if ($this->precioGBP && !TypeValidation::isFloat($this->precioGBP)) {
			$errors->add('El precio en libras no es válido');
		}

		if (!$this->precioEUR && !$this->precioUSD && !$this->precioRBR && !$this->precioGBP) {
			$errors->add('Es obligatorio indicar al menos un precio');
		}
		
		if ($this->ahorro && strlen($this->ahorro)>self::ahorro_LENGTH) {
			$errors->add('El texto de ahorro no es válido');
		}

		if ($this->activo===NULL) {
			$errors->add('El indicador de activo es obligatorio');
		}
				
		if (!$errors->isEmpty()) {
			throw $errors;
		}
	}
}
