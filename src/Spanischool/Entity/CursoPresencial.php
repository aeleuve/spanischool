<?php
namespace Spanischool\Entity;

use Spanischool\Library\TypeValidation;
use Spanischool\Exception\ApplicationException;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * @Entity
 * @Table(name="cursos_presenciales")
 * @HasLifecycleCallbacks
 */
class CursoPresencial {

    /**
     * @Id
     * @Column(type="integer")
     * @GeneratedValue(strategy="AUTO")
     */
    private $id;
    public function getId() { return $this->id; }
    private function setId($id) { $this->id = $id; }
   
    /**
     * @Column(type="string", length=300)
     */
    private $nombre;
    const nombre_LENGTH = 300;
    public function getNombre() { return $this->nombre; }
    public function setNombre($nombre) { $this->nombre = $nombre; }

    /**
     * @Column(type="string", length=10)
     */
    private $nivel;
    const nivel_LENGTH = 10;
    public function getNivel() { return $this->nivel; }
    public function setNivel($nivel) { $this->nivel = $nivel; }
    
    /**
     * @ManyToMany(targetEntity="CursoPresencialDetalle", cascade={"persist", "remove", "merge"})
     * @JoinTable(name="r_cursopresencial_cursopresencialdetalle",
     *      joinColumns={@JoinColumn(name="curso_presencial_id", referencedColumnName="id")},
     *      inverseJoinColumns={@JoinColumn(name="curso_presencial_detalle_id", referencedColumnName="id")}
     *      )
	 * @OrderBy({"id"="ASC"})
     */
    private $detalles;
    public function getDetalles() { return $this->detalles; }
    public function addDetalle($detalle) { $this->detalles[] = $detalle; }
    
    public function __construct() {
		$this->detalles = new ArrayCollection();
    }
    
    /**
	 * @PrePersist
	 * @PreUpdate
	 */
	public function oneEvent1istener() {
		$errors = new ApplicationException();
		
		if (!$this->nombre) {
			$errors->add('El nombre es obligatorio');
		} else if (strlen($this->nombre)>self::nombre_LENGTH) {
			$errors->add('El nombre no es válido');
		}
		
		if (!$this->nivel) {
			$errors->add('El nivel es obligatorio');
		} else if (strlen($this->nivel)>self::nivel_LENGTH) {
			$errors->add('El nivel no es válido');
		}
		
		if (!$errors->isEmpty()) {
			throw $errors;
		}
	}
}
