<?php
namespace Spanischool\Entity;

use Spanischool\Library\TypeValidation;
use Spanischool\Exception\ApplicationException;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * @Entity
 * @Table(name="carteles_presenciales")
 * @HasLifecycleCallbacks
 */
class CartelPresencial {

    /**
     * @Id
     * @Column(type="integer")
     * @GeneratedValue(strategy="AUTO")
     */
    private $id;
    public function getId() { return $this->id; }
    private function setId($id) { $this->id = $id; }
   
    /**
     * @Column(type="string", length=100)
     */
    private $fichero;
    public function getFichero() { return $this->fichero; }
    public function setFichero($fichero) { $this->fichero = $fichero; }

    /**
     * @Column(type="boolean")
     */
    private $activo;
    public function getActivo() { return $this->activo; }
    public function setActivo($activo) { $this->activo = $activo; }

   /**
     * @Column(type="string",length=2)
     */
    private $lang;
    public function getLang() { return $this->lang; }
    public function setLang($lang) { $this->lang = $lang; }
	public function getLangBeauty(){
		$idiomas = array('es'=>'Español', 'en'=>'Inglés', 'pt'=>'Portugués');
		return $idiomas[$this->lang];
	}

	/**
	 * @PrePersist
	 * @PreUpdate
	 */
	public function oneEventListener() {
		$errors = new ApplicationException();
		
		if (!$this->fichero) {
			$errors->add('El fichero es obligatorio');
		}
		
		if ($this->activo===NULL) {
			$errors->add('El indicador de activo es obligatorio');
		}

		if (!in_array($this->lang, array('es','en','pt'))) {
			$errors->add('El idioma no es válido');
		}
				
		if (!$errors->isEmpty()) {
			throw $errors;
		}
	}
}
