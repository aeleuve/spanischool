<?php
namespace Spanischool\Entity;

use Spanischool\Types\TipoProductoType;

use Spanischool\Library\TypeValidation;
use Spanischool\Exception\ApplicationException;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * @Entity
 * @Table(name="productos")
 * @HasLifecycleCallbacks
 */
class Producto {

    /**
     * @Id
     * @Column(type="integer")
     * @GeneratedValue(strategy="AUTO")
     */
    private $id;
    public function getId() { return $this->id; }
    private function setId($id) { $this->id = $id; }
   
    /**
     * @Column(type="string", length=300)
     */
    private $nombre;
    const nombre_LENGTH = 300;
    public function getNombre() { return $this->nombre; }
    public function setNombre($nombre) { $this->nombre = $nombre; }

    /**
     * @Column(name="codigo_plataforma", type="string", length=50, nullable=true)
     */
    private $codigoPlataforma;
    const codigoPlataforma_LENGTH = 50;
    public function getCodigoPlataforma() { return $this->codigoPlataforma; }
    public function setCodigoPlataforma($codigoPlataforma) { $this->codigoPlataforma = $codigoPlataforma; }

    /**
     * @Column(type="string", length=10, nullable=true)
     */
    private $nivel;
    const nivel_LENGTH = 10;
    public function getNivel() { return $this->nivel; }
    public function setNivel($nivel) { $this->nivel = $nivel; }
    
    /**
     * @Column(type="decimal", precision=5, scale=2, nullable=true)
     */
    private $horas;
    public function getHoras() { return $this->horas; }
    public function setHoras($horas) { $this->horas = $horas; }
    
    /**
     * @Column(name="dias_acceso", type="smallint", nullable=true)
     */
    private $diasAcceso;
    public function getDiasAcceso() { return $this->diasAcceso; }
    public function setDiasAcceso($diasAcceso) { $this->diasAcceso = $diasAcceso; }
    
    /**
     * @Column(name="precio_eur", type="decimal", precision=10, scale=2, nullable=true)
     */
    private $precioEUR;
    public function getPrecioEUR() { return $this->precioEUR; }
    public function setPrecioEUR($precioEUR) { $this->precioEUR = $precioEUR; }
    
    /**
     * @Column(name="precio_usd", type="decimal", precision=10, scale=2, nullable=true)
     */
    private $precioUSD;
    public function getPrecioUSD() { return $this->precioUSD; }
    public function setPrecioUSD($precioUSD) { $this->precioUSD = $precioUSD; }
    
    /**
     * @Column(name="precio_rbr", type="decimal", precision=10, scale=2, nullable=true)
     */
    private $precioRBR;
    public function getPrecioRBR() { return $this->precioRBR; }
    public function setPrecioRBR($precioRBR) { $this->precioRBR = $precioRBR; }
    
    /**
     * @Column(name="precio_gbp", type="decimal", precision=10, scale=2, nullable=true)
     */
    private $precioGBP;
    public function getPrecioGBP() { return $this->precioGBP; }
    public function setPrecioGBP($precioGBP) { $this->precioGBP = $precioGBP; }
    
    /**
     * @Column(type="string", length=8)
     */
    private $tipo;
    public function getTipo() { return $this->tipo; }
    public function setTipo($tipo) { $this->tipo = $tipo; }
    
    /**
     * @ManyToMany(targetEntity="CodigoPromocional", cascade={"persist", "remove", "merge"}, orphanRemoval=true)
     * @JoinTable(name="r_producto_codigopromocional",
     *      joinColumns={@JoinColumn(name="producto_id", referencedColumnName="id")},
     *      inverseJoinColumns={@JoinColumn(name="codigo_promocional_id", referencedColumnName="id")}
     *      )
	 * @OrderBy({"id"="ASC"})
     */
    private $codigosPromocionales;
    public function getCodigosPromocionales() { return $this->codigosPromocionales; }
    public function addCodigoPromocional($codigoPromocional) { $this->codigosPromocionales[] = $codigoPromocional; }
    
	/**
     * @OneToOne(targetEntity="Paquete", cascade={"persist", "remove", "merge"}, orphanRemoval=true)
     * @JoinColumn(name="paquete_id")
     */
    private $paquete;
    public function getPaquete() { return $this->paquete; }
    public function setPaquete($paquete) { $this->paquete = $paquete; }

    public function __construct() {
		$this->codigosPromocionales = new ArrayCollection();
    }

    /**
	 * @PrePersist
	 * @PreUpdate
	 */
	public function oneEvent1istener() {
		$errors = new ApplicationException();
		
		if (!$this->nombre) {
			$errors->add('El nombre es obligatorio');
		} else if (strlen($this->nombre)>self::nombre_LENGTH) {
			$errors->add('El nombre no es válido');
		}
		
		if ($this->codigoPlataforma && strlen($this->codigoPlataforma)>self::codigoPlataforma_LENGTH) {
			$errors->add('El código de plataforma no es válido');
		}
		
		if ($this->nivel && strlen($this->nivel)>self::nivel_LENGTH) {
			$errors->add('El nivel no es válido');
		}

		if ($this->horas && !TypeValidation::isFloat($this->horas)) {
			$errors->add('El número de horas no es válido');
		}
		
		if ($this->diasAcceso && !TypeValidation::isInteger($this->diasAcceso)) {
			$errors->add('El número de días de acceso no es válido');
		}

		if ($this->precioEUR && !TypeValidation::isFloat($this->precioEUR)) {
			$errors->add('El precio en euros no es válido');
		}
				
		if ($this->precioUSD && !TypeValidation::isFloat($this->precioUSD)) {
			$errors->add('El precio en dólares no es válido');
		}
				
		if ($this->precioRBR && !TypeValidation::isFloat($this->precioRBR)) {
			$errors->add('El precio en reales brasileños no es válido');
		}

		if ($this->precioGBP && !TypeValidation::isFloat($this->precioGBP)) {
			$errors->add('El precio en libras no es válido');
		}

		if ($this->precioEUR===NULL && $this->precioUSD===NULL && $this->precioRBR===NULL && $this->precioGBP===NULL) {
			$errors->add('Es obligatorio indicar al menos un precio');
		}
		
		if (!$this->tipo) {
			$errors->add('El tipo es obligatorio');
		} elseif (TipoProductoType::valueOf($this->tipo)===NULL) {
			$errors->add('El tipo no es válido');
		}

		if (!$errors->isEmpty()) {
			throw $errors;
		}
	}
}
