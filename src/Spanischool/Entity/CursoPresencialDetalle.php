<?php
namespace Spanischool\Entity;

use Spanischool\Types\ResidenciaType;

use Spanischool\Library\TypeValidation;
use Spanischool\Exception\ApplicationException;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * @Entity
 * @Table(name="cursos_presenciales_detalles")
 * @HasLifecycleCallbacks
 */
class CursoPresencialDetalle {

    /**
     * @Id
     * @Column(type="integer")
     * @GeneratedValue(strategy="AUTO")
     */
    private $id;
    public function getId() { return $this->id; }
    private function setId($id) { $this->id = $id; }
   
    /**
     * @Column(type="string", length=20)
     */
    private $semanas;
    const semanas_LENGTH = 20;
    public function getSemanas() { return $this->semanas; }
    public function setSemanas($semanas) { $this->semanas = $semanas; }

    /**
     * @Column(name="horas", type="smallint")
     */
    private $horas;
    public function getHoras() { return $this->horas; }
    public function setHoras($horas) { $this->horas = $horas; }
    
    /**
     * @Column(type="string", length=8)
     */
    private $residencia;
    public function getResidencia() { return $this->residencia; }
    public function setResidencia($residencia) { $this->residencia = $residencia; }

    /**
     * @Column(name="precio_eur", type="decimal", precision=10, scale=2, nullable=true)
     */
    private $precioEUR;
    public function getPrecioEUR() { return $this->precioEUR; }
    public function setPrecioEUR($precioEUR) { $this->precioEUR = $precioEUR; }
    
    /**
     * @Column(name="precio_usd", type="decimal", precision=10, scale=2, nullable=true)
     */
    private $precioUSD;
    public function getPrecioUSD() { return $this->precioUSD; }
    public function setPrecioUSD($precioUSD) { $this->precioUSD = $precioUSD; }
    
    /**
     * @Column(name="precio_rbr", type="decimal", precision=10, scale=2, nullable=true)
     */
    private $precioRBR;
    public function getPrecioRBR() { return $this->precioRBR; }
    public function setPrecioRBR($precioRBR) { $this->precioRBR = $precioRBR; }
    
    /**
     * @Column(name="precio_gbp", type="decimal", precision=10, scale=2, nullable=true)
     */
    private $precioGBP;
    public function getPrecioGBP() { return $this->precioGBP; }
    public function setPrecioGBP($precioGBP) { $this->precioGBP = $precioGBP; }
        
	/**
	 * @PrePersist
	 * @PreUpdate
	 */
	public function oneEvent1istener() {
		$errors = new ApplicationException();
		
		if (!$this->semanas) {
			$errors->add('El número de semanas es obligatorio');
		} else if (strlen($this->semanas)>self::semanas_LENGTH) {
			$errors->add('El número de semanas no es válido');
		}
		
		if ($this->horas===NULL) {
			$errors->add('El número de horas es obligatorio');
		} elseif (!TypeValidation::isInteger($this->horas)) {
			$errors->add('El número de horas no es válido');
		}

		if (!$this->residencia) {
			$errors->add('La residencia es obligatoria');
		} elseif (ResidenciaType::valueOf($this->residencia)===NULL) {
			$errors->add('La residencia no es válida');
		}

		if ($this->precioEUR && !TypeValidation::isFloat($this->precioEUR)) {
			$errors->add('El precio en euros no es válido');
		}
				
		if ($this->precioUSD && !TypeValidation::isFloat($this->precioUSD)) {
			$errors->add('El precio en dólares no es válido');
		}
				
		if ($this->precioRBR && !TypeValidation::isFloat($this->precioRBR)) {
			$errors->add('El precio en reales brasileños no es válido');
		}

		if ($this->precioGBP && !TypeValidation::isFloat($this->precioGBP)) {
			$errors->add('El precio en libras no es válido');
		}

		if ($this->precioEUR===NULL && $this->precioUSD===NULL && $this->precioRBR===NULL && $this->precioGBP===NULL) {
			$errors->add('Es obligatorio indicar al menos un precio');
		}
		
		if (!$errors->isEmpty()) {
			throw $errors;
		}
	}
}
