<?php
namespace Spanischool\Entity;

use Spanischool\Library\TypeValidation;
use Spanischool\Exception\ApplicationException;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * @Entity
 * @Table(name="comunes_presenciales")
 * @HasLifecycleCallbacks
 */
class ComunPresencial {

    /**
     * @Id
     * @Column(type="integer")
     * @GeneratedValue(strategy="AUTO")
     */
    private $id;
    public function getId() { return $this->id; }
    private function setId($id) { $this->id = $id; }
   
    /**
     * @Column(type="string", length=300)
     */
    private $nombre;
    const nombre_LENGTH = 300;
    public function getNombre() { return $this->nombre; }
    public function setNombre($nombre) { $this->nombre = $nombre; }

    /**
     * @Column(name="precio_eur", type="decimal", precision=10, scale=2, nullable=true)
     */
    private $precioEUR;
    public function getPrecioEUR() { return $this->precioEUR; }
    public function setPrecioEUR($precioEUR) { $this->precioEUR = $precioEUR; }
    
    /**
     * @Column(name="precio_usd", type="decimal", precision=10, scale=2, nullable=true)
     */
    private $precioUSD;
    public function getPrecioUSD() { return $this->precioUSD; }
    public function setPrecioUSD($precioUSD) { $this->precioUSD = $precioUSD; }
    
    /**
     * @Column(name="precio_rbr", type="decimal", precision=10, scale=2, nullable=true)
     */
    private $precioRBR;
    public function getPrecioRBR() { return $this->precioRBR; }
    public function setPrecioRBR($precioRBR) { $this->precioRBR = $precioRBR; }
    
    /**
     * @Column(name="precio_gbp", type="decimal", precision=10, scale=2, nullable=true)
     */
    private $precioGBP;
    public function getPrecioGBP() { return $this->precioGBP; }
    public function setPrecioGBP($precioGBP) { $this->precioGBP = $precioGBP; }
    
	/**
	 * @PrePersist
	 * @PreUpdate
	 */
	public function oneEvent1istener() {
		$errors = new ApplicationException();
		
		if (!$this->nombre) {
			$errors->add('El nombre es obligatorio');
		} else if (strlen($this->nombre)>self::nombre_LENGTH) {
			$errors->add('El nombre no es válido');
		}
		
		if ($this->precioEUR && !TypeValidation::isFloat($this->precioEUR)) {
			$errors->add('El precio en euros no es válido');
		}
				
		if ($this->precioUSD && !TypeValidation::isFloat($this->precioUSD)) {
			$errors->add('El precio en dólares no es válido');
		}
				
		if ($this->precioRBR && !TypeValidation::isFloat($this->precioRBR)) {
			$errors->add('El precio en reales brasileños no es válido');
		}

		if ($this->precioGBP && !TypeValidation::isFloat($this->precioGBP)) {
			$errors->add('El precio en libras no es válido');
		}

		if ($this->precioEUR===NULL && $this->precioUSD===NULL && $this->precioRBR===NULL && $this->precioGBP===NULL) {
			$errors->add('Es obligatorio indicar al menos un precio');
		}

		if (!$errors->isEmpty()) {
			throw $errors;
		}
	}
}
