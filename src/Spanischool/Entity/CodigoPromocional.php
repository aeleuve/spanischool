<?php
namespace Spanischool\Entity;

use Spanischool\Library\TypeValidation;
use Spanischool\Exception\ApplicationException;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * @Entity
 * @Table(name="codigos_promocionales")
 * @HasLifecycleCallbacks
 */
class CodigoPromocional {

    /**
     * @Id
     * @Column(type="integer")
     * @GeneratedValue(strategy="AUTO")
     */
    private $id;
    public function getId() { return $this->id; }
    private function setId($id) { $this->id = $id; }
   
    /**
     * @Column(type="string", length=50)
     */
    private $codigo;
    const codigo_LENGTH = 50;
    public function getCodigo() { return $this->codigo; }
    public function setCodigo($codigo) { $this->codigo = $codigo; }

    /**
     * @Column(type="string", length=50)
     */
    private $descripcion;
    const descripcion_LENGTH = 50;
    public function getDescripcion() { return $this->descripcion; }
    public function setDescripcion($descripcion) { $this->descripcion = $descripcion; }

    /**
     * @Column(name="precio_eur", type="decimal", precision=10, scale=2, nullable=true)
     */
    private $precioEUR;
    public function getPrecioEUR() { return $this->precioEUR; }
    public function setPrecioEUR($precioEUR) { $this->precioEUR = $precioEUR; }
    
    /**
     * @Column(name="precio_usd", type="decimal", precision=10, scale=2, nullable=true)
     */
    private $precioUSD;
    public function getPrecioUSD() { return $this->precioUSD; }
    public function setPrecioUSD($precioUSD) { $this->precioUSD = $precioUSD; }
    
    /**
     * @Column(name="precio_rbr", type="decimal", precision=10, scale=2, nullable=true)
     */
    private $precioRBR;
    public function getPrecioRBR() { return $this->precioRBR; }
    public function setPrecioRBR($precioRBR) { $this->precioRBR = $precioRBR; }
    
    /**
     * @Column(name="precio_gbp", type="decimal", precision=10, scale=2, nullable=true)
     */
    private $precioGBP;
    public function getPrecioGBP() { return $this->precioGBP; }
    public function setPrecioGBP($precioGBP) { $this->precioGBP = $precioGBP; }
    
    /**
	 * @PrePersist
	 * @PreUpdate
	 */
	public function oneEventListener() {
		$errors = new ApplicationException();
		
		if (!$this->codigo) {
			$errors->add('El codigo es obligatorio');
		} else if (strlen($this->codigo)>self::codigo_LENGTH) {
			$errors->add('El código no es válido');
		}
		
		if (!$this->descripcion) {
			$errors->add('La descripción es obligatoria');
		} else if (strlen($this->descripcion)>self::descripcion_LENGTH) {
			$errors->add('La descripción no es válida');
		}
				
		if ($this->precioEUR && !TypeValidation::isFloat($this->precioEUR)) {
			$errors->add('El precio en euros no es válido');
		}
				
		if ($this->precioUSD && !TypeValidation::isFloat($this->precioUSD)) {
			$errors->add('El precio en dólares no es válido');
		}
				
		if ($this->precioRBR && !TypeValidation::isFloat($this->precioRBR)) {
			$errors->add('El precio en reales brasileños no es válido');
		}

		if ($this->precioGBP && !TypeValidation::isFloat($this->precioGBP)) {
			$errors->add('El precio en libras no es válido');
		}

		if (!$this->precioEUR && !$this->precioUSD && !$this->precioRBR && !$this->precioGBP) {
			$errors->add('Es obligatorio indicar al menos un precio');
		}
		
		if (!$errors->isEmpty()) {
			throw $errors;
		}
	}
}
