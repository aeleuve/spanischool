<?php
namespace Spanischool\Controllers\Manager;

use Spanischool\Library\TypeValidation;
use Spanischool\Exception\ApplicationException;
use Spanischool\Entity\CursoPresencial;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

use Silex\Application;
use Silex\ControllerProviderInterface;
use Silex\ControllerCollection;

class CursosPresenciales implements ControllerProviderInterface {
	
	public function connect(Application $app) {
        $controllers = new ControllerCollection();

        // Comprobación de usuario autenticado
		$userIsLogged = function (Request $request) use ($app) {
	        if ($app['session']->get('user')===NULL) {
		        return $app->redirect('/manager/login');
		    }
		};
	
        $controllers->get('/index', function (Application $app) {
   			return $app->redirect('main');
		})->middleware($userIsLogged);
        
        $controllers->get('/main', function (Application $app) {
			CursosPresenciales::setId($app, NULL);
			
        	$em = $app['em'];
        	
        	$cursosPresenciales = $em->createQuery('select cp from Spanischool\Entity\CursoPresencial cp order by cp.nombre, cp.id')->getResult();
        	
			return $app['twig']->render('Manager/CursosPresenciales/main.html.twig', array(
    			'cursosPresenciales' => $cursosPresenciales
    		));
		})->middleware($userIsLogged);
        
		$controllers->get('/view', function (Application $app) {
        	$id = $app['request']->get('id');
        	
        	$em = $app['em'];

       		$cursoPresencial = $em->find('Spanischool\Entity\CursoPresencial', $id);
       		if (!$cursoPresencial) {
       			return $app->redirect('main');
       		}
        	
			return $app['twig']->render('Manager/CursosPresenciales/view.html.twig', array(
    			'cursoPresencial' => $cursoPresencial
    		));
        })->middleware($userIsLogged);
        
        $controllers->get('/edit', function (Application $app) {
        	$id = $app['request']->get('id');
        	
        	if ($id) {
	        	$em = $app['em'];
	        	
        		$cursoPresencial = $em->find('Spanischool\Entity\CursoPresencial', $id);
        		
				return $app['twig']->render('Manager/CursosPresenciales/edit.html.twig', array(
        			'id' => $cursoPresencial->getId(),
	        		'nombre' => $cursoPresencial->getNombre(),
	        		'nivel' => $cursoPresencial->getNivel()
	    		));
        		
        	} else {
				return $app['twig']->render('Manager/CursosPresenciales/edit.html.twig');
        	}
        })->middleware($userIsLogged);

        $controllers->post('/save', function (Application $app) {
        	$id = $app['request']->get('id');
        	$nombre = trim($app['request']->get('nombre'));
        	$nivel = trim($app['request']->get('nivel'));
        	
        	// Validación
        	$messages = array();
			if ($nombre=='') {
				$messages['nombre'] = 'El nombre es obligatorio';
			} else if (strlen($nombre)>CursoPresencial::nombre_LENGTH) {
				$messages['nombre'] = 'El nombre no es válido';
			}
			if ($nivel=='') {
				$messages['nivel'] = 'El nivel es obligatorio';
			} else if (strlen($nivel)>CursoPresencial::nivel_LENGTH) {
				$messages['nivel'] = 'El nivel no es válido';
			}
			
			// Si no hay errores
			if (count($messages)==0) {
		       	$em = $app['em'];
	        	        	
	        	if ($id) {
	        		$cursoPresencial = $em->find('Spanischool\Entity\CursoPresencial', $id);
	        	} else {
	        		$cursoPresencial = new CursoPresencial();
	        	}
	        	
	        	$cursoPresencial->setNombre($nombre!=''? $nombre: NULL);
	        	$cursoPresencial->setNivel($nivel!=''? $nivel: NULL);

	        	try {
		        	$em->persist($cursoPresencial);
		        	$em->flush();
	        	} catch (ApplicationException $e) {
	        		print_r($e->getMessages());
	        		die();
	        	}

	        	return $app->redirect('view?id='.$cursoPresencial->getId());
	        	
			} else {
				return $app['twig']->render('Manager/CursosPresenciales/edit.html.twig', array(
	    			'id' => $id,
	        		'nombre' => $nombre,
	        		'nivel' => $nivel,
	        		'messages' => $messages
	    		));
        	}
        })->middleware($userIsLogged);
        
        $controllers->get('/delete', function (Application $app) {
        	$id = $app['request']->get('id');

	       	$em = $app['em'];
	       	
       		$cursoPresencial = $em->find('Spanischool\Entity\CursoPresencial', $id);
       		
       		// Eliminar
       		$em->remove($cursoPresencial);
       		
       		$em->flush();
       		
       		return $app->redirect('main');
        })->middleware($userIsLogged);
        
        return $controllers;
    }

	const sessionPreffix = 'cursosPresenciales';
	
    public static function setId(Application $app, $id) { $app['session']->set(self::sessionPreffix.'id', $id); }
    public static function getId(Application $app) { return $app['session']->get(self::sessionPreffix.'id'); }
}
