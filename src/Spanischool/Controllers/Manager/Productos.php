<?php
namespace Spanischool\Controllers\Manager;

use Spanischool\Types\TipoProductoType;

use Spanischool\Entity\Paquete;

use Spanischool\Library\TypeValidation;
use Spanischool\Exception\ApplicationException;
use Spanischool\Entity\Producto;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

use Silex\Application;
use Silex\ControllerProviderInterface;
use Silex\ControllerCollection;

class Productos implements ControllerProviderInterface {
	
	public function connect(Application $app) {
        $controllers = new ControllerCollection();

        // Comprobación de usuario autenticado
		$userIsLogged = function (Request $request) use ($app) {
	        if ($app['session']->get('user')===NULL) {
		        return $app->redirect('/manager/login');
		    }
		};
	
        $controllers->get('/index', function (Application $app) {
   			return $app->redirect('main');
		})->middleware($userIsLogged);
        
        $controllers->get('/main', function (Application $app) {
			Productos::setId($app, NULL);
			
        	$em = $app['em'];
        	
        	$productos = $em->createQuery('select p from Spanischool\Entity\Producto p order by p.id')->getResult();
        	
        	$user = $app['session']->get('user');

			return $app['twig']->render('Manager/Productos/main.html.twig', array(
    			'productos' => $productos,
				'isAdministrador' => $user['rol']===Main::Administrador,
				'tipos' => TipoProductoType::toArray()
    		));
		})->middleware($userIsLogged);
        
		$controllers->get('/view', function (Application $app) {
        	$id = $app['request']->get('id');
        	
        	$em = $app['em'];

       		$producto = $em->find('Spanischool\Entity\Producto', $id);
       		if (!$producto) {
       			return $app->redirect('main');
       		}
        	
			return $app['twig']->render('Manager/Productos/view.html.twig', array(
    			'producto' => $producto,
				'tipos' => TipoProductoType::toArray()
    		));
        })->middleware($userIsLogged);
        
        $controllers->get('/edit', function (Application $app) {
        	$id = $app['request']->get('id');
        	
        	if ($id) {
	        	$em = $app['em'];
	        	
        		$producto = $em->find('Spanischool\Entity\Producto', $id);
        		
				return $app['twig']->render('Manager/Productos/edit.html.twig', array(
        			'id' => $producto->getId(),
	        		'nombre' => $producto->getNombre(),
	        		'codigoPlataforma' => $producto->getCodigoPlataforma(),
	        		'nivel' => $producto->getNivel(),
	        		'horas' => $producto->getHoras()? number_format($producto->getHoras(), 2, ',', ''): '',
	        		'diasAcceso' => $producto->getDiasAcceso(),
	        		'tipo' => $producto->getTipo(),
					'precioEUR' => $producto->getPrecioEUR()? number_format($producto->getPrecioEUR(), 2, ',', ''): '',
	        		'precioUSD' => $producto->getPrecioUSD()? number_format($producto->getPrecioUSD(), 2, ',', ''): '',
	        		'precioRBR' => $producto->getPrecioRBR()? number_format($producto->getPrecioRBR(), 2, ',', ''): '',
	        		'precioGBP' => $producto->getPrecioGBP()? number_format($producto->getPrecioGBP(), 2, ',', ''): '',
					'tipos' => TipoProductoType::toArray()
	    		));
        		
        	} else {
				return $app['twig']->render('Manager/Productos/edit.html.twig', array(
					'tipos' => TipoProductoType::toArray()
				));
        	}
        })->middleware($userIsLogged);

        $controllers->post('/save', function (Application $app) {
        	$id = $app['request']->get('id');
        	$nombre = trim($app['request']->get('nombre'));
        	$codigoPlataforma = trim($app['request']->get('codigoPlataforma'));
        	$nivel = trim($app['request']->get('nivel'));
        	$horas = trim($app['request']->get('horas'));
        	$diasAcceso = trim($app['request']->get('diasAcceso'));
        	$tipo = trim($app['request']->get('tipo'));
        	$precioEUR = trim($app['request']->get('precioEUR'));
        	$precioUSD = trim($app['request']->get('precioUSD'));
        	$precioRBR = trim($app['request']->get('precioRBR'));
        	$precioGBP = trim($app['request']->get('precioGBP'));
        	
        	$horasNum = $horas!=''? str_replace(',', '.', $horas): '';
        	$precioEURNum = $precioEUR!=''? str_replace(',', '.', $precioEUR): '';
        	$precioUSDNum = $precioUSD!=''? str_replace(',', '.', $precioUSD): '';
        	$precioRBRNum = $precioRBR!=''? str_replace(',', '.', $precioRBR): '';
        	$precioGBPNum = $precioGBP!=''? str_replace(',', '.', $precioGBP): '';

        	// Validación
        	$messages = array();
			if ($nombre=='') {
				$messages['nombre'] = 'El nombre es obligatorio';
			} else if (strlen($nombre)>Producto::nombre_LENGTH) {
				$messages['nombre'] = 'El nombre no es válido';
			}
			if ($codigoPlataforma!='' && strlen($codigoPlataforma)>Producto::codigoPlataforma_LENGTH) {
				$messages['codigoPlataforma'] = 'El código de plataforma no es válido';
			}
			if ($nivel!='' && strlen($nivel)>Producto::nivel_LENGTH) {
				$messages['nivel'] = 'El nivel no es válido';
			}
			if ($horasNum!='' && !TypeValidation::isFloat($horasNum)) {
				$messages['horas'] = 'El número de horas no es válido';
			}
			if ($diasAcceso!='' && !TypeValidation::isInteger($diasAcceso)) {
				$messages['diasAcceso'] = 'El número de días de acceso no es válido';
			}
			if ($precioEURNum!='' && !TypeValidation::isFloat($precioEURNum)) {
				$messages['precioEUR'] = 'El precio en euros no es válido';
			}
			if ($precioUSDNum!='' && !TypeValidation::isFloat($precioUSDNum)) {
				$messages['precioUSD'] = 'El precio en dólares no es válido';
			}
			if ($precioRBRNum!='' && !TypeValidation::isFloat($precioRBRNum)) {
				$messages['precioRBR'] = 'El precio en reales brasileños no es válido';
			}
			if ($precioGBPNum!='' && !TypeValidation::isFloat($precioGBPNum)) {
				$messages['precioGBP'] = 'El precio en libras no es válido';
			}
			if ($precioEURNum=='' && $precioUSDNum=='' && $precioRBRNum=='' && $precioGBPNum=='') {
				$messages['precio'] = 'Es obligatorio indicar al menos un precio';
			}
			if ($tipo=='') {
				$messages['tipo'] = 'El tipo es obligatorio';
			}
			
			// Si no hay errores
			if (count($messages)==0) {
		       	$em = $app['em'];
	        	        	
	        	if ($id) {
	        		$producto = $em->find('Spanischool\Entity\Producto', $id);
	        	} else {
	        		$producto = new Producto();
	        	}
	        	
	        	$producto->setNombre($nombre!=''? $nombre: NULL);
	        	$producto->setCodigoPlataforma($codigoPlataforma!=''? $codigoPlataforma: NULL);
	        	$producto->setNivel($nivel!=''? $nivel: NULL);
	        	$producto->setHoras($horasNum!=''? $horasNum: NULL);
	        	$producto->setDiasAcceso($diasAcceso!=''? $diasAcceso: NULL);
	        	$producto->setTipo($tipo);
	        	$producto->setPrecioEUR($precioEURNum!=''? $precioEURNum: NULL);
	        	$producto->setPrecioUSD($precioUSDNum!=''? $precioUSDNum: NULL);
	        	$producto->setPrecioRBR($precioRBRNum!=''? $precioRBRNum: NULL);
	        	$producto->setPrecioGBP($precioGBPNum!=''? $precioGBPNum: NULL);

	        	if (!$producto->getPaquete()) {
	        		$paquete = new Paquete();
	        		$paquete->setDescripcion($producto->getNombre());
	        		$paquete->setActivo(FALSE);
	        		$paquete->setPrecioEUR($producto->getPrecioEUR());
	        		$paquete->setPrecioUSD($producto->getPrecioUSD());
	        		$paquete->setPrecioRBR($producto->getPrecioRBR());
	        		$paquete->setPrecioGBP($producto->getPrecioGBP());
	        		$paquete->addProducto($producto);
	        		$producto->setPaquete($paquete);
	        	}

	        	try {
		        	$em->persist($producto);
		        	$em->flush();
	        	} catch (ApplicationException $e) {
	        		print_r($e->getMessages());
	        		die();
	        	}

	        	return $app->redirect('view?id='.$producto->getId());
	        	
			} else {
				return $app['twig']->render('Manager/Productos/edit.html.twig', array(
	    			'id' => $id,
	        		'nombre' => $nombre,
	        		'codigoPlataforma' => $codigoPlataforma,
	        		'nivel' => $nivel,
	        		'horas' => $horas,
	        		'diasAcceso' => $diasAcceso,
	        		'tipo' => $tipo,
					'precioEUR' => $precioEUR,
	        		'precioUSD' => $precioUSD,
	        		'precioRBR' => $precioRBR,
	        		'precioGBP' => $precioGBP,
					'tipos' => TipoProductoType::toArray(),
	        		'messages' => $messages
	    		));
        	}
        })->middleware($userIsLogged);
        
        $controllers->get('/delete', function (Application $app) {
        	$id = $app['request']->get('id');

	       	$em = $app['em'];
	       	
       		$producto = $em->find('Spanischool\Entity\Producto', $id);
       		
			// Eliminar la asociación de su paquete con los productos       		
       		$producto->getPaquete()->getProductos()->clear();
       		// Eliminar la asociación con los códigos promocionales
       		$producto->getCodigosPromocionales()->clear();
       		// Eliminar el producto
       		$em->remove($producto);
       		
       		$em->flush();
       		
       		return $app->redirect('main');
        })->middleware($userIsLogged);
        
        return $controllers;
    }

	const sessionPreffix = 'productos';
	
    public static function setId(Application $app, $id) { $app['session']->set(self::sessionPreffix.'id', $id); }
    public static function getId(Application $app) { return $app['session']->get(self::sessionPreffix.'id'); }
}
