<?php
namespace Spanischool\Controllers\Manager;

use Spanischool\Exception\ApplicationException;

use Symfony\Component\HttpFoundation\File\UploadedFile;

use Spanischool\Entity\Cartel;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

use Silex\Application;
use Silex\ControllerProviderInterface;
use Silex\ControllerCollection;

class Carteles implements ControllerProviderInterface {
	
	const UPLOADURL = '/cartel';
	
	public function connect(Application $app) {
        $controllers = new ControllerCollection();

        // Comprobación de usuario autenticado
		$userIsLogged = function (Request $request) use ($app) {
	        if ($app['session']->get('user')===NULL) {
		        return $app->redirect('/manager/login');
		    }
		};
	
        $controllers->get('/index', function (Application $app) {
   			return $app->redirect('main');
		})->middleware($userIsLogged);
        
        $controllers->get('/main', function (Application $app) {
        	$em = $app['em'];
        	
        	$carteles = $em->createQuery('select c from Spanischool\Entity\Cartel c order by c.id')->getResult();
        	
			return $app['twig']->render('Manager/Carteles/main.html.twig', array(
    			'carteles' => $carteles,
				'uploadURL' => UPLOADURL.Carteles::UPLOADURL
    		));
		})->middleware($userIsLogged);
        
		$controllers->get('/view', function (Application $app) {
        	$id = $app['request']->get('id');
        	
        	$em = $app['em'];

       		$cartel = $em->find('Spanischool\Entity\Cartel', $id);
       		if (!$cartel) {
       			return $app->redirect('main');
       		}
        	
			return $app['twig']->render('Manager/Carteles/view.html.twig', array(
    			'cartel' => $cartel,
				'uploadURL' => UPLOADURL.Carteles::UPLOADURL
    		));
        })->middleware($userIsLogged);
        
        $controllers->get('/edit', function (Application $app) {
        	$id = $app['request']->get('id');
        	
        	if ($id) {
	        	$em = $app['em'];
	        	
        		$cartel = $em->find('Spanischool\Entity\Cartel', $id);

				return $app['twig']->render('Manager/Carteles/edit.html.twig', array(
        			'id' => $cartel->getId(),
	        		'fichero' => $cartel->getFichero(),
					'activo' => $cartel->getActivo(),
					'lang' => $cartel->getLang(),
					'uploadURL' => UPLOADURL.Carteles::UPLOADURL,
					'maxFileSize' => UploadedFile::getMaxFilesize()
	    		));
        		
        	} else {
				return $app['twig']->render('Manager/Carteles/edit.html.twig', array(
					'maxFileSize' => UploadedFile::getMaxFilesize()
	    		));
        	}
        })->middleware($userIsLogged);

        $controllers->post('/save', function (Application $app) {
        	$id = $app['request']->get('id');
        	$borrar = $app['request']->get('borrar');
        	$fichero = $app['request']->files->get('fichero');
        	$activo = trim($app['request']->get('activo'));
        	$lang = trim($app['request']->get('lang'));

        	// Validación
        	$messages = array();
			if ($fichero===NULL && $id==='') {
				$messages['fichero'] = 'La imagen es obligatoria';
			}

			// Si no hay errores
			if (count($messages)==0) {
		       	$em = $app['em'];
	        	        	
	        	if ($id) {
	        		$cartel = $em->find('Spanischool\Entity\Cartel', $id);
	        	} else {
	        		$cartel = new Cartel();
	        	}

	        	$cartel->setActivo($activo!=''? TRUE: FALSE);
	        	$cartel->setLang($lang);
	
	        	// Si tenemos fichero
	        	if ($fichero!==NULL) {
	        		if ($cartel->getFichero()) {
	        			unlink(UPLOADDIR.Carteles::UPLOADURL.'/'.$cartel->getFichero());
	        		}
	        		
	        		// Nombre del fichero a guardar (nombre+extensión)
	        		$fileName = microtime(TRUE).substr($fichero->getClientOriginalName(), strrpos($fichero->getClientOriginalName(), '.'));
	        		
	        		// Mover el fichero subido al directorio de descargas y renombrarlo
	        		$fichero->move(UPLOADDIR.Carteles::UPLOADURL, $fichero->getClientOriginalName());
	        		rename(UPLOADDIR.Carteles::UPLOADURL.'/'.$fichero->getClientOriginalName(), UPLOADDIR.Carteles::UPLOADURL.'/'.$fileName);
	        		
	        		$cartel->setFichero($fileName);
	        	}
	        	
	        	try {
		        	$em->persist($cartel);
		        	$em->flush();
	        	} catch (ApplicationException $e) {
	        		print_r($e->getMessages());
	        		die();
	        	}

	        	return $app->redirect('view?id='.$cartel->getId());
	        	
			} else {
				return $app['twig']->render('Manager/Carteles/edit.html.twig', array(
	    			'id' => $id,
	        		'activo' => $activo,
					'lang'	=> $lang,
					'maxFileSize' => UploadedFile::getMaxFilesize(),
	        		'messages' => $messages
	    		));
        	}
        })->middleware($userIsLogged);
        
        $controllers->get('/delete', function (Application $app) {
        	$id = $app['request']->get('id');

	       	$em = $app['em'];
	       	
       		$cartel = $em->find('Spanischool\Entity\Cartel', $id);
       		
       		// Eliminar
       		$em->remove($cartel);
       		$em->flush();
       		
       		// Eliminar el fichero
       		unlink(UPLOADDIR.Carteles::UPLOADURL.'/'.$cartel->getFichero());
       		
       		return $app->redirect('main');
        })->middleware($userIsLogged);
        
        return $controllers;
    }
}
