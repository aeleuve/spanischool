<?php
namespace Spanischool\Controllers\Manager;

use Spanischool\Entity\Paquete;

use Spanischool\Library\TypeValidation;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Silex\Application;
use Silex\ControllerProviderInterface;
use Silex\ControllerCollection;

class Paquetes implements ControllerProviderInterface {
	
	public function connect(Application $app) {
        $controllers = new ControllerCollection();

        // Comprobación de usuario autenticado
		$userIsLogged = function (Request $request) use ($app) {
	        if ($app['session']->get('user')===NULL) {
		        return $app->redirect('/manager/login');
		    }
		};
	
        $controllers->get('/index', function (Application $app) {
        	$id = $app['request']->get('id');
        	
        	Productos::setId($app, $id);

		   	return $app->redirect('main');
        })->middleware($userIsLogged);

        $controllers->get('/main', function (Application $app) {
        	$productoId = Productos::getId($app);
        	
        	$em = $app['em'];

        	$sql = 'select prod, paq, prods
        		from Spanischool\Entity\Producto prod
        		join prod.paquete paq
        		join paq.productos prods
        		where prod.id = :productoId
        		order by prods.nombre, prods.id';        	
        	$producto = $em->createQuery($sql)->setParameter('productoId', $productoId)->getSingleResult();
        	
			return $app['twig']->render('Manager/Paquetes/main.html.twig', array(
				'producto' => $producto
    		));
		})->middleware($userIsLogged);
        
        $controllers->get('/edit', function (Application $app) {
        	$id = $app['request']->get('id');
        	
        	$productoId = Productos::getId($app);
			
	        $em = $app['em'];
	        	
        	$producto = $em->find('Spanischool\Entity\Producto', $productoId);
        	
       		$paquete = $em->find('Spanischool\Entity\Paquete', $id);
        		
			return $app['twig']->render('Manager/Paquetes/edit.html.twig', array(
				'producto' => $producto,
       			'id' => $paquete->getId(),
        		'descripcion' => $paquete->getDescripcion(),
        		'ahorro' => $paquete->getAhorro(),
        		'activo' => $paquete->getActivo(),
        		'precioEUR' => $paquete->getPrecioEUR()? number_format($paquete->getPrecioEUR(), 2, ',', ''): '',
        		'precioUSD' => $paquete->getPrecioUSD()? number_format($paquete->getPrecioUSD(), 2, ',', ''): '',
        		'precioRBR' => $paquete->getPrecioRBR()? number_format($paquete->getPrecioRBR(), 2, ',', ''): '',
        		'precioGBP' => $paquete->getPrecioGBP()? number_format($paquete->getPrecioGBP(), 2, ',', ''): ''
    		));
        		
        })->middleware($userIsLogged);

        $controllers->post('/save', function (Application $app) {
        	$id = $app['request']->get('id');
        	$descripcion = trim($app['request']->get('descripcion'));
        	$ahorro = trim($app['request']->get('ahorro'));
        	$activo = trim($app['request']->get('activo'));
        	$precioEUR = trim($app['request']->get('precioEUR'));
        	$precioUSD = trim($app['request']->get('precioUSD'));
        	$precioRBR = trim($app['request']->get('precioRBR'));
        	$precioGBP = trim($app['request']->get('precioGBP'));
        	
        	$precioEURNum = $precioEUR!=''? str_replace(',', '.', $precioEUR): '';
        	$precioUSDNum = $precioUSD!=''? str_replace(',', '.', $precioUSD): '';
        	$precioRBRNum = $precioRBR!=''? str_replace(',', '.', $precioRBR): '';
        	$precioGBPNum = $precioGBP!=''? str_replace(',', '.', $precioGBP): '';

        	// Validación
        	$messages = array();
			if ($descripcion=='') {
				$messages['descripcion'] = 'La descripción es obligatoria';
			} else if (strlen($descripcion)>Paquete::descripcion_LENGTH) {
				$messages['descripcion'] = 'La descripción no es válida';
			}
			if ($ahorro!='' && strlen($ahorro)>Paquete::ahorro_LENGTH) {
				$messages['ahorro'] = 'El ahorro no es válido';
			}
			if ($precioEURNum!='' && !TypeValidation::isFloat($precioEURNum)) {
				$messages['precioEUR'] = 'El precio en euros no es válido';
			}
			if ($precioUSDNum!='' && !TypeValidation::isFloat($precioUSDNum)) {
				$messages['precioUSD'] = 'El precio en dólares no es válido';
			}
			if ($precioRBRNum!='' && !TypeValidation::isFloat($precioRBRNum)) {
				$messages['precioRBR'] = 'El precio en reales brasileños no es válido';
			}
			if ($precioGBPNum!='' && !TypeValidation::isFloat($precioGBPNum)) {
				$messages['precioGBP'] = 'El precio en libras no es válido';
			}
			if ($precioEURNum=='' && $precioUSDNum=='' && $precioRBRNum=='' && $precioGBPNum=='') {
				$messages['precio'] = 'Es obligatorio indicar al menos un precio';
			}
			
			$em = $app['em'];

       		$productoId = Productos::getId($app);
			
			$producto = $em->find('Spanischool\Entity\Producto', $productoId);
			
			// Si no hay errores
			if (count($messages)==0) {
				
        		$paquete = $em->find('Spanischool\Entity\Paquete', $id);
	        	$paquete->setDescripcion($descripcion!=''? $descripcion: NULL);
	        	$paquete->setAhorro($ahorro!=''? $ahorro: NULL);
	        	$paquete->setActivo($activo!=''? TRUE: FALSE);
	        	$paquete->setPrecioEUR($precioEURNum!=''? $precioEURNum: NULL);
	        	$paquete->setPrecioUSD($precioUSDNum!=''? $precioUSDNum: NULL);
	        	$paquete->setPrecioRBR($precioRBRNum!=''? $precioRBRNum: NULL);
	        	$paquete->setPrecioGBP($precioGBPNum!=''? $precioGBPNum: NULL);

	        	try {
	        		
		        	$em->persist($producto);
		        	$em->flush();
	        	} catch (ApplicationException $e) {
	        		print_r($e->getMessages());
	        		die();
	        	}

	        	return $app->redirect('main?');
	        	
			} else {
				return $app['twig']->render('Manager/Paquetes/edit.html.twig', array(
					'producto' => $producto,
	    			'id' => $id,
	        		'descripcion' => $descripcion,
	        		'ahorro' => $ahorro,
	        		'activo' => $activo,
					'precioEUR' => $precioEUR,
	        		'precioUSD' => $precioUSD,
	        		'precioRBR' => $precioRBR,
	        		'precioGBP' => $precioGBP,
	        		'messages' => $messages
	    		));
        	}
        })->middleware($userIsLogged);
        
        // Lista de productos que pueden añadirse al paquete indicado
        $controllers->post('/productos', function (Application $app) {
        	$paqueteId = $app['request']->get('paquete');
        	
			$em = $app['em'];

			// Carga del paquete
			$sql = 'select paq, prod
        		from Spanischool\Entity\Paquete paq
        		join paq.productos prod
        		where paq.id = :paqueteId';
       		$paquete = $em->createQuery($sql)->setParameter('paqueteId', $paqueteId)->getSingleResult();
       		
       		// Lista con los productos que actualmente están asociados al paquete
       		$actuales = array();
       		foreach ($paquete->getProductos() as $producto) {
       			$actuales[] = $producto->getId();
       		}
       		
       		// Lista de todos los productos exceptuando los que actualmente está asociados al paquete
       		$sql = 'select prod
       			from Spanischool\Entity\Producto prod
       			where prod.id not in (:actuales)
       			order by prod.nombre, prod.id';
        	$productos = $em->createQuery($sql)->setParameter('actuales', $actuales)->getResult();
        	
        	// Componer el cuerpo de la tabla html de selección de productos
        	$html = '';
        	foreach ($productos as $producto) {
        		$html .= '<tr>';
        		$html .= '<td><input type="checkbox" rel="'.$producto->getId().'">';
        		$html .= '</td><td>'.$producto->getNombre().'</td>';
				$html .= '</tr>';
        	}
        	
		   	return $html;
        })->middleware($userIsLogged);

        // Guardar la asociación entre el paquete y los productos especificados
        $controllers->post('/guardarProductos', function (Application $app) {
        	$paqueteId = $app['request']->get('paquete');
        	$productosId = $app['request']->get('productosId');
        	
			$em = $app['em'];

			// Carga del paquete
        	$sql = 'select paq, prod
        		from Spanischool\Entity\Paquete paq
        		join paq.productos prod
        		where paq.id = :paqueteId';
       		$paquete = $em->createQuery($sql)->setParameter('paqueteId', $paqueteId)->getSingleResult();
       		
       		// Lista de los productos que hay que asociar
       		$sql = 'select prod
       			from Spanischool\Entity\Producto prod
       			where prod.id in (:asociar)';
        	$productos = $em->createQuery($sql)->setParameter('asociar', explode(',', $productosId))->getResult();

       		// Por cada producto a asociar
       		foreach ($productos as $producto) {
       			$paquete->addProducto($producto);
       		}
       		
       		$em->persist($paquete);
       		$em->flush();
       		
        })->middleware($userIsLogged);

        // Quitar la asociación entre el paquete y el producto especificado
        $controllers->post('/quitarProducto', function (Application $app) {
        	$paqueteId = $app['request']->get('paquete');
        	$productoId = $app['request']->get('producto');
        	
			$em = $app['em'];

			// Carga del paquete
        	$sql = 'select paq, prod
        		from Spanischool\Entity\Paquete paq
        		join paq.productos prod
        		where paq.id = :paqueteId';
       		$paquete = $em->createQuery($sql)->setParameter('paqueteId', $paqueteId)->getSingleResult();
       		
       		// cargar el producto
			$producto = $em->find('Spanischool\Entity\Producto', $productoId);

			// Quitar la asociación
   			$paquete->getProductos()->removeElement($producto);
       		
       		$em->persist($paquete);
       		$em->flush();
       		
        })->middleware($userIsLogged);
        
        return $controllers;
    }
}
