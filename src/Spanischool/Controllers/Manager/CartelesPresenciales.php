<?php
namespace Spanischool\Controllers\Manager;

use Spanischool\Exception\ApplicationException;

use Symfony\Component\HttpFoundation\File\UploadedFile;

use Spanischool\Entity\CartelPresencial;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

use Silex\Application;
use Silex\ControllerProviderInterface;
use Silex\ControllerCollection;

class CartelesPresenciales implements ControllerProviderInterface {
	
	const UPLOADURL = '/cartelPresencial';
	
	public function connect(Application $app) {
        $controllers = new ControllerCollection();

        // Comprobación de usuario autenticado
		$userIsLogged = function (Request $request) use ($app) {
	        if ($app['session']->get('user')===NULL) {
		        return $app->redirect('/manager/login');
		    }
		};
	
        $controllers->get('/index', function (Application $app) {
   			return $app->redirect('main');
		})->middleware($userIsLogged);
        
        $controllers->get('/main', function (Application $app) {
        	$em = $app['em'];
        	
        	$cartelesPresenciales = $em->createQuery('select c from Spanischool\Entity\CartelPresencial c order by c.id')->getResult();
        	
			return $app['twig']->render('Manager/CartelesPresenciales/main.html.twig', array(
    			'cartelesPresenciales' => $cartelesPresenciales,
				'uploadURL' => UPLOADURL.CartelesPresenciales::UPLOADURL
    		));
		})->middleware($userIsLogged);
        
		$controllers->get('/view', function (Application $app) {
        	$id = $app['request']->get('id');
        	
        	$em = $app['em'];

       		$cartelPresencial = $em->find('Spanischool\Entity\CartelPresencial', $id);
       		if (!$cartelPresencial) {
       			return $app->redirect('main');
       		}
        	
			return $app['twig']->render('Manager/CartelesPresenciales/view.html.twig', array(
    			'cartelPresencial' => $cartelPresencial,
				'uploadURL' => UPLOADURL.CartelesPresenciales::UPLOADURL
    		));
        })->middleware($userIsLogged);
        
        $controllers->get('/edit', function (Application $app) {
        	$id = $app['request']->get('id');
        	
        	if ($id) {
	        	$em = $app['em'];
	        	
        		$cartelPresencial = $em->find('Spanischool\Entity\CartelPresencial', $id);

				return $app['twig']->render('Manager/CartelesPresenciales/edit.html.twig', array(
        			'id' => $cartelPresencial->getId(),
	        		'fichero' => $cartelPresencial->getFichero(),
					'activo' => $cartelPresencial->getActivo(),
					'lang' => $cartelPresencial->getLang(),
					'uploadURL' => UPLOADURL.CartelesPresenciales::UPLOADURL,
					'maxFileSize' => UploadedFile::getMaxFilesize()
	    		));
        		
        	} else {
				return $app['twig']->render('Manager/CartelesPresenciales/edit.html.twig', array(
					'maxFileSize' => UploadedFile::getMaxFilesize()
	    		));
        	}
        })->middleware($userIsLogged);

        $controllers->post('/save', function (Application $app) {
        	$id = $app['request']->get('id');
        	$borrar = $app['request']->get('borrar');
        	$fichero = $app['request']->files->get('fichero');
        	$activo = trim($app['request']->get('activo'));
        	$lang = trim($app['request']->get('lang'));

        	// Validación
        	$messages = array();
			if ($fichero===NULL && $id==='') {
				$messages['fichero'] = 'La imagen es obligatoria';
			}

			// Si no hay errores
			if (count($messages)==0) {
		       	$em = $app['em'];
	        	        	
	        	if ($id) {
	        		$cartelPresencial = $em->find('Spanischool\Entity\CartelPresencial', $id);
	        	} else {
	        		$cartelPresencial = new CartelPresencial();
	        	}

	        	$cartelPresencial->setActivo($activo!=''? TRUE: FALSE);
	        	$cartelPresencial->setLang($lang);
	        	
	        	// Si tenemos fichero
	        	if ($fichero!==NULL) {
	        		if ($cartelPresencial->getFichero()) {
	        			unlink(UPLOADDIR.CartelesPresenciales::UPLOADURL.'/'.$cartelPresencial->getFichero());
	        		}
	        		
	        		// Nombre del fichero a guardar (nombre+extensión)
	        		$fileName = microtime(TRUE).substr($fichero->getClientOriginalName(), strrpos($fichero->getClientOriginalName(), '.'));
	        		
	        		// Mover el fichero subido al directorio de descargas y renombrarlo
	        		$fichero->move(UPLOADDIR.CartelesPresenciales::UPLOADURL, $fichero->getClientOriginalName());
	        		rename(UPLOADDIR.CartelesPresenciales::UPLOADURL.'/'.$fichero->getClientOriginalName(), UPLOADDIR.CartelesPresenciales::UPLOADURL.'/'.$fileName);
	        		
	        		$cartelPresencial->setFichero($fileName);
	        	}
	        	
	        	try {
		        	$em->persist($cartelPresencial);
		        	$em->flush();
	        	} catch (ApplicationException $e) {
	        		print_r($e->getMessages());
	        		die();
	        	}

	        	return $app->redirect('view?id='.$cartelPresencial->getId());
	        	
			} else {
				return $app['twig']->render('Manager/CartelesPresenciales/edit.html.twig', array(
	    			'id' => $id,
	        		'activo' => $activo,
					'maxFileSize' => UploadedFile::getMaxFilesize(),
	        		'messages' => $messages
	    		));
        	}
        })->middleware($userIsLogged);
        
        $controllers->get('/delete', function (Application $app) {
        	$id = $app['request']->get('id');

	       	$em = $app['em'];
	       	
       		$cartelPresencial = $em->find('Spanischool\Entity\CartelPresencial', $id);
       		
       		// Eliminar
       		$em->remove($cartelPresencial);
       		$em->flush();
       		
       		// Eliminar el fichero
       		unlink(UPLOADDIR.CartelesPresenciales::UPLOADURL.'/'.$cartelPresencial->getFichero());
       		
       		return $app->redirect('main');
        })->middleware($userIsLogged);
        
        return $controllers;
    }
}
