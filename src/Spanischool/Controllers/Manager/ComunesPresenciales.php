<?php
namespace Spanischool\Controllers\Manager;

use Spanischool\Library\TypeValidation;
use Spanischool\Exception\ApplicationException;
use Spanischool\Entity\ComunPresencial;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

use Silex\Application;
use Silex\ControllerProviderInterface;
use Silex\ControllerCollection;

class ComunesPresenciales implements ControllerProviderInterface {
	
	public function connect(Application $app) {
        $controllers = new ControllerCollection();

        // Comprobación de usuario autenticado
		$userIsLogged = function (Request $request) use ($app) {
	        if ($app['session']->get('user')===NULL) {
		        return $app->redirect('/manager/login');
		    }
		};
	
        $controllers->get('/index', function (Application $app) {
   			return $app->redirect('main');
		})->middleware($userIsLogged);
        
        $controllers->get('/main', function (Application $app) {
        	$em = $app['em'];
        	
        	$comunesPresenciales = $em->createQuery('select cp from Spanischool\Entity\ComunPresencial cp order by cp.nombre, cp.id')->getResult();
        	
			return $app['twig']->render('Manager/ComunesPresenciales/main.html.twig', array(
    			'comunesPresenciales' => $comunesPresenciales
    		));
		})->middleware($userIsLogged);
        
		$controllers->get('/view', function (Application $app) {
        	$id = $app['request']->get('id');
        	
        	$em = $app['em'];

       		$comunPresencial = $em->find('Spanischool\Entity\ComunPresencial', $id);
       		if (!$comunPresencial) {
       			return $app->redirect('main');
       		}
        	
			return $app['twig']->render('Manager/ComunesPresenciales/view.html.twig', array(
    			'comunPresencial' => $comunPresencial
    		));
        })->middleware($userIsLogged);
        
        $controllers->get('/edit', function (Application $app) {
        	$id = $app['request']->get('id');
        	
        	$comunPresencial = null;
        	if ($id) {
	        	$em = $app['em'];
	        	
        		$comunPresencial = $em->find('Spanischool\Entity\ComunPresencial', $id);
        		
				return $app['twig']->render('Manager/ComunesPresenciales/edit.html.twig', array(
        			'id' => $comunPresencial->getId(),
	        		'nombre' => $comunPresencial->getNombre(),
	        		'precioEUR' => $comunPresencial->getPrecioEUR()? number_format($comunPresencial->getPrecioEUR(), 2, ',', ''): '',
	        		'precioUSD' => $comunPresencial->getPrecioUSD()? number_format($comunPresencial->getPrecioUSD(), 2, ',', ''): '',
	        		'precioRBR' => $comunPresencial->getPrecioRBR()? number_format($comunPresencial->getPrecioRBR(), 2, ',', ''): '',
	        		'precioGBP' => $comunPresencial->getPrecioGBP()? number_format($comunPresencial->getPrecioGBP(), 2, ',', ''): ''
	    		));
        		
        	} else {
				return $app['twig']->render('Manager/ComunesPresenciales/edit.html.twig');
        	}
        })->middleware($userIsLogged);

        $controllers->post('/save', function (Application $app) {
        	$id = $app['request']->get('id');
        	$nombre = trim($app['request']->get('nombre'));
        	$precioEUR = trim($app['request']->get('precioEUR'));
        	$precioUSD = trim($app['request']->get('precioUSD'));
        	$precioRBR = trim($app['request']->get('precioRBR'));
        	$precioGBP = trim($app['request']->get('precioGBP'));
        	
        	$precioEURNum = $precioEUR!=''? str_replace(',', '.', $precioEUR): '';
        	$precioUSDNum = $precioUSD!=''? str_replace(',', '.', $precioUSD): '';
        	$precioRBRNum = $precioRBR!=''? str_replace(',', '.', $precioRBR): '';
        	$precioGBPNum = $precioGBP!=''? str_replace(',', '.', $precioGBP): '';

        	// Validación
        	$messages = array();
			if ($nombre=='') {
				$messages['nombre'] = 'El nombre es obligatorio';
			} else if (strlen($nombre)>ComunPresencial::nombre_LENGTH) {
				$messages['nombre'] = 'El nombre no es válido';
			}
			if ($precioEURNum!='' && !TypeValidation::isFloat($precioEURNum)) {
				$messages['precioEUR'] = 'El precio en euros no es válido';
			}
			if ($precioUSDNum!='' && !TypeValidation::isFloat($precioUSDNum)) {
				$messages['precioUSD'] = 'El precio en dólares no es válido';
			}
			if ($precioRBRNum!='' && !TypeValidation::isFloat($precioRBRNum)) {
				$messages['precioRBR'] = 'El precio en reales brasileños no es válido';
			}
			if ($precioGBPNum!='' && !TypeValidation::isFloat($precioGBPNum)) {
				$messages['precioGBP'] = 'El precio en libras no es válido';
			}
			if ($precioEURNum=='' && $precioUSDNum=='' && $precioRBRNum=='' && $precioGBPNum=='') {
				$messages['precio'] = 'Es obligatorio indicar al menos un precio';
			}
			
			// Si no hay errores
			if (count($messages)==0) {
		       	$em = $app['em'];
	        	        	
	        	if ($id) {
	        		$comunPresencial = $em->find('Spanischool\Entity\ComunPresencial', $id);
	        	} else {
	        		$comunPresencial = new ComunPresencial();
	        	}
	        	
	        	$comunPresencial->setNombre($nombre!=''? $nombre: NULL);
	        	$comunPresencial->setPrecioEUR($precioEURNum!=''? $precioEURNum: NULL);
	        	$comunPresencial->setPrecioUSD($precioUSDNum!=''? $precioUSDNum: NULL);
	        	$comunPresencial->setPrecioRBR($precioRBRNum!=''? $precioRBRNum: NULL);
	        	$comunPresencial->setPrecioGBP($precioGBPNum!=''? $precioGBPNum: NULL);

	        	try {
		        	$em->persist($comunPresencial);
		        	$em->flush();
	        	} catch (ApplicationException $e) {
	        		print_r($e->getMessages());
	        		die();
	        	}

	        	return $app->redirect('view?id='.$comunPresencial->getId());
	        	
			} else {
				return $app['twig']->render('Manager/ComunesPresenciales/edit.html.twig', array(
	    			'id' => $id,
	        		'nombre' => $nombre,
	        		'precioEUR' => $precioEUR,
	        		'precioUSD' => $precioUSD,
	        		'precioRBR' => $precioRBR,
	        		'precioGBP' => $precioGBP,
	        		'messages' => $messages
	    		));
        	}
        })->middleware($userIsLogged);
        
        $controllers->get('/delete', function (Application $app) {
        	$id = $app['request']->get('id');

	       	$em = $app['em'];
	       	
       		$comunPresencial = $em->find('Spanischool\Entity\ComunPresencial', $id);
       		
       		// Eliminar
       		$em->remove($comunPresencial);
       		
       		$em->flush();
       		
       		return $app->redirect('main');
        })->middleware($userIsLogged);
        
        return $controllers;
    }
}
