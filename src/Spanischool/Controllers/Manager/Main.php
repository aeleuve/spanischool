<?php
namespace Spanischool\Controllers\Manager;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Silex\Application;
use Silex\ControllerProviderInterface;
use Silex\ControllerCollection;

class Main implements ControllerProviderInterface {
	
	// Roles
	const Administrador = 'Administrador';
	const Usuario = 'Usuario';
	
	// Usuarios con acceso a la aplicación
	// La clave del array es el login
	public static $validUsers = array(
		'0' => array( 'passwd' => '0', 'rol' => Main::Administrador),
		'1' => array( 'passwd' => '1', 'rol' => Main::Usuario)
	);
	
	public function connect(Application $app) {
        $controllers = new ControllerCollection();

        // Comprobación de usuario autenticado
		$userIsLogged = function (Request $request) use ($app) {
	        if ($app['session']->get('user')===NULL) {
		        return $app->redirect('/manager/login');
		    }
		};
	
        $controllers->get('/', function (Application $app) {
        	return $app['twig']->render('Manager/Main/index.html.twig');
		})->middleware($userIsLogged);
        
        $controllers->get('/login', function (Application $app) {
        	return $app['twig']->render('Manager/Main/login.html.twig');
		});

		$controllers->post('/check', function (Application $app) {
        	$login = trim($app['request']->get('login'));
        	$passwd = trim($app['request']->get('passwd'));
        	
		    if (key_exists($login, Main::$validUsers) && Main::$validUsers[$login]['passwd']===$passwd) {
		    	$app['session']->start();
		        $app['session']->set('user', array('login' => $login, 'rol' => Main::$validUsers[$login]['rol']));
		        return $app->redirect('/manager');
		    }

        	return $app['twig']->render('Manager/Main/login.html.twig', array(
        		'error' => 'Credenciales incorrectas'
        	));
		});
        
        $controllers->get('/logout', function (Application $app) {
	        if (($user = $app['session']->get('user'))!==NULL) {
		        $app['session']->invalidate();
	    	}        	
	        return $app->redirect('/manager');
		});

		return $controllers;
    }
}
