<?php
namespace Spanischool\Controllers\Manager;

use Spanischool\Exception\ApplicationException;

use Spanischool\Types\ResidenciaType;

use Spanischool\Library\TypeValidation;
use Spanischool\Entity\CursoPresencialDetalle;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Silex\Application;
use Silex\ControllerProviderInterface;
use Silex\ControllerCollection;

class CursosPresencialesDetalles implements ControllerProviderInterface {
	
	public function connect(Application $app) {
        $controllers = new ControllerCollection();

        // Comprobación de usuario autenticado
		$userIsLogged = function (Request $request) use ($app) {
	        if ($app['session']->get('user')===NULL) {
		        return $app->redirect('/manager/login');
		    }
		};
	
        $controllers->get('/index', function (Application $app) {
        	$id = $app['request']->get('id');
        	
        	CursosPresenciales::setId($app, $id);

		   	return $app->redirect('main');
        })->middleware($userIsLogged);
        
        $controllers->get('/main', function (Application $app) {
        	$cursoPresencialId = CursosPresenciales::getId($app);
        	
        	$em = $app['em'];
        	
        	$sql = 'select cp, det
        		from Spanischool\Entity\CursoPresencial cp
        		left join cp.detalles det
        		where cp.id = :cursoPresencialId
        		order by cp.nombre, cp.id';
        	$cursoPresencial = $em->createQuery($sql)->setParameter('cursoPresencialId', $cursoPresencialId)->getSingleResult();
        	
			return $app['twig']->render('Manager/CursosPresencialesDetalles/main.html.twig', array(
				'cursoPresencial' => $cursoPresencial,
    			'detalles' => $cursoPresencial->getDetalles(),
				'residencias' => ResidenciaType::toArray()
    		));
		})->middleware($userIsLogged);
        
		$controllers->get('/view', function (Application $app) {
			$id = $app['request']->get('id');

        	$cursoPresencialId = CursosPresenciales::getId($app);
			
        	$em = $app['em'];

        	$cursoPresencial = $em->find('Spanischool\Entity\CursoPresencial', $cursoPresencialId);
        	
        	$detalle = $em->find('Spanischool\Entity\CursoPresencialDetalle', $id);
       		if (!$detalle) {
       			return $app->redirect('main');
       		}
        	
			return $app['twig']->render('Manager/CursosPresencialesDetalles/view.html.twig', array(
				'cursoPresencial' => $cursoPresencial,
				'detalle' => $detalle,
				'residencias' => ResidenciaType::toArray()
    		));
        })->middleware($userIsLogged);
        
        $controllers->get('/edit', function (Application $app) {
        	$id = $app['request']->get('id');
        	
        	$cursoPresencialId = CursosPresenciales::getId($app);

	        $em = $app['em'];
	        	
        	$cursoPresencial = $em->find('Spanischool\Entity\CursoPresencial', $cursoPresencialId);
        	
        	if ($id) {
        		$detalle = $em->find('Spanischool\Entity\CursoPresencialDetalle', $id);
        		
				return $app['twig']->render('Manager/CursosPresencialesDetalles/edit.html.twig', array(
					'cursoPresencial' => $cursoPresencial,
        			'id' => $detalle->getId(),
	        		'semanas' => $detalle->getSemanas(),
	        		'horas' => $detalle->getHoras(),
	        		'residencia' => $detalle->getResidencia(),
					'precioEUR' => $detalle->getPrecioEUR()? number_format($detalle->getPrecioEUR(), 2, ',', ''): '',
	        		'precioUSD' => $detalle->getPrecioUSD()? number_format($detalle->getPrecioUSD(), 2, ',', ''): '',
	        		'precioRBR' => $detalle->getPrecioRBR()? number_format($detalle->getPrecioRBR(), 2, ',', ''): '',
	        		'precioGBP' => $detalle->getPrecioGBP()? number_format($detalle->getPrecioGBP(), 2, ',', ''): '',
					'residencias' => ResidenciaType::toArray()
	    		));
        		
        	} else {
				return $app['twig']->render('Manager/CursosPresencialesDetalles/edit.html.twig', array(
					'cursoPresencial' => $cursoPresencial,
					'residencias' => ResidenciaType::toArray()
				));
        	}
        })->middleware($userIsLogged);

        $controllers->post('/save', function (Application $app) {
        	$id = $app['request']->get('id');
        	$semanas = trim($app['request']->get('semanas'));
        	$horas = trim($app['request']->get('horas'));
        	$residencia = $app['request']->get('residencia');
        	$precioEUR = trim($app['request']->get('precioEUR'));
        	$precioUSD = trim($app['request']->get('precioUSD'));
        	$precioRBR = trim($app['request']->get('precioRBR'));
        	$precioGBP = trim($app['request']->get('precioGBP'));
        	
        	$precioEURNum = $precioEUR!=''? str_replace(',', '.', $precioEUR): '';
        	$precioUSDNum = $precioUSD!=''? str_replace(',', '.', $precioUSD): '';
        	$precioRBRNum = $precioRBR!=''? str_replace(',', '.', $precioRBR): '';
        	$precioGBPNum = $precioGBP!=''? str_replace(',', '.', $precioGBP): '';

        	// Validación
        	$messages = array();
			if ($semanas=='') {
				$messages['semanas'] = 'El número de semanas es obligatorio';
			} else if (strlen($semanas)>CursoPresencialDetalle::semanas_LENGTH) {
				$messages['semanas'] = 'El número de semanas no es válido';
			}
			if ($horas=='') {
				$messages['horas'] = 'El número de horas es obligatorio';
			} else if (!TypeValidation::isInteger($horas)) {
				$messages['horas'] = 'El número de horas no es válido';
			}
			if ($residencia=='') {
				$messages['residencia'] = 'La residencia es obligatoria';
			}
			if ($precioEURNum!='' && !TypeValidation::isFloat($precioEURNum)) {
				$messages['precioEUR'] = 'El precio en euros no es válido';
			}
			if ($precioUSDNum!='' && !TypeValidation::isFloat($precioUSDNum)) {
				$messages['precioUSD'] = 'El precio en dólares no es válido';
			}
			if ($precioRBRNum!='' && !TypeValidation::isFloat($precioRBRNum)) {
				$messages['precioRBR'] = 'El precio en reales brasileños no es válido';
			}
			if ($precioGBPNum!='' && !TypeValidation::isFloat($precioGBPNum)) {
				$messages['precioGBP'] = 'El precio en libras no es válido';
			}
			if ($precioEURNum=='' && $precioUSDNum=='' && $precioRBRNum=='' && $precioGBPNum=='') {
				$messages['precio'] = 'Es obligatorio indicar al menos un precio';
			}
			
			$em = $app['em'];

        	$cursoPresencialId = CursosPresenciales::getId($app);
						
			$cursoPresencial = $em->find('Spanischool\Entity\CursoPresencial', $cursoPresencialId);
			
			// Si no hay errores
			if (count($messages)==0) {
				
	        	if ($id) {
	        		$detalle = $em->find('Spanischool\Entity\CursoPresencialDetalle', $id);
	        	} else {
	        		$detalle = new CursoPresencialDetalle();
	        		$cursoPresencial->addDetalle($detalle);
	        	}
	        	
	        	$detalle->setSemanas($semanas!=''? $semanas: NULL);
	        	$detalle->setHoras($horas!=''? $horas: NULL);
	        	$detalle->setResidencia($residencia);
	        	$detalle->setPrecioEUR($precioEURNum!=''? $precioEURNum: NULL);
	        	$detalle->setPrecioUSD($precioUSDNum!=''? $precioUSDNum: NULL);
	        	$detalle->setPrecioRBR($precioRBRNum!=''? $precioRBRNum: NULL);
	        	$detalle->setPrecioGBP($precioGBPNum!=''? $precioGBPNum: NULL);

	        	try {
	        		
		        	$em->persist($cursoPresencial);
		        	$em->flush();
	        	} catch (ApplicationException $e) {
	        		print_r($e->getMessages());
	        		die();
	        	}

	        	return $app->redirect('view?id='.$detalle->getId());
	        	
			} else {
				return $app['twig']->render('Manager/CursosPresencialesDetalles/edit.html.twig', array(
					'cursoPresencial' => $cursoPresencial,
	    			'id' => $id,
	        		'semanas' => $semanas,
	        		'horas' => $horas,
					'residencia' => $residencia,
	        		'precioEUR' => $precioEUR,
	        		'precioUSD' => $precioUSD,
	        		'precioRBR' => $precioRBR,
	        		'precioGBP' => $precioGBP,
					'residencias' => ResidenciaType::toArray(),
	        		'messages' => $messages
	    		));
        	}
        })->middleware($userIsLogged);
        
        $controllers->get('/delete', function (Application $app) {
        	$id = $app['request']->get('id');

        	$cursoPresencialId = CursosPresenciales::getId($app);

        	$em = $app['em'];
	       	
			$cursoPresencial = $em->find('Spanischool\Entity\CursoPresencial', $cursoPresencialId);
	       	$detalle = $em->find('Spanischool\Entity\CursoPresencialDetalle', $id);
	       	
	       	$cursoPresencial->getDetalles()->removeElement($detalle);
	       	
       		$em->persist($cursoPresencial);
       		$em->flush();
       		
       		return $app->redirect('main');
        })->middleware($userIsLogged);
        
		return $controllers;
    }
}
