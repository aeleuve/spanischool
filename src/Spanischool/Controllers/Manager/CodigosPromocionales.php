<?php
namespace Spanischool\Controllers\Manager;

use Spanischool\Library\TypeValidation;

use Spanischool\Entity\CodigoPromocional;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Silex\Application;
use Silex\ControllerProviderInterface;
use Silex\ControllerCollection;

class CodigosPromocionales implements ControllerProviderInterface {
	
	public function connect(Application $app) {
        $controllers = new ControllerCollection();

        // Comprobación de usuario autenticado
		$userIsLogged = function (Request $request) use ($app) {
	        if ($app['session']->get('user')===NULL) {
		        return $app->redirect('/manager/login');
		    }
		};
	
        $controllers->get('/index', function (Application $app) {
        	$id = $app['request']->get('id');
        	
        	Productos::setId($app, $id);

		   	return $app->redirect('main');
        })->middleware($userIsLogged);
        
        $controllers->get('/main', function (Application $app) {
        	$productoId = Productos::getId($app);
        	
        	$em = $app['em'];
        	
        	$sql = 'select p, cp
        		from Spanischool\Entity\Producto p
        		left join p.codigosPromocionales cp
        		where p.id = :productoId
        		order by cp.codigo, cp.descripcion, cp.id';
        	$producto = $em->createQuery($sql)->setParameter('productoId', $productoId)->getSingleResult();
        	
			return $app['twig']->render('Manager/CodigosPromocionales/main.html.twig', array(
				'producto' => $producto,
    			'codigosPromocionales' => $producto->getCodigosPromocionales()
    		));
		})->middleware($userIsLogged);
        
		$controllers->get('/view', function (Application $app) {
			$id = $app['request']->get('id');

        	$productoId = Productos::getId($app);
			
        	$em = $app['em'];

        	$producto = $em->find('Spanischool\Entity\Producto', $productoId);
        	
        	$codigoPromocional = $em->find('Spanischool\Entity\CodigoPromocional', $id);
       		if (!$codigoPromocional) {
       			return $app->redirect('main');
       		}
        	
			return $app['twig']->render('Manager/CodigosPromocionales/view.html.twig', array(
				'producto' => $producto,
				'codigoPromocional' => $codigoPromocional
    		));
        })->middleware($userIsLogged);
        
        $controllers->get('/edit', function (Application $app) {
        	$id = $app['request']->get('id');
        	
        	$productoId = Productos::getId($app);
			
	        $em = $app['em'];
	        	
        	$producto = $em->find('Spanischool\Entity\Producto', $productoId);
        	
        	if ($id) {
        		$codigoPromocional = $em->find('Spanischool\Entity\CodigoPromocional', $id);
        		
				return $app['twig']->render('Manager/CodigosPromocionales/edit.html.twig', array(
					'producto' => $producto,
        			'id' => $codigoPromocional->getId(),
	        		'codigo' => $codigoPromocional->getCodigo(),
	        		'descripcion' => $codigoPromocional->getDescripcion(),
	        		'precioEUR' => $codigoPromocional->getPrecioEUR()? number_format($codigoPromocional->getPrecioEUR(), 2, ',', ''): '',
	        		'precioUSD' => $codigoPromocional->getPrecioUSD()? number_format($codigoPromocional->getPrecioUSD(), 2, ',', ''): '',
	        		'precioRBR' => $codigoPromocional->getPrecioRBR()? number_format($codigoPromocional->getPrecioRBR(), 2, ',', ''): '',
	        		'precioGBP' => $codigoPromocional->getPrecioGBP()? number_format($codigoPromocional->getPrecioGBP(), 2, ',', ''): ''
	    		));
        		
        	} else {
				return $app['twig']->render('Manager/CodigosPromocionales/edit.html.twig', array(
					'producto' => $producto
				));
        	}
        })->middleware($userIsLogged);

        $controllers->post('/save', function (Application $app) {
        	$id = $app['request']->get('id');
        	$codigo = trim($app['request']->get('codigo'));
        	$descripcion = trim($app['request']->get('descripcion'));
        	$precioEUR = trim($app['request']->get('precioEUR'));
        	$precioUSD = trim($app['request']->get('precioUSD'));
        	$precioRBR = trim($app['request']->get('precioRBR'));
        	$precioGBP = trim($app['request']->get('precioGBP'));
        	
        	$precioEURNum = $precioEUR!=''? str_replace(',', '.', $precioEUR): '';
        	$precioUSDNum = $precioUSD!=''? str_replace(',', '.', $precioUSD): '';
        	$precioRBRNum = $precioRBR!=''? str_replace(',', '.', $precioRBR): '';
        	$precioGBPNum = $precioGBP!=''? str_replace(',', '.', $precioGBP): '';

        	// Validación
        	$messages = array();
			if ($codigo=='') {
				$messages['codigo'] = 'El código es obligatorio';
			} else if (strlen($codigo)>CodigoPromocional::codigo_LENGTH) {
				$messages['codigo'] = 'El código no es válido';
			}
			if ($descripcion=='') {
				$messages['descripcion'] = 'La descripción es obligatoria';
			} else if (strlen($descripcion)>CodigoPromocional::descripcion_LENGTH) {
				$messages['descripcion'] = 'La descripción no es válida';
			}
			if ($precioEURNum!='' && !TypeValidation::isFloat($precioEURNum)) {
				$messages['precioEUR'] = 'El precio en euros no es válido';
			}
			if ($precioUSDNum!='' && !TypeValidation::isFloat($precioUSDNum)) {
				$messages['precioUSD'] = 'El precio en dólares no es válido';
			}
			if ($precioRBRNum!='' && !TypeValidation::isFloat($precioRBRNum)) {
				$messages['precioRBR'] = 'El precio en reales brasileños no es válido';
			}
			if ($precioGBPNum!='' && !TypeValidation::isFloat($precioGBPNum)) {
				$messages['precioGBP'] = 'El precio en libras no es válido';
			}
			if ($precioEURNum=='' && $precioUSDNum=='' && $precioRBRNum=='' && $precioGBPNum=='') {
				$messages['precio'] = 'Es obligatorio indicar al menos un precio';
			}
			
			$em = $app['em'];

       		$productoId = Productos::getId($app);
			
			$producto = $em->find('Spanischool\Entity\Producto', $productoId);
			
			// Si no hay errores
			if (count($messages)==0) {
				
	        	if ($id) {
	        		$codigoPromocional = $em->find('Spanischool\Entity\CodigoPromocional', $id);
	        	} else {
	        		$codigoPromocional = new CodigoPromocional();
	        		$producto->addCodigoPromocional($codigoPromocional);
	        	}
	        	
	        	$codigoPromocional->setCodigo($codigo!=''? $codigo: NULL);
	        	$codigoPromocional->setDescripcion($descripcion!=''? $descripcion: NULL);
	        	$codigoPromocional->setPrecioEUR($precioEURNum!=''? $precioEURNum: NULL);
	        	$codigoPromocional->setPrecioUSD($precioUSDNum!=''? $precioUSDNum: NULL);
	        	$codigoPromocional->setPrecioRBR($precioRBRNum!=''? $precioRBRNum: NULL);
	        	$codigoPromocional->setPrecioGBP($precioGBPNum!=''? $precioGBPNum: NULL);

	        	try {
	        		
		        	$em->persist($producto);
		        	$em->flush();
	        	} catch (ApplicationException $e) {
	        		print_r($e->getMessages());
	        		die();
	        	}

	        	return $app->redirect('view?id='.$codigoPromocional->getId());
	        	
			} else {
				return $app['twig']->render('Manager/CodigosPromocionales/edit.html.twig', array(
					'producto' => $producto,
	    			'id' => $id,
	        		'codigo' => $codigo,
	        		'descripcion' => $descripcion,
	        		'precioEUR' => $precioEUR,
	        		'precioUSD' => $precioUSD,
	        		'precioRBR' => $precioRBR,
	        		'precioGBP' => $precioGBP,
	        		'messages' => $messages
	    		));
        	}
        })->middleware($userIsLogged);
        
        $controllers->get('/delete', function (Application $app) {
        	$id = $app['request']->get('id');

       		$productoId = Productos::getId($app);
			
        	$em = $app['em'];
	       	
			$producto = $em->find('Spanischool\Entity\Producto', $productoId);
	       	$codigoPromocional = $em->find('Spanischool\Entity\CodigoPromocional', $id);
	       	
	       	$producto->getCodigosPromocionales()->removeElement($codigoPromocional);
	       	
       		$em->persist($producto);
       		$em->flush();
       		
       		return $app->redirect('main');
        })->middleware($userIsLogged);
        
		return $controllers;
    }
}
