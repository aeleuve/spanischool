<?php
namespace Spanischool\Controllers\FrontEnd;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Silex\Application;
use Silex\ControllerProviderInterface;
use Silex\ControllerCollection;
use Spanischool\Types\TipoProductoType;

class CursosPresenciales implements ControllerProviderInterface {
	public function connect(Application $app) {
        $controllers = new ControllerCollection();
        
        //Controlador para la página inicial de cursos presenciales.
        $controllers->get('/', function (Application $app, Request $request, $lang) {
	        // Recuperar la moneda en uso
            $currency = $app['session']->get('user_currency');
	
			$em = $app['em'];
			$vallas = $em->createQuery('select cp from Spanischool\Entity\CartelPresencial cp where cp.activo = true and cp.lang = :lang order by cp.id')->setParameter('lang', $lang)->getResult();			
			$cursos = $em->createQuery('select c,det from Spanischool\Entity\CursoPresencial c join c.detalles det order by c.nombre')->getResult();
			
			return $app['twig']->render('cursosPresenciales/presenciales.html.twig', array(
				'lang' 		=> $lang, 
				'vallas'	=>$vallas,
				'uploadURL' => $app['upload.url'].\Spanischool\Controllers\Manager\CartelesPresenciales::UPLOADURL.'/',
				'cursos' 	=> $cursos,
				'moneda' 	=> $app['session']->get('user_currency'),
				'currency' 	=> $currency
			));
        });

		$controllers->get('/ficha/{curso_nombre}', function(Application $app, Request $request, $lang, $curso_nombre){
			// Recuperar la moneda en uso
            $currency = $app['session']->get('user_currency');
	        
			$em = $app['em'];
			$sql = 'select c,det from Spanischool\Entity\CursoPresencial c join c.detalles det where c.nombre = :nombreCurso';
			$sql_comunes = 'select cp from Spanischool\Entity\ComunPresencial cp';
        	$curso = $em->createQuery($sql)->setParameter('nombreCurso', $curso_nombre)->getResult();
        	$comunes = $em->createQuery($sql_comunes)->getResult();
			
			return $app['twig']->render('cursosPresenciales/ficha.html.twig', array(
				'curso'   	=> $curso,
				'comunes' 	=> $comunes,
				'lang'    	=> $lang,
				'moneda'  	=> $app['session']->get('user_currency'),
				'currency' 	=> $currency
			));
		});


        return $controllers; 
	}
}
?>