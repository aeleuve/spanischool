<?php
namespace Spanischool\Controllers\FrontEnd;

use Spanischool\Library\ShoppingCart;
use Spanischool\Types\ShoppingCartTypeType;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Silex\Application;
use Silex\ControllerProviderInterface;
use Silex\ControllerCollection;

class Correo implements ControllerProviderInterface {
	
	public function connect(Application $app) {
        $controllers = new ControllerCollection();

        // Cambiar la moneda en uso
        $controllers->post('/contacto', function (Application $app, Request $request) {
            //TODO Usar swiftmailer aquí...
            

            //TODO Recuerda redirigir a una vista dando las gracias ...
        });

        $controllers->post('/cuponDescuento', function (Application $app, Request $request, $lang) {
			$tipo = $request->get('tipo');
			
			switch ($lang){
				case 'es':
					$txt_exito='Solicitud procesada con éxito.';	
					
					if ($tipo == 'sol_cupon'){
						$peticion_txt = 'Nueva petición de código de descuento.';
						$greetings_txt = 'Muchas gracias por contactar, en breve nos pondremos en contacto con Usted para enviarle el cupón de descuento.';
						$asunto = 'Cupon de descuento';
					}else if ($tipo == 'sol_info'){
						$peticion_txt = 'Petición de información';
						$greetings_txt = 'Muchas gracias por contactar con nosotros, en breve irá recibiendo información y promociones del portal.';
						$asunto = 'Petición de información';						
					}else{
						$peticion_txt = 'Nueva petición de acceso al campus.';
						$greetings_txt = 'Muchas gracias por contactar, en breve nos pondremos en contacto para enviarle los datos de acceso.';
						$asunto = 'Solicitud de acceso al campus';
					}
					
				break;
				case 'en':
					$txt_exito='Your request has been processed successfully';					
					
					if ($tipo == 'sol_cupon'){
						$peticion_txt = 'New discount code request';
						$greetings_txt = 'Thank very much. We will send you your discount code shortly,';					
						$asunto = 'Discount coupon';
					}else if ($tipo == 'sol_info'){
						$peticion_txt = 'Information request';
						$greetings_txt = 'Thank very much. You will receive more information and news from our website shortly';
						$asunto = 'Information request';						
					}else{
						$peticion_txt = 'New campus access request';
						$greetings_txt = 'Thank very much. We will contact to send your login access shortly';					
						$asunto = 'Campus login access request';
					}
				break;
				case 'pt':
				break;
			}	
		    $message = \Swift_Message::newInstance();

		    $message->setSubject($asunto);
		    $message->setFrom(array($request->get('mail')));

		    $message->setTo($app['configuration']->mail());
		    $message->setBody($peticion_txt.' Email: '.$request->get('mail'));

		    //Now we send a new greeting mail to the user
		    $greeting_message = \Swift_Message::newInstance();

		    $greeting_message->setSubject($asunto);
		    // $message->setFrom($app['mail.adress']);
		    $greeting_message->setFrom('aramirezc@formaciondigital.com');
		    $greeting_message->setTo($request->get('mail'));
		    $greeting_message->setBody($greetings_txt);
			try{
		    	$app['mailer']->send($message);
				$app['mailer']->send($greeting_message);
				
				return 'exito##'.$txt_exito;
			}catch(\Exception $e){
				return 'Error##: '.$e->getMessage();
			}
		
        });

		$controllers->post('/contacto', function (Application $app, Request $request, $lang) {
			$body = '<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN"
			   "http://www.w3.org/TR/html4/strict.dtd">
				<html>
					<body>
						<table border="0" cellpadding="0" cellspacing="1" style="text-align:left" width="500px">
                            <tr>
                                <th><b>Nombre:</b></th>
                                <td>'.$request->get('nombre').'</td>
                                <th><b>Apellidos:</b></th>
                                <td>'.$request->get('apellidos').'</td>
                            </tr>
                            <tr>
                                <th><b>Empresa:</b></th>
                                <td>'.$request->get('empresa').'</td>
                                <th><b>Cargo:</b></th>
                                <td>'.$request->get('cargo').'</td>
                            </tr>
                            <tr>
                                <th><b>Mail:</b></th>
                                <td>'.$request->get('email').'</td>
                                <th><b>Teléfono:</b></th>
                                <td>'.$request->get('tlfn').'</td>
                            </tr>
                            <tr>
                                <th colspan="4">Consulta</th>
                            </tr>
                            <tr>
                                <td colspan="4">'.$request->get('consulta').'</td> 
                            </tr>
                        </table>​
					</body>
				</html>';
			
		    $message = \Swift_Message::newInstance();

		    $message->setSubject('Consulta www.spanischool.com');
		    $message->setFrom(array($request->get('email')));
		    $message->setTo($app['configuration']->mail());
		    $message->setBody($body, 'text/html');
		
			try{
		    	$app['mailer']->send($message);
				return $app['twig']->render('mensaje.html.twig', array('estado'=>_('estado_exito_msg'), 'mensaje'=>_('contacto_exito_msg'), 'error'=>false, 'lang'=>$lang));
				// return 'exito##'.$txt_exito;
			}catch(\Exception $e){
				return $app['twig']->render('mensaje.html.twig', array('estado'=>_('estado_error_msg'), 'mensaje'=>_('contacto_error_msg'), 'error'=>false, 'lang'=>$lang));
				// return 'Error##: '.$e->getMessage();
			}
		});
        
        return $controllers;
    }
}
