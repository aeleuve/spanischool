<?php
namespace Spanischool\Controllers\FrontEnd;

use Spanischool\Library\ShoppingCart;
use Spanischool\Types\ShoppingCartTypeType;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Silex\Application;
use Silex\ControllerProviderInterface;
use Silex\ControllerCollection;

class Compras implements ControllerProviderInterface {
	
	public function connect(Application $app) {
        $controllers = new ControllerCollection();

        // Necesitamos hacer referencia a este objeto desde las closures
        $_this = $this;
        
        // Si no existe el carro de compra, lo creamos
		$existShoppingCart = function (Request $request) use ($app) {
	        if ($app['session']->get('shoppingCart')===NULL) {
		    	$app['session']->start();
		        $app['session']->set('shoppingCart', new ShoppingCart());
		    }
		};
	
        //raiz de la pagina
        $controllers->get('/', function (Application $app, Request $request, $lang) {
            // Recuperar el carro de la compra
            $shoppingCart = $app['session']->get('shoppingCart');
            // Recuperar la moneda en uso
            $currency = $app['session']->get('user_currency');

            // Hidratar con la información del almacén de datos
            $shoppingCart->hydrate($app['em'], $currency);
			$total = 0;
			$subtotal = 0;
			foreach($shoppingCart as $item){
				if($item->getPromotionalPrice()!=null){
					$total += $item->getPromotionalPrice()*$item->getQuantity();
				}else {
					$total += $item->getPrice()*$item->getQuantity();
				}
				$subtotal += $item->getPrice()*$item->getQuantity();
			}

            return $app['twig']->render('FrontEnd/Compras/compras.html.twig', array(
                'lang' 			=> $lang,
                'shoppingCart' 	=> $shoppingCart,
				'currency' 		=> $currency,
				'total' 		=> $total,
				'subtotal' 		=> $subtotal,
				'nItems'		=> $shoppingCart->length()
            ));
        });

		// Lista del carro de la compra
        $controllers->get('/list', function (Application $app, $lang) {
		    // Recuperar el carro de la compra
		    $shoppingCart = $app['session']->get('shoppingCart');
		    // Recuperar la moneda en uso
		    $currency = $app['session']->get('user_currency');

		    // Hidratar con la información del almacén de datos
		    $shoppingCart->hydrate($app['em'], $currency);

        	return $app['twig']->render('FrontEnd/Compras/list.html.twig', array(
        		'shoppingCart' 	=> $shoppingCart,
        		'currency' 		=> $app['session']->get('user_currency'),
				'lang'			=> $lang,
				'nItems'		=> $shoppingCart->length()
        	));
		})->middleware($existShoppingCart);
                
        $controllers->post('/count', function (Application $app) {
                // Recuperar el carro de la compra
                $shoppingCart = $app['session']->get('shoppingCart');
                // Recuperar la moneda en uso
                $currency = $app['session']->get('user_currency');

                // Hidratar con la información del almacén de datos
                $shoppingCart->hydrate($app['em'], $currency);
                
                return new Response($shoppingCart->length(), 200, array('Content-Type' => 'text'));
        
        })->middleware($existShoppingCart);
		
		// Añadir producto al carro
        $controllers->get('/addProducto', function (Application $app) use ($_this) {
        	$id = $app['request']->get('id');
        	$cantidad = $app['request']->get('cantidad');
        	
	       	if (!$id || $id=='' || !$cantidad || $cantidad=='') {
	       		return new Response(json_encode(array('error' => 'Los parámetros de llamada no son válidos')), 200, array('Content-Type' => 'application/json'));
	       	}
	
	    	// Recuperar el carro de la compra
		    $shoppingCart = $app['session']->get('shoppingCart');
		    // Recuperar la moneda en uso
		    $currency = $app['session']->get('user_currency');
		    
		    // Añadir elemento
		    $shoppingCart->add(ShoppingCartTypeType::product, $id, $cantidad);
        	
        	return new Response($shoppingCart->toJSON($app['em'], $currency), 200, array('Content-Type' => 'application/json'));
        	
        })->middleware($existShoppingCart);

		// Añadir paquete al carro
        $controllers->get('/addPaquete', function (Application $app) use ($_this) {
        	$id = $app['request']->get('id');
        	$cantidad = $app['request']->get('cantidad');

	       	if (!$id || $id=='' || !$cantidad || $cantidad=='') {
	       		return new Response(json_encode(array('error' => 'Los parámetros de llamada no son válidos')), 200, array('Content-Type' => 'application/json'));
	       	}
	
	    	// Recuperar el carro de la compra
		    $shoppingCart = $app['session']->get('shoppingCart');
		    // Recuperar la moneda en uso
		    $currency = $app['session']->get('user_currency');
		    
		    // Añadir elemento
		    $shoppingCart->add(ShoppingCartTypeType::pack, $id, $cantidad);
        	
        	return new Response($shoppingCart->toJSON($app['em'], $currency), 200, array('Content-Type' => 'application/json'));
        	        	
        })->middleware($existShoppingCart);

        // Incrementar/decrementar la cantidad de un producto del carro
            $controllers->get('/increaseProducto', function (Application $app) use ($_this) {
        	$id = $app['request']->get('id');
        	$cantidad = $app['request']->get('cantidad');

	       	if (!$id || $id=='' || !$cantidad || $cantidad=='') {
	       		return new Response(json_encode(array('error' => 'Los parámetros de llamada no son válidos')), 200, array('Content-Type' => 'application/json'));
	       	}
	
	    	// Recuperar el carro de la compra
		    $shoppingCart = $app['session']->get('shoppingCart');
		    // Recuperar la moneda en uso
		    $currency = $app['session']->get('user_currency');
		    
		    // Añadir elemento
		    $shoppingCart->increase(ShoppingCartTypeType::product, $id, $cantidad);

        	return new Response($shoppingCart->toJSON($app['em'], $currency), 200, array('Content-Type' => 'application/json'));

        })->middleware($existShoppingCart);

        // Incrementar/decrementar la cantidad de un paquete del carro
        $controllers->get('/increasePaquete', function (Application $app) use ($_this) {
        	$id = $app['request']->get('id');
        	$cantidad = $app['request']->get('cantidad');

	       	if (!$id || $id=='' || !$cantidad || $cantidad=='') {
	       		return new Response(json_encode(array('error' => 'Los parámetros de llamada no son válidos')), 200, array('Content-Type' => 'application/json'));
	       	}
	
	    	// Recuperar el carro de la compra
		    $shoppingCart = $app['session']->get('shoppingCart');
		    // Recuperar la moneda en uso
		    $currency = $app['session']->get('user_currency');
		    
		    // Añadir elemento
		    $shoppingCart->increase(ShoppingCartTypeType::pack, $id, $cantidad);

        	return new Response($shoppingCart->toJSON($app['em'], $currency), 200, array('Content-Type' => 'application/json'));

        })->middleware($existShoppingCart);

        // Eliminar un producto del carro
        $controllers->get('/removeProducto', function (Application $app) use ($_this) {
        	$id = $app['request']->get('id');
        	
	       	if (!$id || $id=='') {
	       		return new Response(json_encode(array('error' => 'Los parámetros de llamada no son válidos')), 200, array('Content-Type' => 'application/json'));
	       	}
	
	    	// Recuperar el carro de la compra
		    $shoppingCart = $app['session']->get('shoppingCart');
		    // Recuperar la moneda en uso
		    $currency = $app['session']->get('user_currency');
		    
		    // Quitar elemento
		    $shoppingCart->remove(ShoppingCartTypeType::product, $id);

        	return new Response($shoppingCart->toJSON($app['em'], $currency), 200, array('Content-Type' => 'application/json'));

        })->middleware($existShoppingCart);
        
        // Eliminar un paquete del carro
        $controllers->get('/removePaquete', function (Application $app) use ($_this) {
        	$id = $app['request']->get('id');
        	
	       	if (!$id || $id=='') {
	       		return new Response(json_encode(array('error' => 'Los parámetros de llamada no son válidos')), 200, array('Content-Type' => 'application/json'));
	       	}
	
	    	// Recuperar el carro de la compra
		    $shoppingCart = $app['session']->get('shoppingCart');
		    // Recuperar la moneda en uso
		    $currency = $app['session']->get('user_currency');
		    
		    // Quitar elemento
		    $shoppingCart->remove(ShoppingCartTypeType::pack, $id);

        	return new Response($shoppingCart->toJSON($app['em'], $currency), 200, array('Content-Type' => 'application/json'));

        })->middleware($existShoppingCart);

        // Añadir un código promocional al carro
        $controllers->get('/addCodigo', function (Application $app) use ($_this) {
        	$codigo = $app['request']->get('codigo');
        	
	    	// Recuperar el carro de la compra
		    $shoppingCart = $app['session']->get('shoppingCart');
		    // Recuperar la moneda en uso
		    $currency = $app['session']->get('user_currency');
		    
		    // Añadir elemento
		    $shoppingCart->addPromotionalCode($codigo);

        	return new Response($shoppingCart->toJSON($app['em'], $currency), 200, array('Content-Type' => 'application/json'));
		    
        })->middleware($existShoppingCart);

        // Eliminar un código promocional del carro
        $controllers->get('/removeCodigo', function (Application $app) use ($_this) {
        	$codigo = $app['request']->get('codigo');
        	
	    	// Recuperar el carro de la compra
		    $shoppingCart = $app['session']->get('shoppingCart');
		    // Recuperar la moneda en uso
		    $currency = $app['session']->get('user_currency');
		    
		    // Quitar elemento
		    $shoppingCart->removePromotionalCode($codigo);

        	return new Response($shoppingCart->toJSON($app['em'], $currency), 200, array('Content-Type' => 'application/json'));
		    
        })->middleware($existShoppingCart);

        // Eliminar la lista completa de productos y paquetes del carro
        $controllers->get('/clear', function (Application $app) {
		    // Recuperar el carro de la compra
		    $shoppingCart = $app['session']->get('shoppingCart');
		    // Vaciar
		    $shoppingCart->clear();
    
		   	return new Response(json_encode(array()), 200, array('Content-Type' => 'application/json'));
        })->middleware($existShoppingCart);

        // Cambiar la moneda en uso
        $controllers->get('/changeCurrency', function (Application $app) {
        	$currency = $app['request']->get('currency');

	       	$app['session']->set('user_currency', $currency);
        
        	return new Response(json_encode(array()), 200, array('Content-Type' => 'application/json'));
        });

        // Cambiar la moneda en uso
        $controllers->get('/toJSON', function (Application $app) {
	    	// Recuperar el carro de la compra
		    $shoppingCart = $app['session']->get('shoppingCart');
		    // Recuperar la moneda en uso
		    $currency = $app['session']->get('user_currency');
		    
        	return new Response($shoppingCart->toJSON($app['em'], $currency), 200, array('Content-Type' => 'application/json'));
        });
        $controllers->post('/proccess', function(Application $app, Request $request, $lang) {
            // Recuperar el carro de la compra
            $shoppingCart = $app['session']->get('shoppingCart');
            // Recuperar la moneda en uso
            $currency = $app['session']->get('user_currency');

            // Recuperamos los datos de los alumnos para la plataforma
            $nombres = $request->get('alu_nombre');
            $apellidos = $request->get('alu_apellido');
            $emails = $request->get('alu_email');

            // Hidratar con la información del almacén de datos
            $shoppingCart->hydrate($app['em'], $currency);
            $total = 0;
            $subtotal = 0;
			
            
            $compra['nombre'] = $request->get('nombre');
            $compra['apellidos'] = $request->get('apellidos');
            $compra['nif'] = $request->get('id_fiscal');
            $compra['email']= $request->get('email');
            $compra['telefono']= $request->get('telefono');
            $compra['direccion']= $request->get('direccion');
            $compra['poblacion']= $request->get('poblacion');
            $compra['codigo_postal']= $request->get('cp');
            
            //Asigning shop code
            $gatewayConfig = $app['configuration']->payGateway();
            $compra['tienda'] = $gatewayConfig['tienda'];
            $compra['pasarela'] = $gatewayConfig['paypal'];

            if($request->get('metodo_pago')=='paypal'){
                $compra['tipo']= 'Tarjeta';
            } else {
                $compra['tipo']='Transferencia';
            }

            $compra['moneda'] = $currency;

            $detalles = array();
            
            foreach($shoppingCart as $item){
                $producto = $app['em']->find("Spanischool\Entity\Producto", $item->getId());
                $detalle['descripcion'] = $item->getQuantity() . ' ' . _($item->getDescription());
                if($item->getPromotionalPrice()!=null){
                        $detalle['precio_unidad'] = $item->getPromotionalPrice()*$item->getQuantity();
                        $total += $item->getPromotionalPrice()*$item->getQuantity();
                }else {
                        $detalle['precio_unidad'] = $item->getPrice()*$item->getQuantity();
                        $total += $item->getPrice()*$item->getQuantity();
                }
                $detalle['nombre'] = $nombres[$item->getId()];
                $detalle['apellidos'] = $apellidos[$item->getId()];
                $detalle['email'] = $emails[$item->getId()];
                $detalle['codigo'] = $producto->getCodigoPlataforma(); 
                $detalle['fecha_inicio'] = date('Y-m-d');
                if($producto->getCodigoPlataforma() && $producto->getDiasAcceso()){
                    $fechaFinal = new \DateTime();
                    $fechaFinal->add(new \DateInterval('P'.$producto->getDiasAcceso().'D'));
                    $detalle['fecha_fin'] = $fechaFinal->format('Y-m-d');
                }
                $detalles[] = $detalle;
            }
            $compra['detalles'] = $detalles;
            //die(json_encode($compra));
            $ch = curl_init($gatewayConfig['url_pasarela']);
            curl_setopt_array($ch, array(CURLOPT_POST => TRUE,
                                          CURLOPT_RETURNTRANSFER => TRUE,
                                          CURLOPT_POSTFIELDS => "&compra=".json_encode($compra),
                                          )
                              );
            $salida = curl_exec($ch);
            $json = json_decode($salida);
            if(isset($json->error)){
                return "Error";
            } else if(isset($json->inputs)) {
                return $app['twig']->render('FrontEnd/Compras/redireccionpago.html.twig',
                    array('lang'=>$lang, 'json'=>$json, 'gateway' => $request->get('metodo_pago')));
            } else if(isset($json->transferencia) && $json->transferencia=='ok'){
                $email = $compra['email'];
                $message = _('msgtransferencia');
                
                $message = str_replace("###TOTAL###", $total, $message);
                $message = str_replace("###CURRENCY###", $app['session']->get('user_currency'), $message);
                $message = str_replace("###ID###", $json->id, $message);
                $message = str_replace("###EMAIL###", $compra['email'], $message);
                
                $m = \Swift_Message::newInstance();
                $m->setSubject('Pay on Spanischool');
                $m->setFrom($app['configuration']->mail());
                $m->setBcc(array($app['configuration']->mail()));
                $m->setBody($message, 'text/html');
                $app['mailer']->send($m);
            
                return $app['twig']->render('mensaje.html.twig', array('estado'=>_('estado_exito_msg'), 'mensaje' => $message, 'error' => false, 'lang' => $lang));
            } else {
                return $app['twig']->render('mensaje.html.twig', array('estado'=>_('estado_error_msg'), 'mensaje' => $salida, 'error' =>true, 'lang' => $lang));
            }
        });

        $controllers->get('/ok', function(Application $app, $lang){
            return $app['twig']->render('mensaje.html.twig', array('estado'=>_('estado_exito_msg'),'mensaje' => 'El proceso de compra ha finalizado correctamente', 'error'=>false, 'lang'=>$lang));
        });
        
        $controllers->get('/ko', function(Application $app, $lang){
            return $app['twig']->render('mensaje.html.twig', array('estado'=>_('estado_error_msg'), 'mensaje' => 'El proceso de compra se ha interrumpido', 'error'=>true, 'lang'=>$lang));
        });

        return $controllers;
    }
}
