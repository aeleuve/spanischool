<?php
namespace Spanischool\Controllers\FrontEnd;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Silex\Application;
use Silex\ControllerProviderInterface;
use Silex\ControllerCollection;
use Spanischool\Types\TipoProductoType;

class CursosOnline implements ControllerProviderInterface {
	
	public function connect(Application $app) {
        $controllers = new ControllerCollection();
        
        //Controlador para la página inicial de cursos online.
        $controllers->get('/', function (Application $app, Request $request, $lang) {
	        // Recuperar la moneda en uso
            $currency = $app['session']->get('user_currency');
	
			$em = $app['em'];
			$query    = $em->createQuery('select p from Spanischool\Entity\Producto p where p.tipo = :tipo order by p.nivel');
			$cursos   = $query->setParameter('tipo', 'curso')->getResult();
			$one2one  = $query->setParameter('tipo', 'oneToOne')->getResult();
			$examenes = $query->setParameter('tipo', 'examen')->getResult();
			$pildoras = $query->setParameter('tipo', 'pildora')->getResult();
			
            return $app['twig']->render('cursosOnline/cursos_on.html.twig', array(
	            	'lang'     => $lang,
					'cursos'   => $cursos,
					'one2one'  => $one2one,
					'examenes' => $examenes,
					'pildoras' => $pildoras,
					'moneda'   => $app['session']->get('user_currency'),
					'currency' => $currency
            ));
        });

		$controllers->get('/ficha/{curso_nombre}', function(Application $app, Request $request, $lang, $curso_nombre){
			$em = $app['em'];
			$sql = 'select p from Spanischool\Entity\Producto p where p.nombre = :nombreCurso';
        	$item = $em->createQuery($sql)->setParameter('nombreCurso', $curso_nombre)->getSingleResult();
			
			return $app['twig']->render('cursosOnline/ficha.html.twig', array(
				'item'  => $item,
				'lang'   => $lang,
				'moneda' => $app['session']->get('user_currency')
			));
		});
		        
        return $controllers;
    }
}
