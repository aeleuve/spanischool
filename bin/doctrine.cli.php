<?php


use Doctrine\ORM\Configuration,
    Doctrine\ORM\EntityManager,
    Doctrine\Common\Cache\ArrayCache,
    Doctrine\DBAL\Logging\EchoSqlLogger,
    Symfony\Component\ClassLoader\UniversalClassLoader;


define('DB', 'Database');

require_once __DIR__ . '/../vendor/symfony/class-loader/Symfony/Component/ClassLoader/UniversalClassLoader.php';


$classLoader = new Symfony\Component\ClassLoader\UniversalClassLoader();
$classLoader->registerNamespaces(
                array(
                    'Doctrine\ORM'      => __DIR__.'/../vendor/doctrine/orm/lib',
					'Doctrine\Common'   => __DIR__.'/../vendor/doctrine/common/lib',
					'Doctrine\DBAL'     => __DIR__.'/../vendor/doctrine/dbal/lib',
					'Symfony\Component' => __DIR__.'/../vendor/doctrine/orm/lib/vendor',
					'Spanischool'       => __DIR__.'/../src',
                    )
                );

$appConfiguration = new Spanischool\Configuration('DEVELOPMENT');

$configuration = new Configuration();
$configuration->setMetadataCacheImpl($appConfiguration->doctrineCache());
$driverImpl = $configuration->newDefaultAnnotationDriver($appConfiguration->doctrineEntitiesPath());
$configuration->setMetadataDriverImpl($driverImpl);
$configuration->setQueryCacheImpl($appConfiguration->doctrineCache());
$configuration->setProxyDir($appConfiguration->doctrineProxiesPath());
$configuration->setProxyNamespace($appConfiguration->doctrineProxiesNamespace());
$configuration->setAutoGenerateProxyClasses(true);

$em = EntityManager::create($appConfiguration->doctrine(), $configuration);

$helperSet = new \Symfony\Component\Console\Helper\HelperSet(array(
    'db' => new Doctrine\DBAL\Tools\Console\Helper\ConnectionHelper($em->getConnection()),
    'em' => new Doctrine\ORM\Tools\Console\Helper\EntityManagerHelper($em)
));
?>
