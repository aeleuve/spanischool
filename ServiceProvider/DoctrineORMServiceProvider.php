<?php
namespace ServiceProvider;

use Silex\Application;
use Silex\ServiceProviderInterface;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Configuration;
use Doctrine\Common\Cache\ArrayCache;
use Doctrine\Common\Cache\ApcCache;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\DriverManager;
use Doctrine\Common\EventManager;
use Doctrine\Common\Cache\MemcacheCache;

class DoctrineORMServiceProvider implements ServiceProviderInterface {
    public function register(Application $app){
        $app['orm.configuration'] = $app->share(function($app){
            $configuration = new Configuration();
            $configuration->setMetadataCacheImpl($app['configuration']->doctrineCache());
            $driverImpl = $configuration->newDefaultAnnotationDriver($app['configuration']->doctrineEntitiesPath());
            $configuration->setMetadataDriverImpl($driverImpl);
            $configuration->setQueryCacheImpl($app['configuration']->doctrineCache());
            $configuration->setProxyDir($app['configuration']->doctrineProxiesPath());
            $configuration->setProxyNamespace($app['configuration']->doctrineProxiesNamespace());
            $configuration->setAutogenerateProxyClasses(false);
            if($app['environment']=='DEVELOPMENT'){
                $configuration->setAutogenerateProxyClasses(true);
				$configuration->setSQLLogger(new DoctrineLogger());
            }
            return $configuration;
        });

        $app['orm.connection'] = $app->share(function($app){
            return DriverManager::getConnection($app['configuration']->doctrine(), $app['orm.configuration'], new EventManager());
        });

        $app['em'] = $app->share(function($app) {
            return EntityManager::create($app['orm.connection'], $app['orm.configuration'], $app['orm.connection']->getEventManager());
        });
		
		// Creamos un modo abreviado de acceder a escribir en el log de doctrine
		if($app['environment']=='DEVELOPMENT'){
			$app['log'] = $app['em']->getConfiguration()->getSQLLogger();
		}
    }
}
