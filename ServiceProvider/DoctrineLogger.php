<?php
namespace ServiceProvider;

use Doctrine\DBAL\Logging\SQLLogger;

use \DateTime;

class DoctrineLogger implements SQLLogger {
	
	private $logFile;

	public function __construct() {
		$this->logFile = __DIR__.'/../log/doctrine.log';
	}
	
	private function writeLine($line) {
    	$file = fopen($this->logFile, 'a');
		fwrite($file, date('d-m-Y H:i'));
    	fwrite($file, ' ');
    	fwrite($file, $line);
    	fwrite($file, "\n");
    	fclose($file);
	}
	
    public function startQuery($sql, array $params = null, array $types = null) {
    	
    	$line = $sql;
    	
        if ($params) {
    		$line .= ' -> ( ';
			
    		foreach ($params as $i => $param) {
   				if ($i!=0) {
   					$line .= ', ';
   				}
    			if ($param instanceof DateTime) {
					$line .= $param->format('d-m-Y H:i:s');
					
        		} else if (is_array($param)) {
        			$line .= '{ ';
        			foreach ($param as $j => $elem) {
        				if ($j!=0) {
        					$line .= ', ';
        				}
        				$line .= $elem;
        			}
        			$line .= ' }';
        			
        		} else {
					$line .= $param;
        		}
    		}
    		
    		$line .= ' )';
        }
		$this->writeLine($line);
    }
    
    public function stopQuery() {}
    
    public function write($text) {
    	$this->writeLine($text);
    }
    
    public function reset() {
    	if (file($this->logFile)) {
	    	unlink($this->logFile);
    	}
    }
}
