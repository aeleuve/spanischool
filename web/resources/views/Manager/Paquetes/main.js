function abrirVentanaProductos(url, paquete) {
	// Cargar la lista de productos candidatos
	$("#listaProductos").load(url, { paquete: paquete }, function() {
		// Si no hay productos disponibles (están todos asociados)
		if ($("#listaProductos tr").length===0) {
			alert('No hay productos candidatos a incluir en el paquete');
		} else {
			$("#anadirProducto").modal("show");
		}
	});
}

function guardarProductos(url, paquete) {
	// Lista con los elementos seleccionados
	var productosId = "";
	// Se cogen los checks seleccionados de las filas de la tabla de productos
	$.each($("#listaProductos input[type='checkbox']:checked"), function(index, element) {
		if (productosId!=="") {
			productosId += ",";
		}
		productosId += $(element).attr('rel');
	});
	
	// Si hay productos seleccionados
	if (productosId!=="") {
		// Guardar la asociación con los productos seleccionados
		$.ajax({
			url: url,
			type: "POST",
			async: false,
			data: $.param({ paquete: paquete, productosId: productosId }),
			success: function() {
				$("#refreshForm").submit();
			},
			// Si hubo error en la llamada
			error: function(jqXHR, textStatus, errorThrown) {
				alert("Se ha producido un error:\n"+errorThrown);
			}
		});
	}
	// Cerramos la ventana (podemos ponerlo aquí porque la llamada es síncrona)
	$("#anadirProducto").modal("hide");
}

function quitarProducto(url, paquete, producto) {
	if (confirm("¿Desea quitar el producto del paquete?")) {
		// Quitar la asociación del producto seleccionado
		$.ajax({
			url: url,
			type: "POST",
			async: false,
			data: $.param({ paquete: paquete, producto: producto }),
			success: function() {
				$("#refreshForm").submit();
			},
			// Si hubo error en la llamada
			error: function(jqXHR, textStatus, errorThrown) {
				alert("Se ha producido un error:\n"+errorThrown);
			}
		});
	}
}
