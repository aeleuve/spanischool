<?php
define('ENVIRONMENT', 'DEVELOPMENT');
define('BASEPATH', 'http://spanischool');
define('UPLOADDIR', __DIR__.'/uploads');
define('UPLOADURL', '/uploads');

if(ENVIRONMENT=='DEVELOPMENT'){
	error_reporting(E_ALL);
	ini_set('display_errors', 1);
}

use Symfony\Component\ClassLoader\UniversalClassLoader;
use Spanischool\Library\I18n;

ini_set('display_errors', 1);
require_once __DIR__.'/../vendor/symfony/class-loader/Symfony/Component/ClassLoader/UniversalClassLoader.php';
require_once __DIR__.'/../vendor/pimple/pimple/lib/Pimple.php';
require_once __DIR__.'/../locales.php';

//Autocarga de vendors
$loader = new UniversalClassLoader();
$loader -> registerNamespaces(array(
    'Silex'  => __DIR__.'/../vendor/silex/silex/src/',
    'Symfony\Component\HttpKernel'      => __DIR__.'/../vendor/symfony/http-kernel',
    'Symfony\Component\EventDispatcher' => __DIR__.'/../vendor/symfony/event-dispatcher',
    'Symfony\Component\Routing'         => __DIR__.'/../vendor/symfony/routing',
    'Symfony\Component\HttpFoundation'  => __DIR__.'/../vendor/symfony/http-foundation',
    'Doctrine\ORM'                      => __DIR__.'/../vendor/doctrine/orm/lib',
    'Doctrine\Common'                   => __DIR__.'/../vendor/doctrine/common/lib',
    'Doctrine\DBAL'                     => __DIR__.'/../vendor/doctrine/dbal/lib',
    'ServiceProvider'                   => __DIR__.'/..',
    'Spanischool'                       => __DIR__.'/../src',
));
$loader -> register();

//Inicio de Silex
$app = new Silex\Application();
if(ENVIRONMENT=='DEVELOPMENT'){
    $app['debug']=true;
}

$app['environment'] = ENVIRONMENT;
$app['upload.url'] = UPLOADURL;

$app['configuration'] = $app->share(function($app){
   return new Spanischool\Configuration($app['environment']);
});

//Registro de Twig
$app->register(new Silex\Provider\TwigServiceProvider(), $app['configuration']->twig());

//Carga de TwigExtensions
require_once __DIR__.'/../vendor/twig/extensions/lib/Twig/Extensions/Autoloader.php';
Twig_Extensions_Autoloader::register();
$app['twig']->addExtension(new Twig_Extensions_Extension_I18n());

//Registro de SwiftMailer
$app->register(new Silex\Provider\SwiftmailerServiceProvider(), array(
    'swiftmailer.class_path' => __DIR__.'/../vendor/swiftmailer/swiftmailer/lib/classes',
    'swiftmailer.options'    => $app['configuration']->swiftMailer()
));

// Filtro para construir la ruta base de la aplicación
function basePath($url){
    return BASEPATH.$url;
}

function basePathLang($url, $lang){
	return BASEPATH.'/'.$lang.$url;
}

function currencyFormat($amount, $currency) {
    global $locales;
    switch ($currency) {
		case 'EUR': setlocale(LC_MONETARY, $locales['es']); break;
        case 'USD': setlocale(LC_MONETARY, $locales['us']); break;
		case 'RBR': setlocale(LC_MONETARY, $locales['pt']); break;
		case 'GBP': setlocale(LC_MONETARY, $locales['en']); break;
	}
    return money_format('%.2n',$amount);
}

function currencyValue($producto, $currency){
		switch ($currency){
			case 'EUR':
				return currencyFormat($producto->getPrecioEUR(), $currency);
			case 'USD':
				return currencyFormat($producto->getPrecioUSD(), $currency);
			case 'GBP':
				return currencyFormat($producto->getPrecioGBP(), $currency);
			default:
				return currencyFormat($producto->getPrecioRBR(), $currency);
		}
}

function str_replace_twig($string, $search, $replace){
	return str_replace($search, $replace, $string);
}

function findString($string, $needle){
	if (strpos($string, $needle) === FALSE) return false;
	else return true;
}



function truncate($text, $max = 30){
    $lastSpace = 0;

    if (strlen($text) >= $max)
    {
        $text = substr($text, 0, $max);
        // $lastSpace = strrpos($text,' ');
        // $text = substr($text, 0, $lastSpace).'...';
    }

    return $text;	
}

$app['twig']->addFilter('basePath', new Twig_Filter_Function('basePath'));
$app['twig']->addFilter('basePathLang', new Twig_Filter_Function('basePathLang'));
$app['twig']->addFilter('currencyValue', new Twig_Filter_Function('currencyValue'));
$app['twig']->addFilter('currencyFormat', new Twig_Filter_Function('currencyFormat'));
$app['twig']->addFilter('str_replace', new Twig_Filter_Function('str_replace_twig'));
$app['twig']->addFilter('truncate', new Twig_Filter_Function('truncate'));
$app['twig']->addFilter('findString', new Twig_Filter_Function('findString'));

// Registro de Session Provider 
$app->register(new Silex\Provider\SessionServiceProvider(), array(
	'session.storage.options'=>array('name'=>'spanischool')
));

//Registro de DoctrineORM
$app->register(new ServiceProvider\DoctrineORMServiceProvider());

$app->register(new Silex\Provider\SessionServiceProvider(), array(
	'session.storage.options' => array(
		'name' => 'spanischool'
	)
));

$app->register(new Silex\Provider\SessionServiceProvider());

$app->before(function(Symfony\Component\HttpFoundation\Request $request) use ($app, $locales){
    $app['session']->start();
    /*$accepted_langs = array('en'=>'en_GB', 'pt'=>'pt_BR', 'es'=>'es_ES');*/
    $accepted_langs = array('en'=>'en_GB', 'es'=>'es_ES');
    $default_lang = 'en';
    //Si no recibimos la variable lang y no se ha creado la variable de sesión
    if(!$request->get('lang') && !$app['session']->has('user_language')){
        if(array_key_exists($request->getPreferredLanguage(), $accepted_langs)){
            $app['session']->set('user_language', $request->getPreferredLanguage());
        } else {
            $app['session']->set('user_language', $default_lang);
        }
        switch ($app['session']->get('user_language')) {
        	case 'en': $app['session']->set('user_currency', 'GBP'); break;
        	case 'pt': $app['session']->set('user_currency', 'RBR'); break;
        	case 'es': $app['session']->set('user_currency', 'EUR'); break;
        }
    }
    //Si recibimos lang y no existe la variable de sesión o existe y es distinta a lang
    else if ($request->get('lang') && (!$app['session']->has('user_language') || $app['session']->get('user_language')!=$request->get('lang'))){
        if(array_key_exists($request->get('lang'), $accepted_langs)){
            $app['session']->set('user_language', $request->get('lang'));
        } else {
            $app['session']->set('user_language', $default_lang);
            return $app->redirect(str_replace('/'.$request->get('lang').'/', '/'.$default_lang.'/', $request->getRequestUri()));
        }
        switch ($app['session']->get('user_language')) {
        	case 'en': $app['session']->set('user_currency', 'GBP'); break;
        	case 'pt': $app['session']->set('user_currency', 'RBR'); break;
        	case 'es': $app['session']->set('user_currency', 'EUR'); break;
        }
    }
    I18n::dict($locales[$app['session']->get('user_language')]);
});
$app->mount('/manager', new Spanischool\Controllers\Manager\Main());
$app->mount('/manager/productos', new Spanischool\Controllers\Manager\Productos());
$app->mount('/manager/codigosPromocionales', new Spanischool\Controllers\Manager\CodigosPromocionales());
$app->mount('/manager/paquetes', new Spanischool\Controllers\Manager\Paquetes());
$app->mount('/manager/carteles', new Spanischool\Controllers\Manager\Carteles());
$app->mount('/manager/cartelesPresenciales', new Spanischool\Controllers\Manager\CartelesPresenciales());
$app->mount('/manager/comunesPresenciales', new Spanischool\Controllers\Manager\ComunesPresenciales());
$app->mount('/manager/cursosPresenciales', new Spanischool\Controllers\Manager\CursosPresenciales());
$app->mount('/manager/cursosPresencialesDetalles', new Spanischool\Controllers\Manager\CursosPresencialesDetalles());
//$app->mount('/compras', new Spanischool\Controllers\FrontEnd\Compras());
$app->mount('/{lang}/correo', new Spanischool\Controllers\FrontEnd\Correo());
$app->mount('/{lang}/cursos_online', new Spanischool\Controllers\FrontEnd\CursosOnline);
$app->mount('/{lang}/cursos_presenciales', new Spanischool\Controllers\FrontEnd\CursosPresenciales);
$app->mount('/{lang}/compras', new Spanischool\Controllers\FrontEnd\Compras());

$app->get('/{lang}/site/{page}', function($page, $lang) use ($app){
	return $app['twig']->render('site/'.$page.'.html.twig', array('lang'=>$lang));
});

$app->get('/{lang}/home', function($lang) use ($app){
	$em = $app['em'];
	$vallas = $em->createQuery('select c from Spanischool\Entity\Cartel c where c.activo = true and c.lang = :lang order by c.id')->setParameter('lang', $lang)->getArrayResult();
	
	return $app['twig']->render('home.html.twig', array(
		'vallas'=>$vallas,
		'lang'=>$lang,
		'uploadURL' => UPLOADURL.Spanischool\Controllers\Manager\Carteles::UPLOADURL.'/'
		));
});

$app->get('/', function() use ($app){
	return $app->redirect('/'.$app['session']->get('user_language').'/home');
});

$app->run();
?>
