function initFicha(){
	var allTags = $('#tags_nav');
	var allBodies = $('#tags_body');
	var first_tag = allTags.children().first();
	
	//marcar la primera pestaña cursos online.
	first_tag.addClass('tag-pulsado');
	var pest_activa = first_tag.attr('id') + '-content';
	$('#'+pest_activa).removeClass('oculto');

	
	allTags.children().each(function(){
		$(this).click(function(){
			// if (!$(this).hasClass('tag-pulsado')){
				var elem = $(this).attr('id');
				var body = elem + '-content';
				
				allBodies.children().each(function(){
					if (!$(this).hasClass('oculto')){
						$(this).fadeOut('fast',function(){
							$(this).addClass('oculto');
							$('#'+body).fadeIn('fast', function(){
								if ($(this).hasClass('oculto')) $(this).removeClass('oculto');	
							});							
						});
					}
				});				
				//cambiar boton pulsado.
				$(this).siblings().removeClass('tag-pulsado');
				$(this).addClass('tag-pulsado');
			//}
		});
	});
	
	//modificar (dejar activo el boton cursos online) clase botones menu.
	var contentMenu = $('#botoneraItems .items_menu');
	contentMenu.children().each(function(){
		var link = $(this).children().first();
		var txtOriginal = link.html();
		
		if (link.attr('href').indexOf('cursos_online') != -1){
			$(this).addClass('item_menu_pulsado');
			// link.remove();
			//$(this).html(txtOriginal);
			link.addClass('azul');
			$(this).hover(
				function(){
					$(this).addClass('item_menu_pulsado');
				},
				function(){
					$(this).addClass('item_menu_pulsado');
				}
			);
		}
	});	
	
	//posicion background.
	var e = $('#tag-contenidos-content .texto');
	var tam = e.outerHeight(true)+41;
	if (tam > 650) tam = tam;
	else tam = 650;
	// 
	var n = tam - 650;
	var m_top = n+40+'px';
	// 
	var prod = $('#columna-productos');
	// var pos_back = e.position();
	// var pos_top = pos_back.top;
	// var pos_inf = pos_top + tam;
	// var pos_final = pos_inf-110;

	prod.css('top', m_top);
	
	
	//tooltips
	if ($('#tag-contenidos').hasClass('tag-pulsado')) tooltip_init();
}

function tooltip_init(){
	var tooltip = $('#productos-tooltip');
	var todostooltips = $('#productos-tooltip').children();
	var items = $('.productos-incluidos').children();
	
	
	items.each(function(){
		$(this).hover(
			function(){
				var id_item = $(this).attr('id');				
				var pos = id_item.indexOf('-li');
				var item_capa = $('#'+id_item.substring(0, pos));
				var item_position = $(this).position();
				
				todostooltips.hide();
				tooltip.css({'top':item_position.top, 'left':item_position.left - 400});
				if (tooltip.css('display') == 'none') tooltip.show();
				item_capa.show();
			},
			function(){
				var item_capa = $(this).attr('id') + '-li';
				if (tooltip.css('display') == 'block') tooltip.hide();
				item_capa.hide();
			}
		);
	});
}

function verDemo(e){
	var allTags = $('#tags_nav');
	var allBodies = $('#tags_body');
	var pestana = $('#tag-demo');
	var content = $('#tag-demo-content');
	
	allTags.children().each(function(){
		if ($(this).hasClass('tag-pulsado')) $(this).removeClass('tag-pulsado');
	});
	
	allBodies.children().each(function(){
		if ($(this).css('display') != 'none') $(this).hide();
		if (!$(this).hasClass('oculto')) $(this).removeClass('oculto');
	});
	
	if (!pestana.hasClass('tag-pulsado')) pestana.addClass('tag-pulsado');
	content.fadeIn('fast', function(){ $(this).removeClass('oculto') });
}

function linkDemoFancy(){
	var content = $('<div></div');
	var c_contenidos = $('<div></div');
	
	content.addClass('oculto');
	c_contenidos.attr('id','demo-video');
	
	c_contenidos.text('hola');
	content.append(c_contenidos);
	
	$('#contents-online').prepend(content);
}
$(document).ready(function(){
    $('.btn_carrito').click(function(){
        $.get(ruta+'/compras/addProducto?id='+curso_id+'&cantidad=1', function(){
			window.location.href=ruta+"/compras/";
		});
    });
});
