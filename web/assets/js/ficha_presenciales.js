function initFichaPresenciales(){
	$('.iframe').fancybox({
		'width':480,
		'height':385
	});
	
	var allTags = $('#tags_nav');
	var allBodies = $('#tags_body');
	
	allTags.children().each(function(){
		$(this).click(function(){
			if (!$(this).hasClass('tag-pulsado')){
				var elem = $(this).attr('id');
				var body = elem + '-content';

				allBodies.children().each(function(){
					if (!$(this).hasClass('oculto') && !$(this).hasClass('fb-like') && !$(this).hasClass('clear')){
						//añadir padding a las pestañas
						$(this).fadeOut('fast',function(){
							$(this).addClass('oculto');
							$('#'+body).fadeIn('fast', function(){
								if ($(this).hasClass('oculto')) $(this).removeClass('oculto');	
							});							
						});
					}
				});				
				
				//cambiar boton pulsado.
				$(this).siblings().removeClass('tag-pulsado');
				$(this).addClass('tag-pulsado');
			}
		});
	});
	
	//modificar (dejar activo el boton cursos presenciales) clase botones menu.
	var contentMenu = $('#botoneraItems .items_menu');
	contentMenu.children().each(function(){
		var link = $(this).children().first();
		var txtOriginal = link.html();
		
		if (link.attr('href').indexOf('cursos_presenciales') != -1){
			$(this).addClass('item_menu_pulsado');
			// link.remove();
			//$(this).html(txtOriginal);
			$(this).hover(
				function(){
					$(this).addClass('item_menu_pulsado');
				},
				function(){
					$(this).addClass('item_menu_pulsado');
				}
			);
		}
	});	
}