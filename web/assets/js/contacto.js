function initContacto(){
	//capa fancy
	$('#l_notalegal').fancybox();
	
	$('#nota_legal').click(function(){
		if ($('#error-politica').length > 0){
			$('#error-politica').slideUp('fast', function(){ $(this).remove(); });
		}		
	});
	
	$('#form_contacto').find('input, textarea').each(function(){
		$(this).focus(function(){
			if ($('#error-politica').length > 0){
				$('#error-politica').slideUp('fast', function(){ $(this).remove(); });
			}		
		});
	});
}

function resetFields(){
	$('#form_contacto').each (function(){
	  this.reset();
	});
}

function validaContacto(){
	var c_url = document.URL;
	var ubi = $('#botonesFormu');
	var btn_cerrar = $('<div></div>');
	var error_string = '';
	var content_msg = $('<div></div>');		
	
	btn_cerrar.attr({'class':'cerrar right'});
	content_msg.attr({'id':'error-politica', 'class':'error-compra rounded-border-auto'});		
		
	if ($('#nota_legal').is(':checked')){
		var formu = $('#form_contacto');
		var nombre = $('#nombre');
		var apellidos = $('#apellidos');
		var empresas = $('#empresas');
		var cargo = $('#cargo');
		var email = $('#email');
		var tlfn = $('#telefono');
		var consulta = $('#consulta');
		var errores = false;
		

		if (compruebaCampoVacio(nombre.val())) errores = marcaError(nombre.prev());
		if (compruebaCampoVacio(apellidos.val())) errores = marcaError(apellidos.prev());
		if (compruebaCampoVacio(consulta.val())) errores = marcaError(consulta.prev());
		if (compruebaCampoVacio(email.val())) errores = marcaError(email.prev().prev());
		else if (!validarEmail(email.val())) var errorMail = marcaError(email.prev());	

		if (!errores && !errorMail){
			formu.submit();
			// $.post(ruta+'/correo/contacto', {nombre:nombre, ape:apellidos, empresas:empresas, cargo:cargo, email:email, tel:tlfn, consulta:consulta},function(data){
			// 				var msg = data.split('##');
			// 				if (msg[0] == 'exito'){
			// 					
			// 				}
			// 			});
		}else{
			if (c_url.indexOf('/es/') != -1) {
				if (errorMail) error_string="La dirección de correo no es válida";
				else error_string="Tiene que completar los campos obligatorios";
			}
				
			if (c_url.indexOf('/en/') != -1) {
				if (errorMail) error_string="You must enter a valid e-mail";
				else error_string="You must enter required files";
			}
			
			content_msg.append(error_string, btn_cerrar);
			if ($('#error-politica').length == 0){
				ubi.after(content_msg);
				content_msg.slideDown();
				btn_cerrar.click(function(){
					content_msg.slideUp('fast',function(){ $(this).remove(); });
				});
			}			
		}
		
	}else{		
		if (c_url.indexOf('/es/') != -1) error_str="Debe leer y aceptar la nota legal";
		if (c_url.indexOf('/en/') != -1) error_str="You must read terms and conditions.";

		content_msg.append(error_str, btn_cerrar);
		
		if ($('#error-politica').length == 0){
			ubi.after(content_msg);
			content_msg.slideDown();
			btn_cerrar.click(function(){
				content_msg.slideUp('fast',function(){ $(this).remove(); });
			});
		}
	}
}

function marcaError(e){		
	e.addClass("rojo").delay(6000).queue(function(next){
	    $(this).removeClass("rojo");
	    next();
	});
	
	return true;
}