function initCompras(){
    $('.compra_borrar').click(function(){
        var id=$(this).attr('rel');
        $.get(ruta+'/compras/removeProducto?id='+id, function(){
            window.location.href=ruta+"/compras/";
        });
    });
	$('#addProducts').click(function(){
		window.location.href=ruta+'/cursos_online/';
	});
	$('#btn_codigo_descuento').click(function(){
		var codigo = $('#codigo_descuento').val();
		$.get(ruta+'/compras/addCodigo?codigo='+codigo, function(){
			window.location.href=ruta+"/compras/";
		});
	});

	$('.decrementar').click(function(){
		var id = $(this).attr('rel');
		$.get(ruta+'/compras/increaseProducto?id='+id+'&cantidad=-1', function(){
			window.location.href=ruta+"/compras/";
		});
	});
	$('.incrementar').click(function(){
		var id = $(this).attr('rel');
		$.get(ruta+'/compras/increaseProducto?id='+id+'&cantidad=1', function(){
			window.location.href=ruta+"/compras/";
		});
	});
	
	
	$('#chk_privacidad').click(function(){
		if ($('#error-politica').css('display') != 'none'){
			$('#error-politica').slideUp('fast',function(){$(this).remove();});
		}
	});
	
	$('#step3-content #chk_privacidad').click(function(){
		if ($('#error-politica').css('display') != 'none'){
			$('#error-politica').slideUp('fast',function(){$(this).remove();});
		}
	});
	
	//ocultar mensajes error al hacer focus.
	$('#datos-personales1').children(':input').each(function(){
		if ($('.error-compra').css('display') != 'none'){
			$(this).focus(function(){
				$('.error-compra').slideUp('fast', function(){ $(this).remove(); });
			});
		}
	});
	$('#datos-personales2').children(':input').each(function(){
		if ($('.error-compra').css('display') != 'none'){
			$(this).focus(function(){
				$('.error-compra').slideUp('fast', function(){ $(this).remove(); });
			});
		}
	});
	
	//procesar la solicitud
	$('#step5-content .boton_general').click(function(){
		var ubi = $('#step5-content');
		var btn_cerrar = $('<div></div>');	
		var c_url = document.URL;
		btn_cerrar.attr({'class':'cerrar right'});

		if ($('#metodo_pago1').is(':checked') || $('#metodo_pago2').is(':checked')){
			$('#formu-compra').submit();
		}else{
			var error_str = '';
			var content_msg = $('<div></div>');

			content_msg.attr({'id':'error-politica', 'class':'error-compra rounded-border-auto'});

			if (c_url.indexOf('/es/') != -1) error_str="Debe seleccionar un método de pago";
			if (c_url.indexOf('/en/') != -1) error_str="You must select a payment option";

			content_msg.append(error_str, btn_cerrar);
			if ($('#error-politica').length == 0){
				ubi.after(content_msg);
				content_msg.slideDown();
				btn_cerrar.click(function(){
					content_msg.slideUp('fast',function(){ $(this).remove(); });
				});
			}
			return false;		
		}
	});
	
	//limpiarInputs
	$('#reg_usuarios').find(':input').each(function(){
		var txt = $(this).val();
		$(this).focus(function(){$(this).val('');});
		$(this).blur(function(){if ($(this).val() == '') $(this).val(txt);});		
	});
}

function pasoSiguiente(n){
	var resta = n-1;
	var pasoAnterior = $('#step'+ resta + '-content');
	var pasoActual = $('#step'+ n + '-content');
	var pasoBarra = $('#step'+ n +' .number_process');
	
	//validacion para el paso 2.
	if ((n==3 && validarForm()) || (n != 3 && n != 4)){
		pasoAnterior.fadeOut('fast', function(){
			pasoActual.fadeIn('fast');
			pasoBarra.removeClass('proceso'+ n +'_inactivo').addClass('proceso'+ n +'_activo');
		});		
	}
	
	//validacion para el paso 3.
	if (n == 4 && validaUsuarios() || (n != 3 && n != 4)){
		pasoAnterior.fadeOut('fast', function(){
			pasoActual.fadeIn('fast');
			pasoBarra.removeClass('proceso'+ n +'_inactivo').addClass('proceso'+ n +'_activo');
		});				
	}
}

function inArray(needle, haystack) {
    var length = haystack.length;
    for(var i = 0; i < length; i++) {
        if(haystack[i] == needle) return true;
    }
    return false;
}

function validaUsuarios(){
	var ubi = $('#ubi').next();	
	var btn_cerrar = $('<div></div>');	
	var c_url = document.URL;
	var todos = $('#reg_usuarios').find(':input');	
	var array = new Array('Nombre','Apellidos','E-mail','Name','Surname');
	var valido = true;

	btn_cerrar.attr({'class':'cerrar right'});		
	
	if ($('#step3-content #chk_privacidad').is(':checked')){
		todos.each(function(){
			if (compruebaCampoVacio($(this).val()) || inArray($(this).val(),array)){
				marcame($(this));
				valido = false;
			}else{
				if ($(this).hasClass('email_alumno') && !validarEmail($(this).val())){
					var error_string = '';
					var c = $('<div></div>');
					c.attr({'class':'error-compra rounded-border-auto'});
										
					if (c_url.indexOf('/es/') != -1) error_string="La dirección de correo no es válida";
					if (c_url.indexOf('/en/') != -1) error_string="You must enter a valid email adress";
					
					if ($('.error-compra').length == 0){
						c.append(error_string, btn_cerrar);
						ubi.after(c);
						c.slideDown();
						btn_cerrar.click(function(){c.slideUp('fast',function(){$(this).remove(); })});
					}	
					
					marcame($(this));
					valido = false;
				}
			}
			
		});
	}else{
		//no ha aceptado política de privacidad.
		var error_str = '';
		var content_msg = $('<div></div>');
		
		content_msg.attr({'id':'error-politica', 'class':'error-compra rounded-border-auto'});
		
		if (c_url.indexOf('/es/') != -1) error_str="Debe leer y aceptar la política de privacidad y condiciones de uso.";
		if (c_url.indexOf('/en/') != -1) error_str="You must read terms and conditions.";
		
		content_msg.append(error_str, btn_cerrar);
		if ($('#error-politica').length == 0){
			ubi.after(content_msg);
			content_msg.slideDown();
			btn_cerrar.click(function(){
				content_msg.slideUp('fast',function(){ $(this).remove(); });
			});
		}
		return false;		
	}
	return valido;
}

function validarForm(){
	var c_url = document.URL;
	var formu = $('#formu-compra'); 
	var datos1 = $('#datos-personales1').children(':input');
	var datos2 = $('#datos-personales2').children(':input');
	var ubi = $('.formu-margen');	
	var btn_cerrar = $('<div></div>');
	
	var dataValid1 = true;
	var dataValid2 = true;
	
	var mail = $('#email');
	var conf_mail = $('#conf_email');
	
	btn_cerrar.attr({'class':'cerrar right'});
	
	if ($('#chk_privacidad').is(':checked')){

		datos1.each(function(){
			if (compruebaCampoVacio($(this).val())){
				//campo vacio
				marcame($(this));			
				dataValid1 = false;
			}else{
				//no esta vacio, compruebo email.
				if ($(this).attr('id') == 'email' && !validarEmail($(this).val())){
					var error_string = '';
					var c = $('<div></div>');
					c.attr({'class':'error-compra rounded-border-auto'});
										
					if (c_url.indexOf('/es/') != -1) error_string="La dirección de correo no es válida";
					if (c_url.indexOf('/en/') != -1) error_string="You must enter a valid email adress";
					
					
					if ($('.error-compra').length == 0){
						c.append(error_string, btn_cerrar);
						ubi.after(c);
						c.slideDown();
						btn_cerrar.click(function(){c.slideUp('fast',function(){$(this).remove(); })});
					}	
					marcame($(this));
					dataValid1 = false;
				}
			}
		});

	
		datos2.each(function(){
			if (compruebaCampoVacio($(this).val())){
				//campo vacio
				marcame($(this));
				dataValid2 = false;
			
			}
		});
	
		//comprobar confirmacion email una vez comprobados los errores anteriores.
		if (dataValid1 && dataValid2){
			if (mail.val() != conf_mail.val()){
				var error_string = '';
				var div_e = $('<div></div');
		
				div_e.attr({'id':'error_mails', 'class':'error-compra rounded-border-auto'});
		
				if (c_url.indexOf('/es/') != -1) error_string="Las direcciones de correo deben ser iguales.";
				if (c_url.indexOf('/en/') != -1) error_string="The email addresses must be the same.";
		
				if ($('.error-compra').length == 0){
					div_e.append(error_string, btn_cerrar);
					ubi.after(div_e);
					div_e.slideDown();
					btn_cerrar.click(function(){div_e.slideUp('fast', function(){ $(this).remove()});});		
				}
				marcame(mail);
				marcame(conf_mail);
				dataValid2=false;
			} 
		}
	}else{
		//no ha aceptado política de privacidad.
		var error_str = '';
		var content_msg = $('<div></div>');
		
		content_msg.attr({'id':'error-politica', 'class':'error-compra rounded-border-auto'});
		
		if (c_url.indexOf('/es/') != -1) error_str="Debe leer y aceptar la política de privacidad y condiciones de uso.";
		if (c_url.indexOf('/en/') != -1) error_str="You must read terms and conditions.";
		
		content_msg.append(error_str, btn_cerrar);
		if ($('#error-politica').length == 0){
			ubi.after(content_msg);
			content_msg.slideDown();
			btn_cerrar.click(function(){
				content_msg.slideUp('fast',function(){ $(this).remove(); });
			});
		}
		return false;
	}
	
	if (dataValid1 && dataValid2) return true;
	else return false;
}

function marcame(e){
	e.addClass("marcarInput").delay(4000).queue(function(next){
	    $(this).removeClass("marcarInput");
	    next();
	});
}

function pasoAnterior(n){
	var suma = n+1;
	var pasoActual = $('#step'+ suma + '-content');
	var pasoAnterior = $('#step'+ n + '-content');
	var pasoBarra = $('#step'+ suma +' .number_process');
	
	pasoActual.fadeOut('fast', function(){
		pasoAnterior.fadeIn('fast');
		pasoBarra.removeClass('proceso'+ suma +'_activo').addClass('proceso'+ suma +'_inactivo');
	});	
}

// $(document).ready(function(){
// });
