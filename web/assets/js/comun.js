$(document).ready(function(){
	
	//acciones botones redes.
	$('#red_facebook').click(function(){ window.open('http://www.facebook.com/spanischool') });
	$('#red_linkedin').click(function(){ window.open('http://www.linkedin.com/groups?gid=2760474&trk=myg_ugrp_ovr') });
	$('#red_twitter').click(function(){ window.open('http://twitter.com/#!/spanischool') });
	
   revisarItemsCarrito();

   $('.currency_selector').change(function(){
        $.get(ruta+"/compras/changeCurrency?currency="+$('.currency_selector').val(), function(){
            window.location.reload();
        });
    });	
	var cad_url = document.URL;
	//comportamiento botonera home inferior redes sociales.
	//emulación i.e. para el boton g+
	if ($.browser.msie){
		var selector = $('#red_gplus .plus');
		selector.hover(
		function(){
			$(this).css('top', '0px');
		},
		function(){
			$(this).css('top', '6px');
		}
		);
	}
	//fancybox
	$("#link_carrito").fancybox();
	$("#link_bulats").fancybox();
	$("#link_bulats2").fancybox();
		
	$("#politica").fancybox({
		'width':770,
		'height':500		
	});
	$("#metodo_fl").fancybox({
		'width':830,
		'height':495		
	});
	$('#notalegal_footer').fancybox({
		'width':770,
		'height':500
	});
	$('#notalegal_footer2').fancybox({
		'width':770,
		'height':500
	});
	$('.iframe').fancybox({
		'width':780,
		'height':520
	});	
	
	//fancybox
	$(".demos-fancy").click(function() {
		$.fancybox({
			'padding'		: 0,
			'autoScale'		: false,
			'transitionIn'	: 'none',
			'transitionOut'	: 'none',
			'title'			: this.title,
			'width'			: 780,
			'height'		: 488,
			'href'			: this.href.replace(new RegExp("([0-9])","i"),'moogaloop.swf?clip_id=$1'),
			'type'			: 'swf'
		});

		return false;
	});	
		
	//modificar clase botones menu.
	var contentMenu = $('#botoneraItems .items_menu');
	
	contentMenu.children().each(function(){
		var link = $(this).children().first();
		var txtOriginal = link.html();
		
		$(this).hover(function(){
			$(this).addClass('item_menu_pulsado');
		},
		function(){
			$(this).removeClass('item_menu_pulsado');
		}
		);
		
		if (link.attr('href') == cad_url){
			$(this).addClass('item_menu_pulsado');
			// link.remove();
			//$(this).html(txtOriginal);
			$(this).hover(
				function(){
					$(this).addClass('item_menu_pulsado');
				},
				function(){
					$(this).addClass('item_menu_pulsado');
				}
			);
		} 
		else{
			if ($(this).hasClass('item_menu_pulsado')) $(this).removeClass('item_menu_pulsado');
		}
	});	
	
	//comportamiento inputs envío formulario
	var solicitaCupon = $('#sol_cupon');
	var solicitaInfo = $('#sol_info');
	var accesoDemo = $('#campus_send');
	var discountCode = $('#codigo_descuento');
	//, $('.nombre_alumno'), $('.apellidos_alumno'), $('.email_alumno')
	var valores = new Array(solicitaCupon, accesoDemo, discountCode, solicitaInfo);
	
	limpiaInputs(valores);
	
	//envío form campus.
	$('#fset_login .boton_general').click(function(){ $('#formu_CampusLogin').submit(); });
	
	//inits
	if (cad_url.indexOf('home') != -1) initHome();
	if (cad_url.indexOf('contacto') != -1)	initContacto();	
	if (cad_url.indexOf('cursos_online') != -1 && cad_url.indexOf('ficha') != -1) initFicha();
	if (cad_url.indexOf('compras') != -1) initCompras();
	if (cad_url.indexOf('cursos_presenciales') != -1 && cad_url.indexOf('ficha') == -1) initPresenciales();
	if (cad_url.indexOf('cursos_presenciales') != -1 && cad_url.indexOf('ficha') != -1) initFichaPresenciales();	
});

function limpiaInputs(array){
	for (var x in array){
		var obj = array[x];
		var valor = obj.val();
		
		if (valor != undefined){
			var txt = valor;
			obj.focus(function(){$(this).val('');});
			obj.blur(function(){if ($(this).val() == '') $(this).val(txt);});
		}
	}
}

function cargaItems(){
	var destino = $('#box_fancybox');
	destino.html('prueba');
}

function validarEmail(valor) {
	if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(valor)){
		return true;
	} 
	else {
		return false;
		 }
}

function compruebaCampoVacio(campo){
	if (campo == '' || campo.split(' ').length == campo.length+1){
		return true;
	}else{
		return false;
	}
}

function incrementar(e){
	var obj = $(e);
	var caja = obj.prev();
	var currentItems = parseInt(caja.attr('rel'));
	var nuevoValor = currentItems+1;
	
	caja.html(nuevoValor);
	caja.attr('rel', nuevoValor);
}

function decrementar(e){
		var obj = $(e);
		var caja = obj.next();
		var currentItems = parseInt(caja.attr('rel'));
		var nuevoValor = currentItems-1;

		if (nuevoValor > 0){
			caja.html(nuevoValor);
			caja.attr('rel', nuevoValor);
		}
}	

function envioMail(e){
	var target = $(e);
	var input  = target.prev().prev();
	var error  = target.prev();
	var txt    = input.val();
	var tipo   = input.attr('name');

	if (!validarEmail(txt)){
		if (tipo == 'campus_send') error.css('margin-top', '15px');
		target.fadeOut('fast', function(){
			error.fadeIn().delay(2000).fadeOut('fast', function(){
				target.fadeIn();
			});
		});

	}else{
		var itemCarga = $('<div></div');
		itemCarga.attr({'id':'cargando'});
		itemCarga.html('<img src="'+basepath+'/assets/img/comun/loading.gif"/>');
		
		target.fadeOut('fast', function(){
			$(this).before(itemCarga);		
		});
		
		$.post(ruta+'/correo/cuponDescuento',{mail: txt, tipo:tipo}, function(data){
			var feedback = data.split('##');
			if (data.indexOf('exito') != -1){
					itemCarga.fadeOut('fast', function(){
						var nItem = $('<div></div');
						
						nItem.attr({'id':'feedback-cupon'});
						nItem.text(feedback[1]);
						itemCarga.remove();
						target.before(nItem);
						nItem.delay(2000).fadeOut('fast', function(){
							target.fadeIn();
							nItem.remove();
							$(this).remove();
						});
				 });
			}else{
				if (tipo == 'campus_send') error.css('margin-top', '15px');
				itemCarga.fadeOut('fast', function(){
					error.html(feedback[1]);
					//target.remove();
					$(this).remove();
					error.fadeIn().delay(2000).fadeOut('fast', function(){
						target.fadeIn();
					});
				});
				
			}
		});
	}
}

function revisarItemsCarrito(){
    $.post(ruta+'/compras/count', function(data){
        $('#n_items_carrito').html(data);
    }, 'text');
}
function visita_url(url){
	location.href = url;
}