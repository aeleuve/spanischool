function initPresenciales(){
	$('.iframe').fancybox({
		'width':480,
		'height':385
	});	
	
	$('.slideshow').cycle({
	        fx: 'fade',
	        timeout: 3000,
	        random: 0,
	        pause: 1,
	        after:  onAfter,
	        before: onBefore
	});

    btnPulsado="";
    
    jQuery("#base_buttons").children().each(function() {
            $(this).click(function(){
                    if(btnPulsado!=""){
                            $(btnPulsado).attr("pulsado",false);
                            $(btnPulsado).attr("src","/assets/img/home/b_valla_blanco.png");
                    }
                    btnPulsado=this;
                    $(this).attr("pulsado",true);
                    $(this).attr("src","/assets/img/home/b_valla_azul.png");
                    arrayTemp=this.name.split("_");
                    numImgMostrar=arrayTemp[arrayTemp.length-1];
                    $('.slideshow').cycle(parseInt(numImgMostrar));
                    $('.slideshow').cycle('pause');
                    return false;
            });

            $(this).hover(function() {
                    valor=$(this).attr("pulsado");
                    if(valor=="false"){
                            $(this).attr("src",baseURL+"/assets/img/common/bot_rol.png");
                    }
            });

            $(this).mouseout(function() {
                    valor=$(this).attr("pulsado");
                    if(valor=="false"){
                            $(this).attr("src",baseURL+"/assets/img/common/bot_up.png");
                    }
            });
    });	
}

function onBefore(curr,next,opts) {
    elementoActual="bot_valla_"+opts.currSlide;
    //MARCAMOS LA IMAGEN DEL ELEMENTO SELECCIONADO
    jQuery(elementoActual).each(function(){
            $(this).attr("src","/assets/img/home/b_valla_azul.png");
            $(this).attr("pulsado",true);
    });
}

function onAfter(curr,next,opts) {
        elementoActual="bot_valla_"+opts.currSlide;
        //MarcarElemento(elementoActual);
        jQuery("#base_buttons").children().each(function(){
                elementoSacado=$(this).attr('name');
                if(elementoSacado==elementoActual){
                        $(this).attr("pulsado",true);
                        $(this).attr("src","/assets/img/home/b_valla_azul.png"); 
                }else{
                        $(this).attr("pulsado",false);
                        $(this).attr("src", "/assets/img/home/b_valla_blanco.png");
                }
        });
}


function show_demo(destino){

	
}
$(document).ready(function(){
    $('.btn_carrito').click(function(){
        $('#listaCursos tbody tr').each(function(){
            if($(this).find('input:checked').length==1){
                $.ajax({
                    url : ruta+"/compras/addProducto?id="+$(this).find('input').val()+"&cantidad="+$(this).find('.numItems').attr('rel'),
                    async: false,
                    success: function(){
                        revisarItemsCarrito();
                    }
                });
            }
        });
        window.location.href=ruta+"/compras/";
    });
});
