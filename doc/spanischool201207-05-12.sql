# ************************************************************
# Sequel Pro SQL dump
# Version 3408
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Host: webfr (MySQL 5.0.51a-24+lenny5-log)
# Database: spanischool2012
# Generation Time: 2012-05-07 13:26:34 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table carteles
# ------------------------------------------------------------

DROP TABLE IF EXISTS `carteles`;

CREATE TABLE `carteles` (
  `id` int(11) NOT NULL auto_increment,
  `fichero` varchar(100) collate utf8_unicode_ci NOT NULL,
  `activo` tinyint(1) NOT NULL,
  `lang` varchar(2) collate utf8_unicode_ci NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `carteles` WRITE;
/*!40000 ALTER TABLE `carteles` DISABLE KEYS */;

INSERT INTO `carteles` (`id`, `fichero`, `activo`, `lang`)
VALUES
	(4,'1336380769.26.jpg',1,'es'),
	(5,'1335260814.9751.jpg',1,'es'),
	(6,'1335260837.3346.jpg',1,'es'),
	(8,'1335260862.9503.jpg',1,'es'),
	(9,'1335260895.05.jpg',1,'en'),
	(10,'1335260912.0097.jpg',1,'en'),
	(11,'1335260925.4727.jpg',1,'en');

/*!40000 ALTER TABLE `carteles` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table carteles_presenciales
# ------------------------------------------------------------

DROP TABLE IF EXISTS `carteles_presenciales`;

CREATE TABLE `carteles_presenciales` (
  `id` int(11) NOT NULL auto_increment,
  `fichero` varchar(100) collate utf8_unicode_ci NOT NULL,
  `activo` tinyint(1) NOT NULL,
  `lang` varchar(2) collate utf8_unicode_ci NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `carteles_presenciales` WRITE;
/*!40000 ALTER TABLE `carteles_presenciales` DISABLE KEYS */;

INSERT INTO `carteles_presenciales` (`id`, `fichero`, `activo`, `lang`)
VALUES
	(1,'1335261018.4711.jpg',1,'es'),
	(2,'1335261030.9676.jpg',1,'es'),
	(3,'1335261044.4398.jpg',1,'es'),
	(4,'1335261061.5143.jpg',1,'es'),
	(5,'1335261081.1698.jpg',1,'es'),
	(6,'1335261093.2026.jpg',1,'es'),
	(7,'1335261104.2494.jpg',1,'es'),
	(8,'1335261117.0971.jpg',1,'es'),
	(9,'1335261128.8018.jpg',1,'es'),
	(10,'1335261150.659.jpg',1,'en'),
	(11,'1335261182.4944.jpg',1,'en'),
	(12,'1335261199.1179.jpg',1,'en'),
	(13,'1335261290.7671.jpg',1,'en'),
	(14,'1335261313.4552.jpg',1,'en'),
	(15,'1335261428.4445.jpg',1,'en'),
	(16,'1335261457.1025.jpg',1,'en');

/*!40000 ALTER TABLE `carteles_presenciales` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table codigos_promocionales
# ------------------------------------------------------------

DROP TABLE IF EXISTS `codigos_promocionales`;

CREATE TABLE `codigos_promocionales` (
  `id` int(11) NOT NULL auto_increment,
  `codigo` varchar(50) collate utf8_unicode_ci NOT NULL,
  `descripcion` varchar(50) collate utf8_unicode_ci NOT NULL,
  `precio_eur` decimal(10,2) default NULL,
  `precio_usd` decimal(10,2) default NULL,
  `precio_rbr` decimal(10,2) default NULL,
  `precio_gbp` decimal(10,2) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `codigos_promocionales` WRITE;
/*!40000 ALTER TABLE `codigos_promocionales` DISABLE KEYS */;

INSERT INTO `codigos_promocionales` (`id`, `codigo`, `descripcion`, `precio_eur`, `precio_usd`, `precio_rbr`, `precio_gbp`)
VALUES
	(11,'PALMA','Código de pruebas',1.00,1.00,1.00,1.00);

/*!40000 ALTER TABLE `codigos_promocionales` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table comunes_presenciales
# ------------------------------------------------------------

DROP TABLE IF EXISTS `comunes_presenciales`;

CREATE TABLE `comunes_presenciales` (
  `id` int(11) NOT NULL auto_increment,
  `nombre` varchar(300) collate utf8_unicode_ci NOT NULL,
  `precio_eur` decimal(10,2) default NULL,
  `precio_usd` decimal(10,2) default NULL,
  `precio_rbr` decimal(10,2) default NULL,
  `precio_gbp` decimal(10,2) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `comunes_presenciales` WRITE;
/*!40000 ALTER TABLE `comunes_presenciales` DISABLE KEYS */;

INSERT INTO `comunes_presenciales` (`id`, `nombre`, `precio_eur`, `precio_usd`, `precio_rbr`, `precio_gbp`)
VALUES
	(2,'seguro',0.00,0.00,0.00,0.00),
	(3,'matricula',0.00,0.00,0.00,0.00),
	(4,'desplazamiento',140.00,140.00,140.00,140.00);

/*!40000 ALTER TABLE `comunes_presenciales` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table cursos_presenciales
# ------------------------------------------------------------

DROP TABLE IF EXISTS `cursos_presenciales`;

CREATE TABLE `cursos_presenciales` (
  `id` int(11) NOT NULL auto_increment,
  `nombre` varchar(300) collate utf8_unicode_ci NOT NULL,
  `nivel` varchar(10) collate utf8_unicode_ci NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `cursos_presenciales` WRITE;
/*!40000 ALTER TABLE `cursos_presenciales` DISABLE KEYS */;

INSERT INTO `cursos_presenciales` (`id`, `nombre`, `nivel`)
VALUES
	(2,'espanolintenso','A1-C2'),
	(3,'espanolindustriaturismo','A2-C2'),
	(4,'campoverano','A1-C2'),
	(5,'espanolgastronomia','A1-B1'),
	(6,'espanolgolf','A1-B1');

/*!40000 ALTER TABLE `cursos_presenciales` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table cursos_presenciales_detalles
# ------------------------------------------------------------

DROP TABLE IF EXISTS `cursos_presenciales_detalles`;

CREATE TABLE `cursos_presenciales_detalles` (
  `id` int(11) NOT NULL auto_increment,
  `semanas` varchar(20) collate utf8_unicode_ci NOT NULL,
  `horas` smallint(6) NOT NULL,
  `residencia` varchar(8) collate utf8_unicode_ci NOT NULL,
  `precio_eur` decimal(10,2) default NULL,
  `precio_usd` decimal(10,2) default NULL,
  `precio_rbr` decimal(10,2) default NULL,
  `precio_gbp` decimal(10,2) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `cursos_presenciales_detalles` WRITE;
/*!40000 ALTER TABLE `cursos_presenciales_detalles` DISABLE KEYS */;

INSERT INTO `cursos_presenciales_detalles` (`id`, `semanas`, `horas`, `residencia`, `precio_eur`, `precio_usd`, `precio_rbr`, `precio_gbp`)
VALUES
	(1,'1',20,'opcional',170.00,NULL,NULL,NULL),
	(2,'sasas',33,'opcional',56.00,NULL,NULL,NULL),
	(3,'2',40,'opcional',295.00,NULL,NULL,NULL),
	(4,'2',60,'opcional',500.00,659.00,1241.00,407.00),
	(5,'2',20,'incluida',880.00,1159.00,2184.00,716.00),
	(6,'2',25,'incluida',940.00,1238.00,2333.00,765.00),
	(7,'2',30,'incluida',1058.00,1393.00,2626.00,861.00),
	(8,'4',100,'incluida',1.35,1.77,3.41,1.10),
	(9,'4',100,'incluida',1350.00,1773.00,3407.00,1096.00),
	(10,'3',90,'opcional',750.00,988.00,1862.00,611.00),
	(11,'4',120,'opcional',950.00,1251.00,2358.00,733.00),
	(12,'1 Extra',30,'opcional',220.00,329.00,546.00,179.00),
	(13,'3',60,'opcional',400.00,NULL,NULL,NULL),
	(14,'4',80,'opcional',490.00,NULL,NULL,NULL),
	(15,'1 Extra',20,'opcional',130.00,NULL,NULL,NULL);

/*!40000 ALTER TABLE `cursos_presenciales_detalles` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table paquetes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `paquetes`;

CREATE TABLE `paquetes` (
  `id` int(11) NOT NULL auto_increment,
  `descripcion` varchar(60) collate utf8_unicode_ci NOT NULL,
  `precio_eur` decimal(10,2) default NULL,
  `precio_usd` decimal(10,2) default NULL,
  `precio_rbr` decimal(10,2) default NULL,
  `precio_gbp` decimal(10,2) default NULL,
  `ahorro` varchar(20) collate utf8_unicode_ci default NULL,
  `activo` tinyint(1) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `paquetes` WRITE;
/*!40000 ALTER TABLE `paquetes` DISABLE KEYS */;

INSERT INTO `paquetes` (`id`, `descripcion`, `precio_eur`, `precio_usd`, `precio_rbr`, `precio_gbp`, `ahorro`, `activo`)
VALUES
	(11,'cursoelemental1',20.00,30.00,300.00,10.00,NULL,0),
	(12,'cursoelemental1tutor',100.00,100.00,100.00,100.00,NULL,0),
	(13,'cursoelemental2',100.00,100.00,100.00,100.00,NULL,0),
	(14,'cursoelemental2tutor',100.00,100.00,100.00,100.00,NULL,0),
	(15,'cursopreintermedio1',110.00,110.00,110.00,110.00,NULL,0),
	(16,'cursopreintermedio1tutor',112.00,112.00,112.00,112.00,NULL,0),
	(17,'cursopreintermedio2',100.00,100.00,100.00,100.00,NULL,0),
	(18,'cursopreintermedio2tutor',100.00,112.00,112.00,100.50,NULL,0),
	(19,'One-to-one',20.00,20.00,20.00,20.00,NULL,0),
	(20,'One-to-one Pack 5 sesiones',50.00,50.00,50.00,50.00,NULL,0),
	(21,'One-to-one Pack 10 sesiones',100.00,100.00,100.00,100.00,NULL,0),
	(22,'one-to-one20',100.00,20.00,20.00,20.00,NULL,0),
	(23,'examenbulats',50.00,66.00,126.00,41.00,NULL,0);

/*!40000 ALTER TABLE `paquetes` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table productos
# ------------------------------------------------------------

DROP TABLE IF EXISTS `productos`;

CREATE TABLE `productos` (
  `id` int(11) NOT NULL auto_increment,
  `nombre` varchar(300) collate utf8_unicode_ci NOT NULL,
  `codigo_plataforma` varchar(50) collate utf8_unicode_ci default NULL,
  `nivel` varchar(10) collate utf8_unicode_ci default NULL,
  `horas` decimal(5,2) default NULL,
  `dias_acceso` smallint(6) default NULL,
  `precio_eur` decimal(10,2) default NULL,
  `precio_usd` decimal(10,2) default NULL,
  `precio_rbr` decimal(10,2) default NULL,
  `paquete_id` int(11) default NULL,
  `precio_gbp` decimal(10,2) default NULL,
  `tipo` varchar(8) collate utf8_unicode_ci NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `UNIQ_767490E641E2D57E` (`paquete_id`),
  CONSTRAINT `FK_767490E641E2D57E` FOREIGN KEY (`paquete_id`) REFERENCES `paquetes` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `productos` WRITE;
/*!40000 ALTER TABLE `productos` DISABLE KEYS */;

INSERT INTO `productos` (`id`, `nombre`, `codigo_plataforma`, `nivel`, `horas`, `dias_acceso`, `precio_eur`, `precio_usd`, `precio_rbr`, `paquete_id`, `precio_gbp`, `tipo`)
VALUES
	(15,'cursoelemental1','10418','A1.1',80.00,3,150.00,198.00,372.00,11,122.00,'curso'),
	(16,'cursoelemental1tutor','10418','A1.1',80.00,90,310.00,408.00,769.00,12,252.00,'curso'),
	(17,'cursoelemental2','10487','A1.2',80.00,90,150.00,198.00,372.00,13,122.00,'curso'),
	(18,'cursoelemental2tutor','10487','A1.2',80.00,90,310.00,408.00,769.00,14,252.00,'curso'),
	(19,'cursopreintermedio1','10488','A2.1',80.00,90,150.00,198.00,372.00,15,122.00,'curso'),
	(20,'cursopreintermedio1tutor','10488','A2.1',80.00,90,310.00,408.00,769.00,16,252.00,'curso'),
	(21,'cursopreintermedio2','10489','A2.2',80.00,90,150.00,198.00,372.00,17,122.00,'curso'),
	(22,'cursopreintermedio2tutor','10489','A2.2',80.00,90,310.00,408.00,769.00,18,252.00,'curso'),
	(23,'onetoone',NULL,'A1-C2',0.50,1,20.00,26.00,50.00,19,16.00,'oneToOne'),
	(24,'one-to-one5',NULL,'A1-C2',2.50,5,90.00,119.00,223.00,20,73.00,'oneToOne'),
	(25,'one-to-one10',NULL,'A1-C2',5.00,10,160.00,211.00,397.00,21,122.00,'oneToOne'),
	(26,'one-to-one20',NULL,'A1-C2',10.00,20,280.00,369.00,695.00,22,228.00,'oneToOne'),
	(27,'examenbulats',NULL,'A2-C2',1.00,NULL,50.00,66.00,126.00,23,41.00,'examen');

/*!40000 ALTER TABLE `productos` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table r_cursopresencial_cursopresencialdetalle
# ------------------------------------------------------------

DROP TABLE IF EXISTS `r_cursopresencial_cursopresencialdetalle`;

CREATE TABLE `r_cursopresencial_cursopresencialdetalle` (
  `curso_presencial_id` int(11) NOT NULL,
  `curso_presencial_detalle_id` int(11) NOT NULL,
  PRIMARY KEY  (`curso_presencial_id`,`curso_presencial_detalle_id`),
  KEY `IDX_E7AEEE095AA593FD` (`curso_presencial_id`),
  KEY `IDX_E7AEEE09B54CECD2` (`curso_presencial_detalle_id`),
  CONSTRAINT `FK_E7AEEE09B54CECD2` FOREIGN KEY (`curso_presencial_detalle_id`) REFERENCES `cursos_presenciales_detalles` (`id`),
  CONSTRAINT `FK_E7AEEE095AA593FD` FOREIGN KEY (`curso_presencial_id`) REFERENCES `cursos_presenciales` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `r_cursopresencial_cursopresencialdetalle` WRITE;
/*!40000 ALTER TABLE `r_cursopresencial_cursopresencialdetalle` DISABLE KEYS */;

INSERT INTO `r_cursopresencial_cursopresencialdetalle` (`curso_presencial_id`, `curso_presencial_detalle_id`)
VALUES
	(2,1),
	(2,3),
	(2,13),
	(2,14),
	(2,15),
	(3,4),
	(3,10),
	(3,11),
	(3,12),
	(4,5),
	(4,6),
	(4,7),
	(5,8),
	(6,9);

/*!40000 ALTER TABLE `r_cursopresencial_cursopresencialdetalle` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table r_paquetes_productos
# ------------------------------------------------------------

DROP TABLE IF EXISTS `r_paquetes_productos`;

CREATE TABLE `r_paquetes_productos` (
  `paquete_id` int(11) NOT NULL,
  `producto_id` int(11) NOT NULL,
  PRIMARY KEY  (`paquete_id`,`producto_id`),
  KEY `IDX_1392384E41E2D57E` (`paquete_id`),
  KEY `IDX_1392384E7645698E` (`producto_id`),
  CONSTRAINT `FK_1392384E7645698E` FOREIGN KEY (`producto_id`) REFERENCES `productos` (`id`),
  CONSTRAINT `FK_1392384E41E2D57E` FOREIGN KEY (`paquete_id`) REFERENCES `paquetes` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `r_paquetes_productos` WRITE;
/*!40000 ALTER TABLE `r_paquetes_productos` DISABLE KEYS */;

INSERT INTO `r_paquetes_productos` (`paquete_id`, `producto_id`)
VALUES
	(11,15),
	(12,16),
	(13,17),
	(14,18),
	(15,19),
	(16,20),
	(17,21),
	(18,22),
	(19,23),
	(20,24),
	(21,25),
	(22,26),
	(23,27);

/*!40000 ALTER TABLE `r_paquetes_productos` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table r_producto_codigopromocional
# ------------------------------------------------------------

DROP TABLE IF EXISTS `r_producto_codigopromocional`;

CREATE TABLE `r_producto_codigopromocional` (
  `producto_id` int(11) NOT NULL,
  `codigo_promocional_id` int(11) NOT NULL,
  PRIMARY KEY  (`producto_id`,`codigo_promocional_id`),
  KEY `IDX_AC115E647645698E` (`producto_id`),
  KEY `IDX_AC115E64D10C5DCB` (`codigo_promocional_id`),
  CONSTRAINT `FK_AC115E64D10C5DCB` FOREIGN KEY (`codigo_promocional_id`) REFERENCES `codigos_promocionales` (`id`),
  CONSTRAINT `FK_AC115E647645698E` FOREIGN KEY (`producto_id`) REFERENCES `productos` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `r_producto_codigopromocional` WRITE;
/*!40000 ALTER TABLE `r_producto_codigopromocional` DISABLE KEYS */;

INSERT INTO `r_producto_codigopromocional` (`producto_id`, `codigo_promocional_id`)
VALUES
	(15,11);

/*!40000 ALTER TABLE `r_producto_codigopromocional` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
