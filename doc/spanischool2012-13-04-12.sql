-- phpMyAdmin SQL Dump
-- version 2.11.8.1deb5+lenny9
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 13-04-2012 a las 14:17:44
-- Versión del servidor: 5.0.51
-- Versión de PHP: 5.2.6-1+lenny16

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `spanischool2012`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `carteles`
--

CREATE TABLE IF NOT EXISTS `carteles` (
  `id` int(11) NOT NULL auto_increment,
  `fichero` varchar(100) collate utf8_unicode_ci NOT NULL,
  `activo` tinyint(1) NOT NULL,
  `lang` varchar(2) collate utf8_unicode_ci NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=13 ;

--
-- Volcar la base de datos para la tabla `carteles`
--

INSERT INTO `carteles` (`id`, `fichero`, `activo`, `lang`) VALUES
(4, '1332945555.985.jpg', 0, 'es'),
(5, '1334150497.9269.jpg', 1, 'es'),
(6, '1334150527.6491.jpg', 1, 'es'),
(7, '1334153993.4861.jpg', 1, 'en'),
(8, '1334159017.8062.jpg', 1, 'es'),
(9, '1334159035.4124.jpg', 1, 'en'),
(10, '1334159055.5233.jpg', 1, 'en'),
(11, '1334159069.2599.jpg', 1, 'en'),
(12, '1334244495.7213.jpg', 1, 'es');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `carteles_presenciales`
--

CREATE TABLE IF NOT EXISTS `carteles_presenciales` (
  `id` int(11) NOT NULL auto_increment,
  `fichero` varchar(100) collate utf8_unicode_ci NOT NULL,
  `activo` tinyint(1) NOT NULL,
  `lang` varchar(2) collate utf8_unicode_ci NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=10 ;

--
-- Volcar la base de datos para la tabla `carteles_presenciales`
--

INSERT INTO `carteles_presenciales` (`id`, `fichero`, `activo`, `lang`) VALUES
(1, '1334158758.5409.jpg', 1, 'es'),
(2, '1334158777.0428.jpg', 1, 'es'),
(3, '1334158790.4847.jpg', 1, 'es'),
(4, '1334158804.7388.jpg', 1, 'es'),
(5, '1334158818.4625.jpg', 1, 'es'),
(6, '1334158829.6444.jpg', 1, 'es'),
(7, '1334158843.3144.jpg', 1, 'es'),
(8, '1334158856.7606.jpg', 1, 'es'),
(9, '1334158870.3871.jpg', 1, 'es');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `codigos_promocionales`
--

CREATE TABLE IF NOT EXISTS `codigos_promocionales` (
  `id` int(11) NOT NULL auto_increment,
  `codigo` varchar(50) collate utf8_unicode_ci NOT NULL,
  `descripcion` varchar(50) collate utf8_unicode_ci NOT NULL,
  `precio_eur` decimal(10,2) default NULL,
  `precio_usd` decimal(10,2) default NULL,
  `precio_rbr` decimal(10,2) default NULL,
  `precio_gbp` decimal(10,2) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=11 ;

--
-- Volcar la base de datos para la tabla `codigos_promocionales`
--


-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comunes_presenciales`
--

CREATE TABLE IF NOT EXISTS `comunes_presenciales` (
  `id` int(11) NOT NULL auto_increment,
  `nombre` varchar(300) collate utf8_unicode_ci NOT NULL,
  `precio_eur` decimal(10,2) default NULL,
  `precio_usd` decimal(10,2) default NULL,
  `precio_rbr` decimal(10,2) default NULL,
  `precio_gbp` decimal(10,2) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Volcar la base de datos para la tabla `comunes_presenciales`
--

INSERT INTO `comunes_presenciales` (`id`, `nombre`, `precio_eur`, `precio_usd`, `precio_rbr`, `precio_gbp`) VALUES
(2, 'ssss', 33.00, NULL, NULL, NULL),
(3, 'Corte y confección', NULL, NULL, 19.20, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cursos_presenciales`
--

CREATE TABLE IF NOT EXISTS `cursos_presenciales` (
  `id` int(11) NOT NULL auto_increment,
  `nombre` varchar(300) collate utf8_unicode_ci NOT NULL,
  `nivel` varchar(10) collate utf8_unicode_ci NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Volcar la base de datos para la tabla `cursos_presenciales`
--

INSERT INTO `cursos_presenciales` (`id`, `nombre`, `nivel`) VALUES
(2, 'ssssa', 'dds sa'),
(3, 'bbbbbbbbbb', 'bb');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cursos_presenciales_detalles`
--

CREATE TABLE IF NOT EXISTS `cursos_presenciales_detalles` (
  `id` int(11) NOT NULL auto_increment,
  `semanas` varchar(20) collate utf8_unicode_ci NOT NULL,
  `horas` smallint(6) NOT NULL,
  `residencia` varchar(8) collate utf8_unicode_ci NOT NULL,
  `precio_eur` decimal(10,2) default NULL,
  `precio_usd` decimal(10,2) default NULL,
  `precio_rbr` decimal(10,2) default NULL,
  `precio_gbp` decimal(10,2) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Volcar la base de datos para la tabla `cursos_presenciales_detalles`
--

INSERT INTO `cursos_presenciales_detalles` (`id`, `semanas`, `horas`, `residencia`, `precio_eur`, `precio_usd`, `precio_rbr`, `precio_gbp`) VALUES
(1, '111', 33, 'opcional', 3.00, NULL, NULL, NULL),
(2, 'sasas', 33, 'opcional', 56.00, NULL, NULL, NULL),
(3, 'qqq', 111, 'opcional', 1.00, 2.00, 3.00, 4.00);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `paquetes`
--

CREATE TABLE IF NOT EXISTS `paquetes` (
  `id` int(11) NOT NULL auto_increment,
  `descripcion` varchar(60) collate utf8_unicode_ci NOT NULL,
  `precio_eur` decimal(10,2) default NULL,
  `precio_usd` decimal(10,2) default NULL,
  `precio_rbr` decimal(10,2) default NULL,
  `precio_gbp` decimal(10,2) default NULL,
  `ahorro` varchar(20) collate utf8_unicode_ci default NULL,
  `activo` tinyint(1) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=22 ;

--
-- Volcar la base de datos para la tabla `paquetes`
--

INSERT INTO `paquetes` (`id`, `descripcion`, `precio_eur`, `precio_usd`, `precio_rbr`, `precio_gbp`, `ahorro`, `activo`) VALUES
(11, 'cursoelemental1', 20.00, 30.00, 300.00, 10.00, NULL, 0),
(12, 'cursoelemental1tutor', 100.00, 100.00, 100.00, 100.00, NULL, 0),
(13, 'cursoelemental2', 100.00, 100.00, 100.00, 100.00, NULL, 0),
(14, 'cursoelemental2tutor', 100.00, 100.00, 100.00, 100.00, NULL, 0),
(15, 'cursopreintermedio1', 110.00, 110.00, 110.00, 110.00, NULL, 0),
(16, 'cursopreintermedio1tutor', 112.00, 112.00, 112.00, 112.00, NULL, 0),
(17, 'cursopreintermedio2', 100.00, 100.00, 100.00, 100.00, NULL, 0),
(18, 'cursopreintermedio2tutor', 100.00, 112.00, 112.00, 100.50, NULL, 0),
(19, 'One-to-one', 20.00, 20.00, 20.00, 20.00, NULL, 0),
(20, 'One-to-one Pack 5 sesiones', 50.00, 50.00, 50.00, 50.00, NULL, 0),
(21, 'One-to-one Pack 10 sesiones', 100.00, 100.00, 100.00, 100.00, NULL, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos`
--

CREATE TABLE IF NOT EXISTS `productos` (
  `id` int(11) NOT NULL auto_increment,
  `nombre` varchar(300) collate utf8_unicode_ci NOT NULL,
  `codigo_plataforma` varchar(50) collate utf8_unicode_ci default NULL,
  `nivel` varchar(10) collate utf8_unicode_ci default NULL,
  `horas` decimal(5,2) default NULL,
  `dias_acceso` smallint(6) default NULL,
  `precio_eur` decimal(10,2) default NULL,
  `precio_usd` decimal(10,2) default NULL,
  `precio_rbr` decimal(10,2) default NULL,
  `paquete_id` int(11) default NULL,
  `precio_gbp` decimal(10,2) default NULL,
  `tipo` varchar(8) collate utf8_unicode_ci NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `UNIQ_767490E641E2D57E` (`paquete_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=26 ;

--
-- Volcar la base de datos para la tabla `productos`
--

INSERT INTO `productos` (`id`, `nombre`, `codigo_plataforma`, `nivel`, `horas`, `dias_acceso`, `precio_eur`, `precio_usd`, `precio_rbr`, `paquete_id`, `precio_gbp`, `tipo`) VALUES
(15, 'cursoelemental1', NULL, 'A1.1', 75.00, 60, 20.00, 30.00, 300.00, 11, 10.00, 'curso'),
(16, 'cursoelemental1tutor', NULL, 'A1.1', 75.00, 60, 100.00, 100.00, 100.00, 12, 100.00, 'curso'),
(17, 'cursoelemental2', NULL, 'A1.2', 75.00, 60, 100.00, 100.00, 100.00, 13, 100.00, 'curso'),
(18, 'cursoelemental2tutor', NULL, 'A1.2', 75.00, 60, 100.00, 100.00, 100.00, 14, 100.00, 'curso'),
(19, 'cursopreintermedio1', NULL, 'A2.1', 75.00, 60, 110.00, 110.00, 110.00, 15, 110.00, 'curso'),
(20, 'cursopreintermedio1tutor', NULL, 'A2.1', 75.00, 60, 112.00, 112.00, 112.00, 16, 112.00, 'curso'),
(21, 'cursopreintermedio2', NULL, 'A2.2', 60.00, 60, 100.00, 100.00, 100.00, 17, 100.00, 'curso'),
(22, 'cursopreintermedio2tutor', NULL, 'A2.2', 75.00, 60, 100.00, 112.00, 112.00, 18, 100.50, 'curso'),
(23, 'One-to-one', NULL, 'A1-C2', 0.50, NULL, 20.00, 20.00, 20.00, 19, 20.00, 'oneToOne'),
(24, 'One-to-one Pack 5 sesiones', NULL, 'A1-C2', 2.50, NULL, 50.00, 50.00, 50.00, 20, 50.00, 'oneToOne'),
(25, 'One-to-one Pack 10 sesiones', NULL, 'A1-C2', 5.00, NULL, 100.00, 100.00, 100.00, 21, 100.00, 'oneToOne');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `r_cursopresencial_cursopresencialdetalle`
--

CREATE TABLE IF NOT EXISTS `r_cursopresencial_cursopresencialdetalle` (
  `curso_presencial_id` int(11) NOT NULL,
  `curso_presencial_detalle_id` int(11) NOT NULL,
  PRIMARY KEY  (`curso_presencial_id`,`curso_presencial_detalle_id`),
  KEY `IDX_E7AEEE095AA593FD` (`curso_presencial_id`),
  KEY `IDX_E7AEEE09B54CECD2` (`curso_presencial_detalle_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcar la base de datos para la tabla `r_cursopresencial_cursopresencialdetalle`
--

INSERT INTO `r_cursopresencial_cursopresencialdetalle` (`curso_presencial_id`, `curso_presencial_detalle_id`) VALUES
(2, 1),
(2, 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `r_paquetes_productos`
--

CREATE TABLE IF NOT EXISTS `r_paquetes_productos` (
  `paquete_id` int(11) NOT NULL,
  `producto_id` int(11) NOT NULL,
  PRIMARY KEY  (`paquete_id`,`producto_id`),
  KEY `IDX_1392384E41E2D57E` (`paquete_id`),
  KEY `IDX_1392384E7645698E` (`producto_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcar la base de datos para la tabla `r_paquetes_productos`
--

INSERT INTO `r_paquetes_productos` (`paquete_id`, `producto_id`) VALUES
(11, 15),
(12, 16),
(13, 17),
(14, 18),
(15, 19),
(16, 20),
(17, 21),
(18, 22),
(19, 23),
(20, 24),
(21, 25);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `r_producto_codigopromocional`
--

CREATE TABLE IF NOT EXISTS `r_producto_codigopromocional` (
  `producto_id` int(11) NOT NULL,
  `codigo_promocional_id` int(11) NOT NULL,
  PRIMARY KEY  (`producto_id`,`codigo_promocional_id`),
  KEY `IDX_AC115E647645698E` (`producto_id`),
  KEY `IDX_AC115E64D10C5DCB` (`codigo_promocional_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcar la base de datos para la tabla `r_producto_codigopromocional`
--


--
-- Filtros para las tablas descargadas (dump)
--

--
-- Filtros para la tabla `productos`
--
ALTER TABLE `productos`
  ADD CONSTRAINT `FK_767490E641E2D57E` FOREIGN KEY (`paquete_id`) REFERENCES `paquetes` (`id`);

--
-- Filtros para la tabla `r_cursopresencial_cursopresencialdetalle`
--
ALTER TABLE `r_cursopresencial_cursopresencialdetalle`
  ADD CONSTRAINT `FK_E7AEEE09B54CECD2` FOREIGN KEY (`curso_presencial_detalle_id`) REFERENCES `cursos_presenciales_detalles` (`id`),
  ADD CONSTRAINT `FK_E7AEEE095AA593FD` FOREIGN KEY (`curso_presencial_id`) REFERENCES `cursos_presenciales` (`id`);

--
-- Filtros para la tabla `r_paquetes_productos`
--
ALTER TABLE `r_paquetes_productos`
  ADD CONSTRAINT `FK_1392384E7645698E` FOREIGN KEY (`producto_id`) REFERENCES `productos` (`id`),
  ADD CONSTRAINT `FK_1392384E41E2D57E` FOREIGN KEY (`paquete_id`) REFERENCES `paquetes` (`id`);

--
-- Filtros para la tabla `r_producto_codigopromocional`
--
ALTER TABLE `r_producto_codigopromocional`
  ADD CONSTRAINT `FK_AC115E64D10C5DCB` FOREIGN KEY (`codigo_promocional_id`) REFERENCES `codigos_promocionales` (`id`),
  ADD CONSTRAINT `FK_AC115E647645698E` FOREIGN KEY (`producto_id`) REFERENCES `productos` (`id`);
