# ************************************************************
# Sequel Pro SQL dump
# Version 3408
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Host: webfr (MySQL 5.0.51a-24+lenny5-log)
# Database: spanischool2012
# Generation Time: 2012-03-30 11:55:42 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table carteles
# ------------------------------------------------------------

DROP TABLE IF EXISTS `carteles`;

CREATE TABLE `carteles` (
  `id` int(11) NOT NULL auto_increment,
  `fichero` varchar(100) collate utf8_unicode_ci NOT NULL,
  `activo` tinyint(1) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `carteles` WRITE;
/*!40000 ALTER TABLE `carteles` DISABLE KEYS */;

INSERT INTO `carteles` (`id`, `fichero`, `activo`)
VALUES
	(4,'1332945555.985.jpg',1),
	(5,'1332947507.3758.jpg',1),
	(6,'1332947535.6872.jpg',1);

/*!40000 ALTER TABLE `carteles` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table carteles_presenciales
# ------------------------------------------------------------

DROP TABLE IF EXISTS `carteles_presenciales`;

CREATE TABLE `carteles_presenciales` (
  `id` int(11) NOT NULL auto_increment,
  `fichero` varchar(100) collate utf8_unicode_ci NOT NULL,
  `activo` tinyint(1) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `carteles_presenciales` WRITE;
/*!40000 ALTER TABLE `carteles_presenciales` DISABLE KEYS */;

INSERT INTO `carteles_presenciales` (`id`, `fichero`, `activo`)
VALUES
	(1,'1332945214.9833.jpg',1),
	(2,'1332750142.1164.gif',1);

/*!40000 ALTER TABLE `carteles_presenciales` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table codigos_promocionales
# ------------------------------------------------------------

DROP TABLE IF EXISTS `codigos_promocionales`;

CREATE TABLE `codigos_promocionales` (
  `id` int(11) NOT NULL auto_increment,
  `codigo` varchar(50) collate utf8_unicode_ci NOT NULL,
  `descripcion` varchar(50) collate utf8_unicode_ci NOT NULL,
  `precio_eur` decimal(10,2) default NULL,
  `precio_usd` decimal(10,2) default NULL,
  `precio_rbr` decimal(10,2) default NULL,
  `precio_gbp` decimal(10,2) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table comunes_presenciales
# ------------------------------------------------------------

DROP TABLE IF EXISTS `comunes_presenciales`;

CREATE TABLE `comunes_presenciales` (
  `id` int(11) NOT NULL auto_increment,
  `nombre` varchar(300) collate utf8_unicode_ci NOT NULL,
  `precio_eur` decimal(10,2) default NULL,
  `precio_usd` decimal(10,2) default NULL,
  `precio_rbr` decimal(10,2) default NULL,
  `precio_gbp` decimal(10,2) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `comunes_presenciales` WRITE;
/*!40000 ALTER TABLE `comunes_presenciales` DISABLE KEYS */;

INSERT INTO `comunes_presenciales` (`id`, `nombre`, `precio_eur`, `precio_usd`, `precio_rbr`, `precio_gbp`)
VALUES
	(2,'ssss',33.00,NULL,NULL,NULL),
	(3,'Corte y confección',NULL,NULL,19.20,NULL);

/*!40000 ALTER TABLE `comunes_presenciales` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table cursos_presenciales
# ------------------------------------------------------------

DROP TABLE IF EXISTS `cursos_presenciales`;

CREATE TABLE `cursos_presenciales` (
  `id` int(11) NOT NULL auto_increment,
  `nombre` varchar(300) collate utf8_unicode_ci NOT NULL,
  `nivel` varchar(10) collate utf8_unicode_ci NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `cursos_presenciales` WRITE;
/*!40000 ALTER TABLE `cursos_presenciales` DISABLE KEYS */;

INSERT INTO `cursos_presenciales` (`id`, `nombre`, `nivel`)
VALUES
	(2,'ssssa','dds sa'),
	(3,'bbbbbbbbbb','bb');

/*!40000 ALTER TABLE `cursos_presenciales` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table cursos_presenciales_detalles
# ------------------------------------------------------------

DROP TABLE IF EXISTS `cursos_presenciales_detalles`;

CREATE TABLE `cursos_presenciales_detalles` (
  `id` int(11) NOT NULL auto_increment,
  `semanas` varchar(20) collate utf8_unicode_ci NOT NULL,
  `horas` smallint(6) NOT NULL,
  `residencia` varchar(8) collate utf8_unicode_ci NOT NULL,
  `precio_eur` decimal(10,2) default NULL,
  `precio_usd` decimal(10,2) default NULL,
  `precio_rbr` decimal(10,2) default NULL,
  `precio_gbp` decimal(10,2) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `cursos_presenciales_detalles` WRITE;
/*!40000 ALTER TABLE `cursos_presenciales_detalles` DISABLE KEYS */;

INSERT INTO `cursos_presenciales_detalles` (`id`, `semanas`, `horas`, `residencia`, `precio_eur`, `precio_usd`, `precio_rbr`, `precio_gbp`)
VALUES
	(1,'111',33,'opcional',3.00,NULL,NULL,NULL),
	(2,'sasas',33,'opcional',56.00,NULL,NULL,NULL),
	(3,'qqq',111,'opcional',1.00,2.00,3.00,4.00);

/*!40000 ALTER TABLE `cursos_presenciales_detalles` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table paquetes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `paquetes`;

CREATE TABLE `paquetes` (
  `id` int(11) NOT NULL auto_increment,
  `descripcion` varchar(60) collate utf8_unicode_ci NOT NULL,
  `precio_eur` decimal(10,2) default NULL,
  `precio_usd` decimal(10,2) default NULL,
  `precio_rbr` decimal(10,2) default NULL,
  `precio_gbp` decimal(10,2) default NULL,
  `ahorro` varchar(20) collate utf8_unicode_ci default NULL,
  `activo` tinyint(1) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `paquetes` WRITE;
/*!40000 ALTER TABLE `paquetes` DISABLE KEYS */;

INSERT INTO `paquetes` (`id`, `descripcion`, `precio_eur`, `precio_usd`, `precio_rbr`, `precio_gbp`, `ahorro`, `activo`)
VALUES
	(11,'cursoelemental1',20.00,30.00,300.00,10.00,NULL,0),
	(12,'cursoelemental1tutor',100.00,100.00,100.00,100.00,NULL,0),
	(13,'cursoelemental2',100.00,100.00,100.00,100.00,NULL,0),
	(14,'cursoelemental2tutor',100.00,100.00,100.00,100.00,NULL,0),
	(15,'cursopreintermedio1',110.00,110.00,110.00,110.00,NULL,0),
	(16,'cursopreintermedio1tutor',112.00,112.00,112.00,112.00,NULL,0),
	(17,'cursopreintermedio2',100.00,100.00,100.00,100.00,NULL,0),
	(18,'cursopreintermedio2tutor',100.00,112.00,112.00,100.50,NULL,0),
	(19,'One-to-one',20.00,20.00,20.00,20.00,NULL,0),
	(20,'One-to-one Pack 5 sesiones',50.00,50.00,50.00,50.00,NULL,0),
	(21,'One-to-one Pack 10 sesiones',100.00,100.00,100.00,100.00,NULL,0);

/*!40000 ALTER TABLE `paquetes` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table productos
# ------------------------------------------------------------

DROP TABLE IF EXISTS `productos`;

CREATE TABLE `productos` (
  `id` int(11) NOT NULL auto_increment,
  `nombre` varchar(300) collate utf8_unicode_ci NOT NULL,
  `codigo_plataforma` varchar(50) collate utf8_unicode_ci default NULL,
  `nivel` varchar(10) collate utf8_unicode_ci default NULL,
  `horas` decimal(5,2) default NULL,
  `dias_acceso` smallint(6) default NULL,
  `precio_eur` decimal(10,2) default NULL,
  `precio_usd` decimal(10,2) default NULL,
  `precio_rbr` decimal(10,2) default NULL,
  `paquete_id` int(11) default NULL,
  `precio_gbp` decimal(10,2) default NULL,
  `tipo` varchar(8) collate utf8_unicode_ci NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `UNIQ_767490E641E2D57E` (`paquete_id`),
  CONSTRAINT `FK_767490E641E2D57E` FOREIGN KEY (`paquete_id`) REFERENCES `paquetes` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `productos` WRITE;
/*!40000 ALTER TABLE `productos` DISABLE KEYS */;

INSERT INTO `productos` (`id`, `nombre`, `codigo_plataforma`, `nivel`, `horas`, `dias_acceso`, `precio_eur`, `precio_usd`, `precio_rbr`, `paquete_id`, `precio_gbp`, `tipo`)
VALUES
	(15,'cursoelemental1',NULL,'A1.1',75.00,60,20.00,30.00,300.00,11,10.00,'curso'),
	(16,'cursoelemental1tutor',NULL,'A1.1',75.00,60,100.00,100.00,100.00,12,100.00,'curso'),
	(17,'cursoelemental2',NULL,'A1.2',75.00,60,100.00,100.00,100.00,13,100.00,'curso'),
	(18,'cursoelemental2tutor',NULL,'A1.2',75.00,60,100.00,100.00,100.00,14,100.00,'curso'),
	(19,'cursopreintermedio1',NULL,'A2.1',75.00,60,110.00,110.00,110.00,15,110.00,'curso'),
	(20,'cursopreintermedio1tutor',NULL,'A2.1',75.00,60,112.00,112.00,112.00,16,112.00,'curso'),
	(21,'cursopreintermedio2',NULL,'A2.2',60.00,60,100.00,100.00,100.00,17,100.00,'curso'),
	(22,'cursopreintermedio2tutor',NULL,'A2.2',75.00,60,100.00,112.00,112.00,18,100.50,'curso'),
	(23,'One-to-one',NULL,'A1-C2',0.50,NULL,20.00,20.00,20.00,19,20.00,'oneToOne'),
	(24,'One-to-one Pack 5 sesiones',NULL,'A1-C2',2.50,NULL,50.00,50.00,50.00,20,50.00,'oneToOne'),
	(25,'One-to-one Pack 10 sesiones',NULL,'A1-C2',5.00,NULL,100.00,100.00,100.00,21,100.00,'oneToOne');

/*!40000 ALTER TABLE `productos` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table r_cursopresencial_cursopresencialdetalle
# ------------------------------------------------------------

DROP TABLE IF EXISTS `r_cursopresencial_cursopresencialdetalle`;

CREATE TABLE `r_cursopresencial_cursopresencialdetalle` (
  `curso_presencial_id` int(11) NOT NULL,
  `curso_presencial_detalle_id` int(11) NOT NULL,
  PRIMARY KEY  (`curso_presencial_id`,`curso_presencial_detalle_id`),
  KEY `IDX_E7AEEE095AA593FD` (`curso_presencial_id`),
  KEY `IDX_E7AEEE09B54CECD2` (`curso_presencial_detalle_id`),
  CONSTRAINT `FK_E7AEEE09B54CECD2` FOREIGN KEY (`curso_presencial_detalle_id`) REFERENCES `cursos_presenciales_detalles` (`id`),
  CONSTRAINT `FK_E7AEEE095AA593FD` FOREIGN KEY (`curso_presencial_id`) REFERENCES `cursos_presenciales` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `r_cursopresencial_cursopresencialdetalle` WRITE;
/*!40000 ALTER TABLE `r_cursopresencial_cursopresencialdetalle` DISABLE KEYS */;

INSERT INTO `r_cursopresencial_cursopresencialdetalle` (`curso_presencial_id`, `curso_presencial_detalle_id`)
VALUES
	(2,1),
	(2,3);

/*!40000 ALTER TABLE `r_cursopresencial_cursopresencialdetalle` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table r_paquetes_productos
# ------------------------------------------------------------

DROP TABLE IF EXISTS `r_paquetes_productos`;

CREATE TABLE `r_paquetes_productos` (
  `paquete_id` int(11) NOT NULL,
  `producto_id` int(11) NOT NULL,
  PRIMARY KEY  (`paquete_id`,`producto_id`),
  KEY `IDX_1392384E41E2D57E` (`paquete_id`),
  KEY `IDX_1392384E7645698E` (`producto_id`),
  CONSTRAINT `FK_1392384E7645698E` FOREIGN KEY (`producto_id`) REFERENCES `productos` (`id`),
  CONSTRAINT `FK_1392384E41E2D57E` FOREIGN KEY (`paquete_id`) REFERENCES `paquetes` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `r_paquetes_productos` WRITE;
/*!40000 ALTER TABLE `r_paquetes_productos` DISABLE KEYS */;

INSERT INTO `r_paquetes_productos` (`paquete_id`, `producto_id`)
VALUES
	(11,15),
	(12,16),
	(13,17),
	(14,18),
	(15,19),
	(16,20),
	(17,21),
	(18,22),
	(19,23),
	(20,24),
	(21,25);

/*!40000 ALTER TABLE `r_paquetes_productos` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table r_producto_codigopromocional
# ------------------------------------------------------------

DROP TABLE IF EXISTS `r_producto_codigopromocional`;

CREATE TABLE `r_producto_codigopromocional` (
  `producto_id` int(11) NOT NULL,
  `codigo_promocional_id` int(11) NOT NULL,
  PRIMARY KEY  (`producto_id`,`codigo_promocional_id`),
  KEY `IDX_AC115E647645698E` (`producto_id`),
  KEY `IDX_AC115E64D10C5DCB` (`codigo_promocional_id`),
  CONSTRAINT `FK_AC115E64D10C5DCB` FOREIGN KEY (`codigo_promocional_id`) REFERENCES `codigos_promocionales` (`id`),
  CONSTRAINT `FK_AC115E647645698E` FOREIGN KEY (`producto_id`) REFERENCES `productos` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
